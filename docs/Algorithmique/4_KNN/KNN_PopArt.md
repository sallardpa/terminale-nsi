# PopArt et algorithme des plus proches voisins :hibiscus:


Nous allons détourner un algorithme appelé "algorithme des $k$ plus proches voisins" (:flag_gb:/:flag_us: _KNN or k-nearest neighbours_) pour imiter le travail d'Andy Warhol :

![wharhol](wharhol.png){: .center}



## ^^1. Démarche générale de la warholisation^^

On décompose la démarche d'Andy Warhol de la façon suivante :

- on choisit une photographie et on ***simplifie*** ses couleurs. Par exemple, 

![step1](step1.png){: .center}

- on choisit une transformation de chacune des ***couleurs simples***. Par exemple, bleu → vert, blanc → orange, noir → bleu.

![step2](step2.png){: .center}

- on recommence cette transformation plusieurs fois et on assemble les différentes images. Par exemple,

![libfinal](libertyFinal2.png){: .center}


!!! info "Manipulation d'images en Python"
    Nous utiliserons le module `PIL` de Python pour manipuler des images, pixel par pixel.

    Voici un extrait de son interface :

    | Fonction | Description |
    | :------ | :------- | 
    | `get_pixel((x,y))` | donne le code couleur RGB du pixel en position `(x,y)` |
    | `put_pixel((x,y), (r,g,b))` | affecte la couleur `(r,g,b)` au pixel `(x,y)` |

!!! warning "Simplification des couleurs"
    Parmi les 3 étapes de cette démarche, la plus délicate à gérer est la "simplification" des couleurs.

    La couleur d'un pixel est un code RGB à trois nombres compris entre 0 et 255.
    ![rgb](CodageRGB.png){: .center}

    Il y a donc plus de 16 millions (256×256×256 = 16 777 216) de couleurs possibles dans une image. 

    Simplifier une couleur signifie donc :

    - ne garder que 8 ou 10 couleurs de base (la *palette* du peintre) 
    - et identifier, pour chaque pixel de notre image, de quelle couleur de base il est **le plus proche**.


## ^^2. Notion de proximité, calcul de distance^^


!!! question "Proximité dans un plan"
    Considérons pour commencer la situation suivante : il y a trois restaurants A, B et C et vous êtes au point M. Affamé(e), vous devez aller au restaurant le plus proche. 

    ![distancePlan](distance_plan.png){: .center}
    Lequel choisissez-vous ?


??? danger " Solution"
    On utilise le théorème de Pythagore pour calculer les trois distances AM, BM et CM :

    - MA = $\sqrt{ (5-1)^2 + (4-6)^2}\approx 4,47$
    - MB = $\sqrt{ (5-3)^2 + (4-1)^2} \approx 3,6$
    - MC = 5 (_juste en comptant les carreaux !_)

    C'est donc le point B qui est le plus proche de M.

    De manière générale, la distance entre un point $P(x_P, y_P)$ et un point $Q(x_Q, y_Q)$ est donnée par la formule de Pythagore :

    $$ PQ = \sqrt{(x_P-x_Q)^2 + (y_P-y_Q)^2}$$


!!! question "Proximité dans un espace à trois dimensions"
    
    ![distance3D](distance_esp.png){: .center}

    La formule de Pythagore se généralise pour un calcul de distance en 3 dimensions : par exemple, la distance entre P(1,2,3) et Q(4,6,1) vaut 

    $$ PQ = \sqrt{(4-1)^2 + (6-2)^2 + (1-3)^2} \approx 5,38$$

    Vérifiez-le grâce à la fonction `distance3D` :

    {{IDE("distance")}}
    


!!! question "Application"

    Le <span style="background-color: #00561b">vert impérial</span> de code RGB (0, 86, 27) est-il plus proche :

    - de l'<span style="background-color: #4b0082">indigo</span> de code (75, 0, 130) ;
    - du <span style="background-color: #40e0d0">bleu turquoise</span> de code (64, 224, 208) ;
    - ou du <span style="background-color: #dc143c">pourpre</span> de code (220, 20, 60) ?

??? danger "Solution"

    On calcule les trois "distances" à la calculatrice ou bien avec la fonction `distance3D` : 

    - entre (0, 86, 27) et (75, 0, 130) : $d_1 = \sqrt{(0-75)^2 + (86-0)^2 + (27-130)^2} \approx 153,7$
    - entre (0, 86, 27) et (64, 224, 208) : $d_2 = \sqrt{(0-64)^2 + (86-224)^2 + (27-208)^2} \approx 236,4$
    - entre (0, 86, 27) et (220, 20, 60) : $d_3 = \sqrt{(0-220)^2 + (86-20)^2 + (27-60)^2} \approx 232,0$

    Le <span style="background-color: #00561b">vert impérial</span>  est donc plus proche de l'<span style="background-color: #4b0082">indigo</span> que des deux autres couleurs.


## ^^3. TP PopArt^^

Télécharger le [carnet Jupyter](ProjetPopArt.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}. 

Il sera plus commode de copier le code dans un IDE (Thonny par exemple).

Vous aurez besoin de ces deux [images compressées](PopArt_Images.zip), _à extraire dans votre répertoire de travail_ .

## ^^4. Exercice type Bac^^

* Exercice type Bac : à faire sur [feuille](SujetBac_KNN_AmSud2022.pdf)