# Algorithmes de parcours de graphes :ferris_wheel:

!!! tip "Algorithme de parcours"
    Un parcours de graphe est un algorithme consistant à explorer **tous** les sommets d'un graphe de proche en proche à partir d'un sommet initial donné. Cela généralise les parcours des arbres binaires que vous avez déjà vus.
    
    Ces parcours sont notamment utilisés pour rechercher un plus court chemin (routage sur Internet, applications de navigation dans un réseau de métros ou de trains) ou pour trouver la sortie d'un labyrinthe. 



## ^^1. Parcours de graphes, en version intuitive^^


!!! tip "Parcours en largeur"
    Dans un parcours en largeur, on progresse par "cercles concentriques" à partir du sommet de départ. On visite d'abord tous les sommets à une distance de 1 unité du point départ, puis tous les sommets à une distance de 2 unités, etc. (_en considérant que la distance est le nombre d'arêtes_).

    Par exemple, le parcours en largeur du graphe ci-dessous au départ de A donne la liste suivante des sommets : A ; B ; D ; C ; E ; F ; G ; H (_avec la convention que, quand il y a plusieurs sommets à la même distance, on les écrit dans l'ordre alphabétique_).

    ![parcoursLargeur](Algo_Graphes_Largeur.png)


<!--     
<div class="centre">
```mermaid
    graph LR
    A(("A"))
    B(("B"))
    C(("C"))
    D(("D"))
    E(("E"))
    F(("F"))
    G(("G"))
    A --- B
    A --- C
    B --- D
    C --- G
    E --- G
    A --- E
    D --- F
    G --- F
```
</div>
-->

!!! question "Exercice 1"
    Effectuer le parcours en largeur du graphe représenté ci-dessous, en partant du sommet A.

    ![](data/mermaid-diagram-1.svg#only-light){ width=50%}
    ![](data/mermaid-diagram-1d.svg#only-dark){ width=50%}


    ??? danger "Solution"
        Le résultat du parcours en largeur est : A ; B ; C ; E ; D ; G ; F.

<!--     
<div class="centre">
```mermaid
    flowchart LR
    A(A) ---B(B)
    A --- F(F)
    B --- F
    F --- D(D)
    F --- C(C)
    F --- K(K)
    B --- E(E) 
    K --- H(H)
    E --- H
    H --- G(G)
```
</div> 
-->

!!! question "Exercice 2"
    Effectuer le parcours en largeur du graphe représenté ci-dessous, en partant du sommet D.

    ![](data/mermaid-diagram-2.svg#only-light){ width=50%}
    ![](data/mermaid-diagram-2d.svg#only-dark){ width=50%}


    ??? danger "Solution"
        Le résultat du parcours en largeur est : D ; F ; A ; B ; C ; K ; E ; H ; G.


!!! tip "Parcours en profondeur"
    Dans un parcours en profondeur, l'idée est de suivre au maximum un chemin. Si on arrive au bout d'un chemin, on rebrousse chemin jusqu'à la première bifurcation possible. 

    Par exemple, un parcours en profondeur du graphe ci-dessous au départ du sommet A donne : 

    <div class="centre">
    A ; B ; E ; F ; C ; K ; H ; G ; D ; Q ; M.
    </div>

    ![](data/mermaid-diagram-3.svg#only-light){ width=50%}
    ![](data/mermaid-diagram-3d.svg#only-dark){ width=50%}
    
    
<!-- 
<div class="centre">
```mermaid
    flowchart LR
    A(A) --- B(B)
    B --- E(E)
    E --- F(F)
    F --- C(C)
    C --- K(K)
    E --- H(H)
    H --- G(G)
    A --- D(D)
    A --- M(M)
    M --- D
    D --- Q(Q)
```
</div> 
-->


!!! question "Exercice"

    Le mécanisme d'un parcours en profondeur est encore plus explicite dans le cas des graphes orientés.

    Effectuer le parcours en profondeur du graphe représenté ci-dessous, en partant du sommet A.

    <div class="centre">
    ```mermaid
    graph LR
    A(("A"))
    B(("B"))
    C(("C"))
    D(("D"))
    E(("E"))
    F(("F"))
    G(("G"))
    A --> B
    A --> C
    B --> D
    C --> G
    E --> G
    A --> E
    D --> F
    G --> F
    ```
    </div> 


    ??? danger "Solution"
        Le résultat du parcours en profondeur est : A ; E ; G ; F ; C ; B ; D.




## ^^2. Parcours de graphes, en version rigoureuse^^


!!! tip "Algorithme de parcours"
    Les deux types de parcours (en largeur et en profondeur) reposent sur le même principe de base :

    - Le sommet de départ ```dep``` étant donné, on crée une structure ```V```  qui contiendra au départ uniquement ce sommet ```dep``` .

    - Tant que ```V``` n'est pas vide :
    
        - on choisit un sommet ```s```  existant dans ```V``` ;
        - on **visite** ```s```, c'est-à-dire qu'on l'affiche ;
        - on ajoute à ```V``` tous les voisins de ```s``` ***pas encore découverts***.
        
    La structure ```V``` sert donc à mémoriser les sommets qui ont été découverts mais pas encore visités. 

    - Si on choisit une **file** (FIFO), on visitera les sommets dans l'ordre d'arrivée, donc les plus proches du sommet précédent. On obtient donc un *parcours en largeur* (:flag_gb: :flag_us: _Breadth First Search_, ou BFS).
    - Si on choisit une **pile** (LIFO), on visitera d'abord les derniers sommets arrivés, donc on parcourt le graphe en visitant à chaque étape un voisin du précédent. On obtient donc un *parcours en profondeur* (:flag_gb: :flag_us: _Depth First Search_, ou DFS).

### ^^2.1. Le parcours en largeur^^

!!! success "Exemple de parcours en largeur"

    Voici une illustration du parcours en largeur d'un graphe avec B comme sommet de départ :

    ![](bfs.gif){: .center}

    **Codes couleur :**

    - **vert** : les sommets non encore traités.
    - **rouge** : le sommet en cours de traitement.
    - **orange** : la **file d'attente** des sommets qui seront bientôt traités. On y rajoute à chaque fois les voisins du sommet en cours de traitement, uniquement **si ils n'ont pas encore été découverts**.
    - **noir** : les sommets traités.

    Le résultat du parcours en largeur du graphe est donc : B ; A ; D ; E ; C ; F ; G ; H

    
> Remarque : quand on ajoute plusieurs voisins d'un seul coup dans la file, on fait ici le choix de les ajouter dans l'ordre alphabétique.



!!! note "Un outil en ligne"
    Un [outil en ligne](https://workshape.github.io/visual-graph-algorithms/#dfs-visualisation){target=_blank}, permet de visualiser le résultat du parcours en profondeur (*DFS : depth first search*) d'un graphe.

    Un graphe est donné en exemple, mais vous pouvez le modifier ou construire le vôtre :

    > Dans les menus déroulants, bien choisir Algorithm : **BFS** et Example graph : **directedGraph**




### ^^2.2. Codage d'un parcours en largeur en Python^^

> Remarque : ce code est donné uniquement à titre illustratif : il n'est pas nécessaire de l'apprendre par cœur. Par ailleurs, dans une partie cachée, il y a le codage des primitives des files et des graphes.

{{ IDE("data/algoGrapheLargeur")}}


### ^^2.3. Le parcours en profondeur^^



!!! success "Exemple de parcours en profondeur"

    **Rappel :** dans un parcours en profondeur, chaque nouveau voisin découvert est ajouté au sommet d'une **pile**.

    On prend comme exemple le même graphe que dans l'exemple précédent, avec un départ du sommet B. _On convient que, quand il y a plusieurs sommets à empiler en même temps, on les empile dans l'ordre alphabétique._

    ![](dfs.png){: .center}
    
    - B est visité et on empile les voisins de B : la pile contient alors A (bas de la pile), D, E (sommet de la pile).
    - E est dépilé (il est visité) et on empile ses voisins qui ne sont pas déjà dans la pile : la pile contient alors A (bas de la pile), D, F, G (sommet de la pile).
    - G est dépilé (il est visité) et on empile H, son seul voisin pas encore découvert : la pile contient alors A (bas de la pile), D, F, H (sommet de la pile).
    - H est dépilé et il n'y a pas de nouveau voisins à empiler : la pile contient alors A (bas de la pile), D, F (sommet de la pile).
    - et ainsi de suite
     
    Au final, on obtient l'ordre de visite suivant : B ; E ; G ; H ; F ; D ; C ; A.


### ^^2.4. Codage d'un parcours en profondeur en Python^^

> Remarque : ce code est donné uniquement à titre illustratif : il n'est pas nécessaire de l'apprendre par cœur.

{{ IDE("data/algoGrapheProfondeur")}}


### ^^2.5. Applications^^

!!! question "Graphe connexe ou pas ?"
    On donne ci-dessous le code d'un graphe non orienté `graphe1`.

    En comparant les résultats des appels des fonctions `liste_sommets` et `parcours` depuis le sommet A (soit en largeur, soit en profondeur), déterminer si ce graphe est connexe ou pas.

    {{ IDE("data/algoGraphePasConnexe")}}

    ??? danger "Solution"
        On observe que le parcours du graphe depuis le sommet A fournit moins de sommets que la liste des sommets. Ce graphe n'est donc pas connexe.


!!! question "Sommet atteignable ou pas ?"
    On donne ci-dessous le code d'un graphe connexe orienté `graphe2`.

    En effectuant un `parcours` depuis le sommet A (soit en largeur, soit en profondeur), déterminer s'il est possible d'atteindre le sommet B depuis ce sommet A.

    Ce même sommet B est-il atteignable depuis le sommet N ?

    {{ IDE("data/algoGrapheA2B")}}

    ??? danger "Solution"
        On observe que le sommet B n'apparait pas dans le parcours du graphe depuis le sommet A : B n'est donc pas atteignable depuis A. 

        Mais B est atteignable depuis le sommet N, comme le montre un parcours depuis N.



## ^^3. TP: graphe et labyrinthe^^


Télécharger le [carnet Jupyter](TP_Graphe_Labyrinthe.ipynb), à ouvrir sur [Basthon](https://notebook.basthon.fr/){target="_blank"}. 

Il faudra aussi charger dans Basthon les trois modules Python suivants (_Ouvrir..._ puis _Installer le module_) : [laby2d.py](data/laby2d.py), [laby3d.py](data/laby3d.py) et [solution.py](data/solution.py).

## ^^4. Algorithme de recherche d'un plus court chemin : algorithme de Dijkstra^^

Vous avez déjà effectué la recherche d'un **plus court chemin** dans un **graphe pondéré**, quand il s'agissait de trouver le chemin emprunté pour aller d'un routeur A à un routeur B selon le protocole OSPF.

On peut systématiser la détermination du plus court chemin d'un point à un autre en appliquant **l'algorithme de Dijkstra** (mis au point par Edsger Dijkstra en 1959), comme expliqué dans la vidéo suivante :

<p align="center">
<iframe width="790" height="372" src="https://www.youtube.com/embed/rI-Rc7eF4iw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

<!-- 
```mermaid
    flowchart LR
    E([E]) --- |10| D([D])
    E ---  |30| A([A])
    E --- |40| C([C])
    A --- |40| B([B])
    A --- |10| C 
    B --- |20| F([F])
    D --- |70| F
    C --- |20| B
    D --- |10| A
    D --- |40| C
``` 
-->

!!! question "Application de l'algorithme de Dijkstra"
    
    Donner le plus court chemin pour aller de E à F dans le graphe ci-dessous :
    
    ![](data/mermaid-diagram-5.svg#only-light){ width=50%}
    ![](data/mermaid-diagram-5d.svg#only-dark){ width=50%}


    ??? danger "Solution"
    
        |E|A|B|C|D|F|Choix|
        |:---:|:---:|:---:|:---:|:---:|:---:|:---:|
        |**0**|--|--|--|--|--|E(0)|
        |.|30 vE|--|40 vE|**10 vE**|--|D(10)|       
        |.|**20 vD**|--|40 vE|.|80 vD|A(20)|
        |.|.|60 vA|**30 vA**|.|80 vD|C(30)|
        |.|.|**50 vC**|.|.|80 vD|B(50)|
        |.|.|.|.|.|**70 vB**|F(70)|
    
        Le meilleur trajet est donc E-D-A-C-B-F, pour une distance totale de 70.  
        _Attention ce trajet correspond à la colonne choix (dans l'ordre) mais ce n'est pas toujours le cas._ 


!!! question "Deuxième application de l'algorithme de Dijkstra"

    Déterminer le plus court chemin entre A et G dans le graphe pondéré ci-dessous ([en version papier](Dijkstra_Exo2.pdf)).
    
    ![grapheDijkstra](DijkstraSupp.png)


    ??? danger "Solution"
    
        |A|B|C|D|E|F|G|H|Choix|
        |:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
        |**0**|--|--|--|--|--|--|--|A(0)|
        |.|10 vA|8 vA|**5 vA**|--|--|--|--|D(5)|       
        |.|10 vA|**8 vA**|.|--|--|20 vD|--|C(8)|
        |.|**9 vC**|.|.|--|15 vC|20 vD|12 vC|B(9)|
        |.|.|.|.|19 vB|15 vD|20 vD|**12 vC**|H(12)|
        |.|.|.|.|17 vH|**13 vH**|17 vH|.|F(13)|
        |.|.|.|.|17 vH|.|**16 vF**|.|G(16)|

        Le meilleur trajet est donc A - C - H - F - G, pour une distance totale de 16.

L'algorithme de Dijkstra, ici exécuté de manière manuelle, est bien sûr programmable. Mais cela dépasse le niveau attendu en fin de Terminale.

## ^^5. Exercices^^


* Exercice type Bac n°1:
    
    -[énoncé](ParcoursGraphes_SujetBac.pdf), à faire sur feuille.