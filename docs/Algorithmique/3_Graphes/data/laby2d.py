import p5
class Laby2d:
    """
    Trace en 2D un labyrynthe
    Paramètres en entrée: 
        laby: DICT contenant un graphe avec liste d'adjacence
        largeur: INT nombre de case en largeur
        hauteur: INT nombre de cases en hauteur
        taille_case: INT taille de la case en pixel
        coordonnees: BOOLÉEN si on veut tracer les coordonnées sur les cases
    
    """
    def __init__(self,laby,largeur,hauteur,taille_case=32,coordonnees=False):
        self.laby=laby
        self.largeur=largeur
        self.hauteur=hauteur
        self.taille_case=taille_case
        self.largeur_trait=1
        self.run()
        self.coordonnees=coordonnees
    def dessinerBord(self,x,y,direction):
            if direction=="nord":
                ma_ligne=p5.line(x*self.taille_case,y*self.taille_case,x*self.taille_case+self.taille_case,y*self.taille_case)
            elif direction=="sud":
                 ma_ligne=p5.line(x*self.taille_case,y*self.taille_case+self.taille_case,x*self.taille_case+self.taille_case,y*self.taille_case+self.taille_case)
            elif direction=="est":
                 ma_ligne=p5.line(x*self.taille_case+self.taille_case,y*self.taille_case,x*self.taille_case+self.taille_case,y*self.taille_case+self.taille_case)
            elif direction=="ouest":
                 ma_ligne=p5.line(x*self.taille_case,y*self.taille_case,x*self.taille_case,y*self.taille_case+self.taille_case)
    def dessiner_laby(self):
        for sommet in self.laby:
            x=sommet[0]
            y=sommet[1]
            destinations=self.laby[sommet]
            nord=False
            sud=False
            est=False
            ouest=False
            for destination in destinations:
                x_destination=destination[0]
                y_destination=destination[1]
                if x==x_destination:
                    if y+1==y_destination:
                        sud=True
                    elif y-1==y_destination:
                        nord=True
                elif y==y_destination:
                    if x-1==x_destination:
                        ouest=True
                    elif x+1==x_destination:
                        est=True
            # tracé des murs
            p5.stroke("green")
            if nord==False:
                self.dessinerBord(x,y,"nord")
            if sud==False:
                self.dessinerBord(x,y,"sud")
            if est==False:
                self.dessinerBord(x,y,"est")
            if ouest==False:
                self.dessinerBord(x,y,"ouest")
            # tracé des coordonnées
            if self.coordonnees==True:
                p5.noStroke()
                p5.fill("black")
                texte="("+str(x)+","+str(y)+")"
                p5.textSize(self.taille_case/4)
                p5.text(texte,x*self.taille_case+self.taille_case/5,y*self.taille_case+self.taille_case/1.8)
                
    def setup(self):
        p5.createCanvas(self.largeur*self.taille_case,self.hauteur*self.taille_case)
        p5.background("white")
    def draw(self):
        p5.fill("white")
        p5.rect(0, 0, self.largeur*self.taille_case, self.hauteur*self.taille_case)
        self.dessiner_laby()
        
    def run(self):
        p5.run(self.setup,self.draw)
        
