#--- HDR ---#
# file
def creer_file_vide()  :
    return []

def enfiler(f, donnée)  :
    f.append(donnée) # ajouter en queue de la file
    return f # en fait, c'est superflu : en Python, la liste "file" est modifiée en place

def defiler(f)  :
    assert not est_vide(f), "On ne peut pas défiler une file vide"
    tête_de_file = f.pop(0) # enlever la première valeur de la file
    return tête_de_file

def est_vide(f)  :
    return len(f) == 0 # renvoie le booléen True si la longueur est nulle et False sinon
# interface d'un graphe non orienté
def creer_graphe_vide():
    return dict()

def liste_sommets(G):
    return [s for s in G.keys()]

def liste_voisins(G,s) :
    return G[s]

def sont_voisins(G,s1,s2) :
    return s2 in G[s1]

def ajouter_sommet(G,s) :
    assert s not in G.keys(), "Ce sommet existe déja"
    G[s] = []
    return None

def ajouter_arete(G, s1, s2) :
    if not sont_voisins(G, s1, s2) :
        G[s1].append(s2)
        G[s2].append(s1)
    return None 

#--- HDR ---#
def parcours_largeur(G,s):
    f = creer_file_vide()
    enfiler(f,s)
    tab_decouverts = [s]
    while not est_vide(f):
        s = defiler(f)
        print(s, end=" - ")
        for v in liste_voisins(G,s) :
            if v not in tab_decouverts :
                enfiler(f,v)
                tab_decouverts.append(v)
    return None

# modélisation du graphe par un dictionnaire
graphe = {"B" : ["A", "D", "E"], "A" : ["B", "C"], "C" : ["A", "D"], "D":["B", "C", "E"],
"E" : ["B", "F", "G"], "F" : ["E", "G"], "G" : ["E", "F", "H"], "H":["G"]}
# parcours du graphe
parcours_largeur(graphe, "B")



