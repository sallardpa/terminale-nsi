# Algorithmique :roller_coaster:

Cette partie est décomposée en :

* Rappels et compléments de Première : 

    - les algorithmes gloutons :boar:
    - la recherche par dichotomie dans un tableau trié :broken_heart:
    - les algorithmes de tri :performing_arts:
    
* Algorithmes sur les arbres :

    - Parcours d'un arbre :monkey:
    - ABR : insertion et recherche :dart:

* Algorithmes sur les graphes :

    - Parcours d'un graphe :ferris_wheel:

* Autres algorithmes :

    - Programmation Dynamique  :dancer:
    - PopArt et KNN :hibiscus:

