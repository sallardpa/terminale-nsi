# Algorithmes de tris :performing_arts:

## ^^1. Tri par sélection, tri par insertion : rappels du cours de Première^^

Pour le cours complet de Première : c'est [ici](https://sallardpa.forge.apps.education.fr/premiere-nsiAlgorithmique/TriInsertSelect/Algo_TrisInsertSelect/).


!!! tip "Complexité d'un algorithme"
    
    ![Classes de complexité](data/ordres-de-complexite.png){: .center width=40%}
    
    
    En première approche, on peut évaluer la complexité d'un algorithme composé uniquement de boucles `for` d'après l'organisation de ces boucles `for` :

    * s'il y a une seule boucle, ou bien plusieurs boucles les unes à la suite des autres, la complexité de l'algorithme est $O(n)$, c'est-à-dire linéaire, _proportionnelle à la taille_ $n$ de la structure étudiée ;
    * s'il y a une boucle `for` imbriquée à l'intérieur d'une autre boucle `for`, la complexité de l'algorithme est $O(n^2)$, c'est-à-dire quadratique, _proportionnelle au carré de la taille_ $n$ de la structure étudiée.



## ^^2. L'algorithme de tri fusion^^

En anglais : le *merge sort*.



### 2.1 Préambule : l'interclassement

Le mécanisme principal du tri fusion est la **fusion** de deux tableaux triés en un nouveau tableau lui-aussi trié.

![image](data/schema_interclass.png){: .center}

On appellera ce mécanisme l'**interclassement**. Le principe de l'interclassement de deux tableaux ```tab11``` et ```tab2```est le suivant :

- on part d'un tableau vide ```tab_final``` et on initialise à zéro  un indice ```k1``` pour le tableau ```tab1```  et un indice ```k2```  pour le tableau ```tab2```.
- si `tab1[k1] < tab2[k2]`, on ajoute la valeur `tab1[k1]` à `tab_final` et on incrémente `k1` ; et sinon on ajoute la valeur `tab2[k2]` à `tab_final` et on incrémente `k2`.
- on répète cela tant qu'on n'a atteint ni la fin de `tab1` ni celle de `tab2`.
- quand la fin d'un des deux tableaux est atteinte, on ajoute la totalité restante de l'autre tableau.


!!! question "Exercice"
    === "Énoncé"
        Coder la fonction ```interclassement```. 
        On veut par exemple que l'appel `interclassement([3,4,8],[1,2,5,7])` renvoie le tableau `[1,2,3,4,5,7,8]`.

        {{ IDE() }}
    === "Correction"
        ```python
        def interclassement(tab1, tab2):
            k1 = 0
            k2 = 0
            tab_final = []
            while k1 < len(tab1) and k2 < len(tab2):
                if tab1[k1] < tab2[k2]:
                    tab_final.append(tab1[k1])
                    k1 += 1
                else:
                    tab_final.append(tab2[k2])
                    k2 += 1
            # cas où il reste des valeurs dans tab1
            for k in range(k1, len(tab1)):
                tab_final.append(tab1[k])
            # cas où il reste des valeurs dans tab2
            for k in range(k2, len(tab2)):
                tab_final.append(tab2[k])    
            return tab_final
        ```


<!-- ligne vide car bug entre IDE avant et sous-titre après -->

### 2.2 La fusion

#### 2.2.1 Principe

L'idée du tri fusion est le découpage du tableau de départ en une multitude de petits tableaux ne contenant qu'un seul élément. Ces tableaux élémentaires seront ensuite interclassés avec la fonction précédente.

![image](data/fusion.png){: .center}

**Principe de l'algorithme du tri fusion :**

- pour trier un tableau, on interclasse les deux moitiés de ce tableau, précédemment elles-mêmes triées par le tri fusion.
- si un tableau à trier est réduit à un élément, il est déjà trié. 


#### 2.2.2 Implémentation

La grande force de ce tri va être qu'il se programme simplement de manière **récursive**, en appelant à chaque étape la même fonction mais avec une taille de tableau divisée par deux, ce qui justifie son classement parmi les algorithmes utilisant le principe de _«diviser pour régner»_.

!!! tip "Algorithme de tri fusion (*merge sort*)"
    ```python
    def interclassement(tab1, tab2):
        k1 = 0
        k2 = 0
        tab_final = []
        while k1 < len(tab1) and k2 < len(tab2):
            if tab1[k1] < tab2[k2]:
                tab_final.append(tab1[k1])
                k1 += 1
            else:
                tab_final.append(tab2[k2])
                k2 += 1
        # cas où il reste des valeurs dans tab1
        for k in range(k1, len(tab1)):
            tab_final.append(tab1[k])
        # cas où il reste des valeurs dans tab2
        for k in range(k2, len(tab2)):
            tab_final.append(tab2[k])    
        return tab_final

    def tri_fusion(tab):
        if len(tab) <= 1:
            return tab
        else:
            milieu = len(tab) // 2
            tab_gauche = [tab[k] for k in range(milieu) ]
            tab_droit = [tab[k] for k in range(milieu, len(tab)) ]
            return interclassement(tri_fusion(tab_gauche), tri_fusion(tab_droit))
    ```



#### 2.2.3 Visualisation

Une erreur classique avec les fonctions récursives est de considérer que les appels récursifs sont simultanés. Ceci est faux !
L'animation suivante montre la progression du tri :

![](data/gif_fusion.gif)


Il est aussi conseillé d'observer l'évolution de l'algorithme grâce à [PythonTutor](https://pythontutor.com/python-debugger.html#mode=edit){target="_blank"} , en exécutant l'instruction `print(tri_fusion([4,8,3,2,7,5,1]))`.



### 2.3 Complexité

La division par 2 de la taille du tableau pourrait nous amener à penser que le tri fusion est de complexité logarithmique, comme l'algorithme de dichotomie. Il n'en est rien.

En effet, l'instruction finale ```interclassement(tri_fusion(tab_gauche), tri_fusion(tab_droit))``` lance **deux** appels à la fonction ```tri_fusion``` (avec certes des données d'entrée deux fois plus petites) et la fonction `interclassement` a elle-même une **complexité linéaire**.

On peut montrer que :

!!! tip "Complexité du tri fusion"
    L'algorithme de tri fusion a une complexité en $O(n \log n)$.

    On dit qu'il est **semi-logarithmique**.

Une complexité semi-logarithmique (en $O(n \log n)$) se situe «entre» une complexité linéaire (en $O(n)$) et une complexité quadratique (en $O(n^2)$).

![image](data/comparaison.png){: .center}


## ^^3. Exercices^^

* Exercices d'épreuves pratiques sur les tris : [carnet Jupyter](EP_Algo_Tris.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac n°1 : à faire sur [feuille](SujetBac_2022J1AmSud.pdf)

## 4. Crédits
Très largement inspiré de la page de cours de [G. Lassus](https://glassus.github.io/terminale_nsi/T3_Algorithmique/3.1_Diviser_pour_regner/cours/#4-le-tri-fusion).