# Rappels de Première : les algorithmes gloutons :boar:


## ^^1. Le principe glouton^^

Pour les rappels du cours de Première : c'est [ici](https://sallardpa.forge.apps.education.fr/premiere-nsiAlgorithmique/Gloutons/Algo_Gloutons_Intro/).


## ^^2. Un exemple d'algorithme glouton: le rendu de monnaie^^

Pour les rappels du cours de Première : c'est [ici](https://sallardpa.forge.apps.education.fr/premiere-nsiAlgorithmique/Gloutons/Algo_Gloutons_RenduMonnaie/).

## ^^3. Exercices et TP^^

- Télécharger le [carnet Jupyter](TP_algo_gloutons.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}

- Exercice type Bac (coloration d'une carte selon un algorithme glouton, en Programmation Orientée Objet) : télécharger le [carnet Jupyter](ExoBac_AlgoGlouton_Coloriage.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}