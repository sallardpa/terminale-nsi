# Recherche dichotomique dans un tableau trié :broken_heart:

## ^^1. Rappels du cours de Première^^

Pour le cours complet de Première : c'est [ici](https://sallardpa.forge.apps.education.fr/premiere-nsiAlgorithmique/pageDeGarde/).

!!! info "Rappel du principe de la recherche dichotomique d'une valeur dans un tableau trié"
    Le but est de déterminer si une valeur `val` est présente dans un **tableau trié**.

    1. On commence par comparer la valeur `val` avec la valeur située au milieu du tableau :

    * Si la valeur recherchée est plus petite, on peut restreindre la recherche à la première moité du tableau.
    * Sinon, on la restreint à la seconde moitié du tableau.

    2. Et on recommence, mais avec une partie du tableau deux fois plus petite. 



Voici en pseudo-code l'algorithme de recherche par dichotomie :

    FONCTION Recherche_Dichotomique(tab,val)
        debut ← 0  # choix de numéroter à partir de zéro les cases du tableau
        fin ← longueur(tab) - 1
        TANT_QUE debut ⩽ fin
            milieu ← (debut + fin) // 2
            SI tab[milieu] = val ALORS
                 RENVOYER VRAI
            SINON
                SI val > tab[milieu] ALORS
                    debut ← milieu + 1
                SINON
                    fin ← milieu - 1
                FIN_SI
            FIN_SI
        FIN_TANT_QUE
        RENVOYER FAUX



L'algorithme de recherche élaboré ci-dessus applique le principe ***"diviser pour régner"*** : à chaque étape (si la valeur n'a pas encore été trouvée), la charge de travail est **divisée par deux**.

Inversement, si on multiplie par 2 la taille du tableau, il n'y aura besoin que d'un tour de boucle supplémentaire pour y effectuer la recherche d'une valeur. 

Puisqu'il ne faut qu'une étape pour faire une recherche dans un tableau de taille 2 (_vérifiez-le !_), on peut dresser le tableau suivant :

<figure markdown>
| Taille du tableau | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 |
|---------------------------------------------------|---|---|---|----|----|----|-----|-----|
| Nombre maximal de tours de boucle WHILE  | 1 | 2 | 3 | 4  | 5  | 6  | 7   | 8   |
</figure>

On voit ainsi apparaitre le lien mathématique entre ces deux quantités : la taille $t$ du tableau est égale à $2^n$ où $n$ est le nombre de tours de boucle `while`. 

Or ce que l'on cherche à connaitre, c'est la relation _"inverse"_, à savoir le nombre d'étapes $n$ en fonction de la taille $t$. 
En mathématiques, on parle de "fonction réciproque" et la fonction réciproque de la fonction $n\mapsto 2^n$ s'appelle la fonction ***logarithme de base 2***.

Elle se note $\log_2$ (en mathématiques) et `log2` en Python (avec le module `math`) : on a donc $\log_2(8) = 3$, $\log_2(16) = 4$, etc. 


!!! tip "Complexité logarithmique"
    Le nombre maximal de tours de boucle de l'algorithme de recherche s'écrit alors $n = \log_2(t)$ : on dit que la recherche dichotomique est un algorithme de **complexité logarithmique**. 
    
    Il est donc __extrêmement efficace__, bien plus efficace qu'un algorithme de complexité linéaire.  


!!! warning ""
    Mais n'oublions pas que l'utilisation d'une recherche dichotomique nécessite que le tableau soit trié, et que le tri d'un tableau est une opération coûteuse (en temps). _On ne doit donc utiliser cet algorithme que dans le cas où l'on est dans une situation où l'on dispose d'un tableau déjà trié._

## ^^2. Terminaison de l'algorithme, notion de "variant de boucle"^^

:confused: Est-on sûr que l'algorithme va se terminer ? 

La boucle `while` qui est utilisée doit nous inciter à la prudence : il y a en effet le risque de rentrer dans une boucle infinie :scream:.  

Pourquoi n'est-ce pas le cas ?


* La condition de la boucle `while` est `debut <= fin `, qu'on aurait aussi pu écrire `fin - debut >= 0`.
* On est assuré de rentrer dans la boucle `while` car au début de programme, on a `debut = 0` et `fin = len(tab) - 1`.
* Ensuite, à chaque étape, les deux variables `debut` et `fin` vont se **rapprocher** jusqu'à ce que :
    
    * ou bien le programme rencontre un `return` 
    * ou bien jusqu'à ce que `fin` devienne inférieur à `debut`.  
    
    Ceci nous assure qu'on va bien finir par sortir de la boucle `while` et que le programme va bien se terminer.

!!! tip "Variant de boucle :heart:"
    On dit que la valeur `fin - debut ` représente le **variant de boucle** de cet algorithme.

    Un variant de boucle est un nombre entier, d'abord strictement positif, puis qui va décroître jusqu'à la valeur 0, et qui va arrêter une boucle `while` quand il atteint zéro ou devient négatif.


## ^^3. L'algorithme de recherche par dichotomie en version récursive^^

Il est possible d'écrire une version **récursive** de l'algorithme de recherche par dichotomie. 

Le principe est le même (calcul de `milieu`, etc.), mais il faut rajouter dans les paramètres de la fonction les indices `debut` et `fin` qui délimitent la zone de recherche de la valeur `val` dans le tableau trié `tab` afin que les appels récursifs se fassent avec des zones de recherche de plus en plus petites.

!!! question "Algorithme récursif de recherche par dichotomie"
    Compléter les points de suspension du code ci-dessous qui met en œuvre un schéma récursif :

    {{IDE("data/dicho_recursif")}}

??? danger "Solution"
    Le code complété est le suivant :

    ```python
    def recherche_dicho_recursif(tab, val, debut, fin):
        if debut > fin :
            return False
        milieu = (debut + fin) // 2
        if val == tab[milieu] :
            return True
        else :
            if val > tab[milieu] :
                return recherche_dicho_recursif(tab, val, milieu+1, fin)
            else :
                return recherche_dicho_recursif(tab, val, debut, milieu-1)
    ```


## ^^4. Exercices^^

* Exercices d'épreuves pratiques sur la recherche dichotomique : [carnet Jupyter](EP_Algo_Dichotomie.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac n°1 : à faire sur [feuille](SujetBac_2021J1ME3.pdf)