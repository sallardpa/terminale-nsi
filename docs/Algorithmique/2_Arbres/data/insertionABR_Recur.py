#--- HDR ---#
class Noeud:
    def __init__(self,val = None, sag = None, sad = None) :
        self.valeur = val
        self.left = sag
        self.right = sad

def parcoursInfixe(arbreBinaire) :
    """Décrit le parcours infixe d'un arbre binaire
    Entrée : un arbre binaire codé par un objet de la classe Noeud
    Sortie : rien, juste un affichage"""
    if arbreBinaire is None :
        return None
    parcoursInfixe(arbreBinaire.left)
    print(arbreBinaire.valeur, end = "-" )
    parcoursInfixe(arbreBinaire.right)
#--- HDR ---#
def insertion_recur(abr, val) :
    """Insère la valeur val dans l'ABR codé à l'aide d'un objet de la classe Noeud"""
    if abr is None :
        return Noeud(val)
    else :
        if val <= abr.valeur :
            abr.left = insertion_recur(abr.left, val)
        else:
            abr.right = insertion_recur(abr.right, val)
        return abr

# un test
monABR = Noeud(25,Noeud(19, Noeud(7), Noeud(24)),Noeud(37, Noeud(29), Noeud(52)))
newABR = insertion_recur(monABR, 20)
print("parcours infixe de l'ABR après ajout de la valeur 20 :")
parcoursInfixe(newABR)
newABR = insertion_recur(newABR, 40)
print("\nparcours infixe de l'ABR après ajout de la valeur 40 :")
parcoursInfixe(newABR)