# interface d'une pile
class Pile :
    def __init__(self) :
        self.contenu = []
    def __str__(self) : # pour faire un print de cet objet
        message ="Voici la pile :\n"
        for k in range(len(self.contenu)-1, -1,-1) :
            message += str(self.contenu[k])+"\n"
        return message
        
def creer_pile_vide() :
    return Pile()

def est_vide(pile):
    return len(pile.contenu) == 0
    
def empiler(pile, element) :
    pile.contenu.append(element)
    return None

def depiler(pile) :
    return pile.contenu.pop(len(pile.contenu)-1)

### début de l'exercice

# exemple de la question 1
P = creer_pile_vide()
empiler(P,8)
empiler(P,5)
empiler(P,2)
empiler(P,4)
print(P)

# question 2.1
def hauteur_pile(P):
    pass # à remplacer par votre code

# test
# print("hauteur de la pile :", hauteur_pile(P))

# question 2.2

def max_pile(P, i ) :
    pass # à remplacer par votre code

# test
# print("position du maxi de la pile  avec i=2 :", max_pile(P,2))

# question 3
def retourner(P , j ):
    pass # à remplacer par votre code

# test
#retourner(P, 3)
#print("Après retournement : ", P)

#  question 4 :
def tri_crepes(P):
    pass # à remplacer par votre code
    
P1 = creer_pile_vide()
empiler(P1,5)
empiler(P1,9)
empiler(P1,3)
empiler(P1,6)
print("Avant tri :", P1)
#tri_crepes(P1)
print("Après tri :", P1)

P2 = creer_pile_vide()
empiler(P2,8)
empiler(P2,5)
empiler(P2,12)
empiler(P2,14)
empiler(P2,7)
print("Avant tri :", P2)
#tri_crepes(P2)
print("Après tri :", P2)


    