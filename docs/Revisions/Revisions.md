# Révisions :ghost:

1. Révisions sur la structure de données `Liste Chainée` 

    * [Énoncé](Revisions_ListesChainees.pdf), à taiter d'abord sur feuille
    * puis compléter les codes du [fichier Python](Revisions_ListesChainees.py)

2. Révisions sur le routage : [énoncé](Revisions_Routage.pdf).

3. Révisions sur le langage SQL : [énoncé](feuille-tp-sql.pdf) et ouvrir dans [Basthon SQL](https://notebook.basthon.fr/?kernel=sql){target="_blank"} le fichier de la base de données remis par votre professeur.

4. Révisions sur la Programmation Orientée Objet :

    * [Énoncé : gestion d'un gite de vacances](Revisions_POO_2.pdf) à faire d'abord sur feuille puis à tester sur le [carnet Jupyter](Revisions_POO_2.ipynb).

5. Révisions sur les dictionnaires : [énoncé](Revisions_Dictionnaires.pdf) à faire d'abord sur feuille puis à tester sur le [carnet Jupyter](Revisions_Dictionnaires.ipynb).

6. Révisions sur la gestion des processus : 
    * [Énoncé : programmation d'un Tourniquet](Revisions_Processus_POO.pdf) à faire d'abord sur feuille puis à tester sur le [carnet Jupyter](Revisions_Processus_POO.ipynb).

7. Révisions sur la structure de données de `Pile` : 

    * [Énoncé n°1 : le crêpier psycho-rigide](Revisions_Piles.pdf) à faire d'abord sur feuille puis à tester avec le [fichier Python](Revisions_Piles.py).
    * [Énoncé n°2 : les tours de Hanoi](Revisions_Hanoi_Piles.pdf) à faire d'abord sur feuille puis à tester sur le [carnet Jupyter](Revisions_Hanoi_Piles.ipynb).

8. Révisions sur les arbres binaires : [énoncé](Revisions_ArbresBinaires.pdf) à faire d'abord sur feuille puis à coder dans le [fichier Python](Revisions_ArbresBinaires.py).