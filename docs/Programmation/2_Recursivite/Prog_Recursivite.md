# Récursivité 🪆

## ^^1. Le principe récursif sur un exemple^^

Voici le code d'une fonction qui donne la somme de tous les nombres entiers jusqu'à un certain nombre entier `n` :

```python 
# Code 1
def somme(n):
    s = 0
    for k in range(1,n+1) :
        s = s + k
    return s
```

Sans lancer le programme, on peut faire des premiers calculs de tête :

- le calcul de `somme(2)` est facile : ça vaut 1+2 = **3** ;
- connaissant `somme(2)`, le calcul de `somme(3)` est également simple : ça vaut **3**+3 = **6** ;
- connaissant `somme(3)`, le calcul de `somme(4)` est également simple : ça vaut **6**+4 = **10** ;
- connaissant `somme(4)`, le calcul de `somme(5)` est également simple : ça vaut **10**+5 = **15** ;
- et ainsi de suite.

On comprend que, pour tout entier n ≥ 1, on a la relation `somme(n) = n + somme(n-1)`. Et bien évidemment, `somme(0)` vaut 0.

!!! tip "Premier exemple de fonction récursive :heart:"
    
    Il est possible de programmer en Python la fonction `somme` en utilisant la relation `somme(n) = n + somme(n-1)` :

    ```python 
    # Code 2
    def somme(n):
        if n == 0 :
            return 0
        else :
            return n + somme(n-1)
    ```

!!! info "Remarques" 
    - le bloc `if n == 0 : return 0` s'appelle le **cas de base** ; 
    - la partie `else : return n + somme(n-1)` constitue ce qui s'appelle un **appel récursif** de la fonction `somme`.

L'exécution de cette fonction génère des appels récursifs successifs qu'il faut

- "empiler" dans la mémoire de l'ordinateur, 
- et ensuite exécuter dans le sens inverse, puisque chaque appel doit attendre le résultat de la requête qu'il a engendrée avant de pouvoir être exécuté. 

Dès qu'une réponse est disponible, elle est utilisée pour remonter en dépilant les appels en attente. Ce processus est illustré par le diagramme suivant qui donne l'exemple de l'exécution de `somme(4)`.

![](data/schema_recur.png){: .center}


Pour bien visualiser : copier le code n°2 sur la page [Recursion Visualizer](https://www.recursionvisualizer.com/){target="_blank"} et ajouter un appel `somme(4)` dans la case juste en-dessous.

## ^^2. Notion de récursivité^^

!!! tip "Notion de récursivité"

    On constate que, dans l'exemple ci-dessus, la fonction `somme`  ***s'appelle elle-même***. 
    
    Cette technique de programmation, où une fonction s'appelle elle-même, s'appelle la **récursivité**.


![](data/matreshka.png){: .center width=60%}

Normalement, ce **paradigme de programmation** (cette manière de concevoir)  est utilisé quand on peut résoudre un problème en le "découpant" en une ***version plus petite du même problème***. Comme le problème devient plus petit, le processus se terminera avec un problème basique qui sera résolu facilement (c'est le _cas de base_).


Par opposition, une fonction qui n'est pas récursive (qui n'utilise que des boucles, sans jamais s'appeler elle-même) est dite **itérative** : le code n°1 correspond à une fonction itérative et le code n°2 à une fonction récursive.


Sur cet exemple, on ne peut pas dire que l'une des deux fonctions est plus simple à coder que l'autre. On a simplement deux possibilités correspondant à deux façons de programmer différentes, à deux _paradigmes de programmation différents_.

## ^^3. Un exemple pédagogique^^

!!! warning "L'erreur classique : l'oubli du cas de base (_d'après un exercice du Bac_)"
    On considère la fonction `suite(n)` qui reçoit un entier positif et renvoie un entier.
    ```python
    def suite(n) :
        if n == 0 :
            return 0
        else :
            return 1+2*suite(n-2)
    ```

    Que répondriez-vous aux questions suivantes :

    * Quelle valeur renvoie l’appel de `suite(6)` ?
    * Que se passe-t-il si on exécute `suite(7)` ?

    {{IDE()}}

??? danger "Explications"
    * pour le calcul de `suite(6)`, on a les appels suivants :
    ```mermaid
    sequenceDiagram
        suite(6)->>+suite(4): 1+2×suite(4)
        suite(4)->>+suite(2): 1+2×suite(2)
        suite(2)->>+suite(0): 1+2×suite(0)
        suite(0)-->>-suite(2): 1+2×0 = 1
        suite(2)-->>-suite(4): 1+2×1 = 3
        suite(4)-->>-suite(6): 1+2×3 = 7
    ```
    donc `suite(6)` vaut 7.

    * On remarque que les nombres passés en paramètre diminuent de 2 en 2 : pour le calcul de `suite(7)`, on a besoin de `suite(5)`, puis de `suite(3)`, puis de `suite(1)`, puis de `suite(-1)`, etc. ! On tombe sur une boucle infinie : le code a été mal écrit, **il manque un cas de base** du type `if n == 1 : return 1`.

## ^^4. Exercices et TP^^


#### Pour tout le monde :

* Généralités : télécharger le [carnet Jupyter](Prog_Recursivite_TP.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac n°1 : [énoncé](SujetBac_BasesRecursivite.pdf)

#### Pour les rapides :

* Généralités (avancé) : télécharger le [carnet Jupyter](Prog_Recursivite_TP_advanced.ipynb), 


* Exercice type Bac n°2 : [énoncé](Prog_Recursivite_SujetBac_22NSIJ2G11.pdf), à faire sur papier puis vérifier vos réponses avec un IDE (Thonny, Spyder, carnet Jupyter, etc.).

* Exercice type Bac n°3 :

    * [énoncé](Prog_Recursivite_SujetBac_22J1PO1.pdf), à faire d'abord sur papier ;
    * [codage sur un carnet Jupyter](Prog_Recursivite_SujetBac_22J1PO1.ipynb), pour tester vos réponses et visualiser le résultat.

* Exercice type Bac n°4 :

    * [énoncé](Prog_Recursivite_SujetBac_21Sujet0.pdf), à faire d'abord sur papier ;
    * [codage sur un carnet Jupyter](Prog_Recursivite_SujetBac_21Sujet0.ipynb), pour tester vos réponses et visualiser le résultat.



