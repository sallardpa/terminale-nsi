class Pistolet :
    def __init__(self, nbBalles) :
        self.nombreBalles = nbBalles
        
    def affichage(self) :
        print("il y a ", self.nombreBalles, " balles dans le pistolet")
        
    def tirer(self) :
        self.nombreBalles = self.nombreBalles - 1
        print("Je tire")
        self.affichage()
        
    def recharger(self) :
        self.nombreBalles = 6
        print("Je recharge")
        self.affichage()

# Corps du programme
monPistolet = Pistolet(0) # on initialise un pistolet avec 0 balles
monPistolet.recharger()
for tir in range(3) :
    monPistolet.tirer()