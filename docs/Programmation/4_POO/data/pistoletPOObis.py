class Pistolet :
    def __init__(self, nbBalles) :
        self.nombreBalles = nbBalles
    
    def get_quantitéBalles(self) :
        return self.nombreBalles
    
    def __str__(self) :
        message =  "il y a " + str(self.nombreBalles) + " balles dans le pistolet"
        return message
        
    def tirer(self) :
        self.nombreBalles = self.nombreBalles - 1
        print("Je tire")
        
    def recharger(self, nouvelleCharge) :
        self.nombreBalles = nouvelleCharge
        print("Je recharge")

# Corps du programme
monPistolet = Pistolet(2) # on initialise un pistolet avec 2 balles
print(monPistolet)
monPistolet.tirer()
print(monPistolet)
monPistolet.recharger(10)
print(monPistolet)