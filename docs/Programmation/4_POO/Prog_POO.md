# Programmation Orientée Objet :gun:

## ^^1. Introduction :^^ comment gérer un pistolet dans un programme Python ?

![](data/revolver.jpg){: .center width=60%}

Supposons que, dans un jeu vidéo, vous deviez écrire un code qui représente l'utilisation d'un pistolet par un personnage.

Le cahier des charges est le suivant : 

* le pistolet a un certain nombre de balles dans son chargeur, 
* il peut tirer des balles une par une,
* et il peut être rechargé, ce qui consiste à avoir un total de 6 balles dans le chargeur .

### 1.1. Une première méthode 

Une première idée de code Python est le suivant : 

{{ IDE("data/pistoletClassique")}}


!!! note "Analyse du code"

    Ici, le programmeur a choisi d'utiliser une variable `nbBallesPistolet` de type `int` et deux fonctions `tirerUneBalle` et `recharger` pour modifier la valeur de cette variable.
    
    Ce code est acceptable : il respecte le cahier des charges.

    Par contre, on peut lui trouver un gros défaut : ce n'est pas vraiment un pistolet qui est géré, mais plutôt un nombre (de type `int`) de balles. **On ne voit pas bien l'objet Pistolet** dans ce code.  

    Autre défaut : le manque de clarté de l'instruction `nbBallesPistolet = tirerUneBalle(nbBallesPistolet)` qui simule un tir.

### 1.2. Une deuxième méthode

Une deuxième idée est d'utiliser quelque chose de nouveau pour vous : une **classe** que l'on nomme `Pistolet` :<a id="PistoletPOO"></a>

{{ IDE("data/pistoletPOO")}}


!!! note "Analyse du code"

    À l'intérieur de cette classe, on trouve :

    * une fonction `__init__` qui sert lors de la création (_initialisation_) de l'objet Pistolet et qui définit une variable interne `nombreBalles` en lui affectant la valeur de la variable `nbBalles` passée en paramètre ;

    * et trois fonctions `affichage`, `tirer` et `recharger`, qui permettent respectivement l'affichage de la valeur de la variable interne `nombreBalles` et la modification de la valeur de cette variable.

    Le mot-clé `self` revient souvent : il fait référence à l'objet lui-même ( :flag_gb: :flag_us: _self_ = lui-même ), qui est en train d'être construit.

    On pourra remarquer que le **corps du programme est beaucoup plus compréhensible** que celui du code n° 1.

Vous venez de voir un premier exemple de **Programmation Orientée Objet** : c'est un nouveau ***paradigme de programmation*** (ou méthode de programmation).

La méthode de programmation utilisée jusqu'à présent (comme celle du code n° 1) s'appelle **programmation procédurale** car elle repose sur l'utilisation de **fonctions**, qu'on appelle parfois aussi **procédures**.

## ^^2. Vocabulaire de la Programmation Orientée Objet (POO)^^

La POO a son propre vocabulaire, qu'il faut connaître :

* une **classe** est une sorte de moule à partir duquel sont produits les objets qu'on appelle les **instances** de la classe. 

Dans notre code n°2, la classe s'appelle `Pistolet` et on a créé une seule instance de cette classe : la variable `monPistolet`. Vous pouvez vérifier qu'on a bien créé une variable d'un type nouveau, en saisissant dans le terminal la commande `type(monPistolet)`.

{{ terminal()}}

* la variable interne  `nombreBalles` est un **attribut** de la classe `Pistolet`.

* les fonctions internes `affichage`, `tirer` et `recharger` sont des **méthodes** de la classe `Pistolet`.

* la fonction `__init__` est appelée **méthode constructeur**.

<!-- caractère Unicode Bonne Réponse : ✅
caractère Unicode Mauvaise Réponse : ❌ -->

!!! question "Exercice"

    On considère le code suivant :

    ```python
    class Personnage :
        def __init__(self, nom , vivant ) :
            """nom est de type str, vivant est de type bool"""
            self.nomPersonne = nom
            self.estVivant = vivant
    
        def mourir(self) :
            self.estVivant = False
        
        def affichage(self) :
            if self.estVivant :
                print("je m'appelle ", self.nomPersonne, " et je suis vivant")
            else :
                print("je m'appelle ", self.nomPersonne, " et je suis mort")
    ```
    

    **1.** Que fait ce code ?

    === "Choisir la bonne réponse"
        - [ ] il définit une classe Personnage
        - [ ] il crée une instance Personnage
        - [ ] il définit une méthode Personnage
        - [ ] il définit un attribut Personnage
        - [ ] il construit un Personnage

    === "Solution"
        - ✅ il définit une classe Personnage
        - ❌ il crée une instance Personnage
        - ❌ il définit une méthode Personnage
        - ❌ il définit un attribut Personnage
        - ❌ il construit un Personnage
    
    **2.** Quels sont les attributs de cette classe  ?

    === "Choisir la ou les bonne(s) réponse(s)"
        - [ ] nom
        - [ ] vivant
        - [ ] nomPersonne
        - [ ] estVivant

    === "Solution"
        - ❌ nom
        - ❌ vivant
        - ✅ nomPersonne
        - ✅ estVivant

    **3.** Quelles sont les méthodes de cette classe  ?

    === "Choisir la ou les bonne(s) réponse(s)"
        - [ ] nomPersonne
        - [ ] mourir
        - [ ] affichage
        - [ ] Personnage

    === "Solution"
        - ❌ nomPersonne
        - ✅ mourir
        - ✅ affichage
        - ❌ Personnage

    **4.** On veut déclarer un objet `sorcier`, instance de cette classe, correspondant à un personnage dont le nom est "Dumbledore" et qui est vivant. Quelle instruction doit-on utiliser ?

    === "Choisir la bonne réponse"
        - [ ] `Personnage(sorcier, "Dumbledore", vivant)`
        - [ ] `Personnage(sorcier, "Dumbledore", True)`
        - [ ] `sorcier = Personnage("Dumbledore", "vivant")`
        - [ ] `sorcier = ("Dumbledore", True)`
        - [ ] `sorcier = Personnage("Dumbledore", True)`

    === "Solution"
        - ❌ `Personnage(sorcier, "Dumbledore", vivant)`
        - ❌ `Personnage(sorcier, "Dumbledore", True)`
        - ❌ `sorcier = Personnage("Dumbledore", "vivant")`
        - ❌ `sorcier = ("Dumbledore", True)`
        - ✅ `sorcier = Personnage("Dumbledore", True)`

    **5.** On veut simuler la mort du personnage `sorcier` créé à la question précédente. Quelle instruction doit-on utiliser ?

    === "Choisir la bonne réponse"
        - [ ] `sorcier.mourir()`
        - [ ] `sorcier.mourir() = True`
        - [ ] `mourir(sorcier)`
        - [ ] `mourir(sorcier) = True`
        - [ ] `estVivant(sorcier) = False`

    === "Solution"
        - ✅ `sorcier.mourir()`
        - ❌ `sorcier.mourir() = True`
        - ❌ `mourir(sorcier)`
        - ❌ `mourir(sorcier) = True`
        - ❌ `estVivant(sorcier) = False`

## ^^3. Consulter et modifier les attributs d'un objet^^

On reprend l'exemple de la classe `Pistolet`.

### 3.1. Consulter la valeur d'un attribut 

Dans le code n°2 ci-dessus, nous avons créé (_instancié_ pour utiliser le vocabulaire de la POO) un objet de la classe `Pistolet` : l'objet `monPistolet`.

!!! tip "Accès direct à un attribut"
    Pour accéder à la valeur de son attribut `nombreBalles`, on peut _cette année encore_ saisir simplement `monPistolet.nombreBalles` : faites-le dans la console ci-dessous.

    {{ terminal() }}


!!! question "Exercice"

    On crée la classe `Fleur` dont les objets ont deux attributs : `nom` et `couleur`.

    En appuyant sur le bouton Lancer, vous allez exécuter un code caché qui crée une instance `fleur1` de cette classe `Fleur`.

    {{ IDE("data/fleur")}}

    {{ multi_qcm(
        [
            "Quel est le nom de `fleur1` ?",
            ["Rose", "Tulipe", "Primevère", "Dahlia"],[3],
        ],
        [
            "Quel est la couleur de `fleur1` ?",
            ["bleue", "rouge", "jaune", "blanche"],[1],
        ]
    )}}

    ??? danger "Explications"
        On saisit dans la console `fleur1.nom` puis `fleur1.couleur` pour accéder directement à ses attributs.

Cet accès direct aux attributs d'un objet fonctionne, mais cela ne respecte pas l'esprit de la Programmation Orientée Objet ! En effet, avec ce paradigme de programmation, on cherche à ***encapsuler*** au maximum les données en distinguant bien :

* les attributs, qui sont un peu comme la partie invisible de l'iceberg
* les méthodes, qui sont ses parties visibles.

!!! tip "Accès à un attribut par une méthode dédiée"
    On privilégie alors la création d'une méthode spécifique qui va ***renvoyer*** la valeur de l'attribut souhaité. Une méthode de ce type est appelée une méthode ***getter*** ( :flag_gb: :flag_us: _to get_ = obtenir).

Par exemple, pour la classe `Pistolet`, on peut ajouter la méthode _getter_ suivante :

```python
    def get_quantitéBalles(self) :
        return self.nombreBalles
```

On utilise alors cette méthode comme ceci :

```python
balles = monPistolet.quantitéBalles() # utilisation d'une méthode getter
print(balles)
```

!!! question "Exercice"
    Recopier alors ci-dessous le [code n°2](#PistoletPOO) et ajouter à la classe `Pistolet` la méthode `get_quantitéBalles`.

    Puis utiliser cette méthode comme montré ci-dessus.  

    {{ IDE() }}

### 3.2. Modifier la valeur d'un attribut

Il est possible, _cette année encore_, de modifier directement la valeur d'un attribut d'un objet déjà créé. Par exemple, l'instruction `monPistolet.nombreBalles = 7` est valide : vous pouvez le vérifier dans la console du code précédent.

Mais là aussi, cela ne respecte pas vraiment le concept d'encapsulation qui sous-tend la Programmation Orientée Objet. On préfère écrire une méthode comme `recharger()` , qui est une méthode qualifiée de ***setter*** puisqu'elle permet, depuis le corps du programme, de modifier la valeur de l'attribut `nombreBalles`.


À noter que cette méthode remet toujours à 6 la valeur de l'attribut `nombreBalles` car c'était notre cahier des charges. 

Si on souhaite maintenant avoir plus de liberté sur le nombre de balles après un rechargement du pistolet, on peut mettre un paramètre `nouvelleCharge` à cette fonction :

```python
    def recharger(self, nouvelleCharge) :
        self.nombreBalles = nouvelleCharge
```

puis passer une valeur en argument quand on utilise cette méthode :

```python
# dans le corps du programme
monPistolet.recharger(10)
```

!!! question "Exercice"
    Recopier le code de la partie précédente et effectuer ces modifications.

    {{ IDE() }}

### 3.3. Affichage de l'objet (_hors-programme_)

Dans notre code de la classe `Pistolet`, il y a beaucoup de `print` : c'est uniquement dans un but pédagogique, pour illustrer le fonctionnement des différentes **méthodes** de cette classe.

En revanche, essayez de faire directement un `print(monPistolet)` dans le corps du programme ou dans la console :

{{ terminal() }}

Rien d'exploitable ne s'affiche :disappointed:

Pour que cela fonctionne et ainsi étendre la fonction `print` à toutes instances de la classe `Pistolet` , on doit ajouter une méthode `__str__` comme dans le code ci-dessous. On en profite alors pour supprimer la méthode `affichage()` qui devient superflue.

{{ IDE("data/pistoletPOObis" )}}

## ^^4. Bilan et synthèse^^

La Programmation Orientée Objet permet de rassembler les données et les méthodes au sein d'une structure _en cachant l'implémentation_ de l'objet : on a ***encapsulé*** les données.

Les ***méthodes*** d'une ***classe***  sont les "services" accessibles aux utilisateurs de l'objet, les "actions" que l'on peut effectuer sur l'objet.

Les ***attributs***, c'est-à-dire les constituants internes de l'objet, sont censés rester "cachés" pour les utilisateurs. :warning: ***Mais pour cette année de Terminale, on n'utilisera que des attributs publics, c'est-à-dire qu'ils seront directement modifiables*** (aux risques et périls de l'utilisateur qui pourra en faire n'importe quoi).

Vous trouverez parfois une représentation schématique d'une classe où, après le nom de la classe, vient le bloc des attributs puis celui des méthodes. Cela constitue l'**interface** de l'objet (voir le chapitre **"Modularité"**) :

```mermaid
classDiagram
    class Pistolet{
      +nombreBalles
      +get_quantitéBalles()
      +tirer()
      +recharger(nouvelleCharge)
    }
```

## ^^5. TP et Exercices^^

* Généralités : télécharger le [carnet Jupyter](TP_Prog_POO.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac n°1 : 

    - [énoncé](Prog_POO_SujetBac_JA1.pdf) à faire d'abord sur papier ;
    - [codage dans un carnet Jypyter](Prog_POO_SujetBac_JA1.ipynb).

* Exercice type Bac n°2 (LaserGame) : 

    - [énoncé et codage dans un carnet Jypyter](Prog_POO_SujetBac_22J1ME1.ipynb).

* Exercice type Bac n°3 (jeu de Bulles): 

    - [énoncé](Prog_POO_SujetBac_22J1LR1.pdf) à faire d'abord sur papier ;
    - puis recopier vos réponses dans ce [fichier Python](data/Bulles_incomplet.py) en le renommant `Bulles.py` ;
    - pour faire fonctionner le jeu de bulles décrit par l'énoncé, on vous fournit [ici](data/Bulles_interface_graphique.py) un programme Python qui **importe** votre programme et qui crée l'animation graphique. Si le code de votre fichier `Bulles.py` est correct, l'animation fonctionnera bien :blush: !

> On retrouve ici le concept de **modularité** des programmes : d'un côté la gestion des fonctionnalités du jeu (votre programme `Bulles.py`), de l'autre côté la gestion graphique.  

* Exercice type Bac n°4 (POO et dictionnaires) : 

    - [énoncé](Prog_POO_Dico_SujetBac_LR1_2023.pdf) à faire d'abord sur papier ;
    - [codage dans un carnet Jypyter](Prog_POO_Dico_SujetBac_LR1_2023.ipynb).

* Exercice type Bac n°5 (tri d'une Pile): cliquer sur [Exo Bac POO + Pile](https://sallardpa.forge.apps.education.fr/terminale-nsiProgrammation/4_POO/Prog_POO_22-PO1-ex4/).