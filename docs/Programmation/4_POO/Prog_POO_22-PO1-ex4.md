---
title: Tri d'une pile
author: Franck Chambon
---
# Exercice Type Bac : Tri d'une pile

> D'après 2022, Polynésie, J1, Ex. 4

Dans cet exercice, les **piles** seront constituées uniquement de nombres entiers. 

La classe `Pile` utilisée dans cet exercice est implémentée en utilisant des listes Python et propose quatre éléments d'interface :

- un constructeur qui permet de créer une pile vide, représentée par `[]` ;
- la méthode `est_vide()` qui renvoie `True` si l'objet est une pile ne contenant aucun élément, et `False` sinon ;
- la méthode `empiler` qui prend un élément quelconque en paramètre et ajoute cet élément au sommet de la pile. Dans la représentation de la pile dans la console, cet élément ^^apparaît à droite^^ des autres éléments de la pile ;
- la méthode `depiler` qui renvoie l'élément présent au sommet de la pile et le retire de la pile.

!!! example "Exemples :"

    ```pycon
    >>> ma_pile = Pile()
    >>> ma_pile.empiler(2)
    >>> ma_pile
    [2]
    >>> ma_pile.empiler(3)
    >>> ma_pile.empiler(50)
    >>> ma_pile
    [2, 3, 50]
    >>> ma_pile.depiler()
    50
    >>> ma_pile
    [2, 3]
    ```

La méthode `est_triee` ci-dessous renvoie `True` si, en dépilant tous les éléments, ils sont traités dans l'ordre croissant, et `False` sinon.

```python linenums="1"
def est_triee(self):
    if not self.est_vide():
        e1 = self.depiler()
        while not self.est_vide():
            e2 = self.depiler()
            if e1 ... e2 :
                return False
            e1 = ...
    return True
```

**1.** Recopier sur la copie les lignes 6 et 8 en complétant les points de suspension.

??? danger "Réponse"

    ```python linenums="1"
    def est_triee(self):
        if not self.est_vide():
            e1 = self.depiler()
            while not self.est_vide():
                e2 = self.depiler()
                if e1 > e2 :
                    return False
                e1 = e2
        return True
    ```


**2.** On crée dans la console la pile `A` représentée par `[1, 2, 3, 4]`.

**2.a.** Donner la valeur renvoyée par l'appel `A.est_triee()`.

??? danger "Réponse"
    La valeur $4$ est d'abord dépilée, puis $3$. L'ordre n'est pas croissant, ainsi `A.est_triee()` renvoie `False`.

**2.b.** Donner le contenu de la pile `A` après l'exécution de cette instruction.

??? danger "Réponse"

    `A` sera représenté par `[1, 2]`.

On souhaite ajouter à la classe `Pile` une méthode `depile_max` qui est applicable à une pile non vide et qui renvoie le plus grand élément de cette pile ^^en le retirant de la pile^^.

Après l'exécution de `p.depile_max()`, le nombre d'éléments de la pile `p` diminue donc de 1.


```python linenums="1"
def depile_max(self):
    assert not self.est_vide(), "Pile vide"
    q = Pile() # initialisation d'une pile vide
    maxi = self.depiler()
    while not self.est_vide():
        elt = self.depiler()
        if maxi < elt:
            q.empiler(maxi)
            maxi = ...
        else :
            ...
    while not q.est_vide():
        self.empiler(q.depiler())
    return maxi
```

**3.** Recopier sur la copie les lignes 9 et 11 en complétant les points de suspension.

??? danger "Réponse"

    ```python linenums="1"
    def depile_max(self):
        assert not self.est_vide(), "Pile vide"
        q = Pile()
        maxi = self.depiler()
        while not self.est_vide():
            elt = self.depiler()
            if maxi < elt:
                q.empiler(maxi)
                maxi = elt
            else :
                q.empiler(elt)
        while not q.est_vide():
            self.empiler(q.depiler())
        return maxi
    ```


**4.** On crée la pile `B` représentée par `[9, -7, 8, 12, 4]` et on effectue l'appel `B.depile_max()`.

**4.a.** Donner le contenu des piles `B` et `q` à la fin de chaque itération de la boucle `while` de la ligne 5.

??? danger "Réponse"
    === "Initialisation"
        Juste avant le premier tour de boucle :

        - `q` est vide ;
        - `maxi` est égal à `4`.
        - `B` contient `[9, -7, 8, 12]` ;

    === "Fin du tour 1"
        - `B` contient `[9, -7, 8]` ;
        - `q` contient `[4]` ;
        - `maxi` est égal à `12`.

    === "Fin du tour 2"
        - `B` contient `[9, -7]` ;
        - `q` contient `[4, 8]` ;
        - `maxi` est égal à `12`.

    === "Fin du tour 3"
        - `B` contient `[9]` ;
        - `q` contient `[4, 8, -7]` ;
        - `maxi` est égal à `12`.
    
    === "Fin du tour 4"
        - `B` est vide ;
        - `q` contient `[4, 8, -7, 9]` ;
        - `maxi` est égal à `12`.

**4.b.** Donner le contenu des piles `B` et `q` avant l'exécution de la ligne 14.

??? danger "Réponse"
    La dernière boucle renverse la pile `q` dans la pile `B`, ainsi, à la ligne 14 :

    - `q` est vide ;
    - `B` contient `[9, -7, 8, 4]`.

**4.c.** Quelle est la valeur renvoyée par l'exécution de  `B.depile_max()` ?

??? danger "Réponse"
    Cela renvoie la valeur de `maxi` à la fin du code, c'est-à-dire ici 12.

**4.d.** Donner un exemple de pile qui montre que l'ordre des éléments restants n'est pas préservé après l'exécution de `depile_max`.

??? danger "Réponse"
    Avec une pile `B` qui contient `[3, 1, 2]`

    === "Initialisation"
        - `B` contient `[3, 1]` ;
        - `q` est vide ;
        - `maxi` est égal à `2`.

        Juste avant le premier tour de boucle

    === "Fin du tour 1"
        - `B` contient `[3]` ;
        - `q` contient `[1]` ;
        - `maxi` est égal à `2`.

    === "Fin du tour 2"
        - `B` est vide ;
        - `q` contient `[1, 2]` ;
        - `maxi` est égal à `3`.

    La dernière boucle renverse la pile `q` dans la pile `B`, ainsi, à la ligne 14 :

    - `q` est vide ;
    - `B` contient `[2, 1]`.

    Sans `3` dans la pile `B` initiale, on a dans l'ordre `[1, 2]` ce qui est différent de `[2, 1]` obtenu ici avec `depile_max`.

    On a ainsi un exemple où l'ordre des éléments restants n'est pas préservé après l'exécution de `depile_max`.


**5.** On ajoute à la classe `Pile` la méthode `traiter` suivante :

```python linenums="1"
def traiter(self):
    q = Pile()
    while not self.est_vide():
        q.empiler(self.depile_max())
    while not q.est_vide():
        self.empiler(q.depiler())
```

**5.a.** On crée la pile `C` représentée par `[1, 6, 4, 3, 7, 2]` et on effectue l'appel `C.traiter()`. Donner les contenus successifs des piles `C` et `q`

- avant la ligne 3 ,
- avant la ligne 5,
- à la fin de l'exécution .

??? danger "Réponse"
    Avec `C = [1, 6, 4, 3, 7, 2]`, un appel `C.traiter()` conduit successivement à :

    - Avant la ligne 3,
        - `C` contient `[1, 6, 4, 3, 7, 2]` ;
        - `q` est vide.
    - Avant la ligne 5,
        - `C` est vide ;
        - `q` contient `[7, 6, 4, 3, 2, 1]`
    - À la fin,
        - `C` contient `[1, 2, 3, 4, 6, 7]`
        - `q` est vide.

**5.b.** Expliquer le traitement effectué par cette méthode.

??? danger "Réponse"
    Ce traitement est un tri de la pile. On retire d'abord les éléments maximums de `self` pour les empiler dans `q` , qui devient alors la pile des éléments de `self` dans l'ordre décroissant. On dépile ensuite la pile `q` dans la pile `self`, qui se retrouve comme si on avait empilé les éléments de `self` dans l'ordre croissant.

    Attention, il s'agit de l'ordre inverse de celui proposé par la fonction `est_triee` vu à la question **1**. Ici, si on dépile les éléments, ils sont désormais dans l'ordre décroissant.

