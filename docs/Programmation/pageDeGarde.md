# Programmation Python 🎮

Cette partie est décomposée en :

* Révisions et compléments :snake:
* Rappels sur le codage des nombres entiers :night_with_stars:
* Modularité des programmes (avec TP Traitement d'images) 🧩
* Récursivité 🪆
* Programmation Orientée Objet :gun:

