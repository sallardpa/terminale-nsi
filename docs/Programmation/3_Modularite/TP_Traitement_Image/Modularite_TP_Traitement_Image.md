# TP : Modularité des programmes & Traitement d'image



## ^^1. Quelques rappels sur le codage des images^^ 

Le graphique ci-dessous illustre la façon dont sont repérés les pixels dans une image. 

![](data/graphique_pixel.png){: .center width 60%}


Dans l'exemple ci-dessus, l'image mesure 800 de large par 600 de haut. Les pixels sont rangés de gauche à droite et de haut en bas. Le pixel en haut à gauche est l'origine. Le pixel (799,0) est donc en haut à droite, et (799,599) en bas à droite.

Vous savez certainement que :

* chaque pixel est codé par 3 nombres du système RVB (RGB en anglais) ;
* chacun de ces 3 nombres est compris entre 0 et 255, et que c'est lié au fait qu'avec 1 octet, soit 8 bits, on peut coder 256 valeurs.

![](data/CodageRGB.png){: .center width 60%}



## ^^2. Objectif du TP : création d'un module de traitement d'image^^ 

L'objectif de ce TP est de créer un **module** de traitement d'images :

* traitement par inversion des couleurs ("négatif" d'une image)

![](data/baboon_posneg.png){: .center width 60%}


* traitement par symétrie selon un axe horizontal (image vue dans un miroir horizontal, ou encore renversement haut/bas)

![](data/watch_hautbas.png){: .center}



* traitement par symétrie selon un axe vertical (image vue dans un miroir vertical, ou encore inversion droite/gauche)

![](data/watch_symetrie.png){: .center width 60%}


L'**interface** de ce module doit être :

| Fonction | Description de la fonction du module |
| :------ | :------- | 
| `negatif(image_in, image_out)` | à partir de l'image `image_in`, crée son négatif `image_out` et renvoie `True` |
| `miroir_horizontal(image_in, image_out)`| crée le symétrique horizontal `image_out` de `image_in` et renvoie `True` |
| `miroir_vertical(image_in, image_out)` | crée le symétrique vertical `image_out` de `image_in` et renvoie `True` |


Un autre programmeur (_votre professeur_) a déjà écrit un programme qui facilite la gestion des entrées/sorties :

- il y a des boutons à cliquer pour choisir le fichier image d'entrée et le type de traitement à appliquer à cette image ;
- ce programme importe un module `NSI_Traitement_Image` que vous devez coder en respectant son interface.

!!! done "Dossier à télécharger"
    Télécharger (_clic droit puis Enregistrer sous_) [le dossier zippé](TP_Traitement_Image.zip) puis extraire tous les fichiers dans votre répertoire du TP.

    On y trouve :

    - quelques images pour tester le code ;
    - le fichier `interface_graphique_Traitement_Image.py` qui gère les entrées/sorties ;
    - et un fichier `exemple_PIL.py` qui donne une petit exemple de traitement d'image.


### ^^3. Découverte de quelques fonctions du module PIL^^

En Python, il existe déjà un module de traitement basique des images : le module PIL.

* Ouvrir le fichier `exemple_PIL.py`, parcourir le code puis l'exécuter.
* En déduire la signification  des codes suivants :

    - `.open` : ...
    - `.size` : ...
    - `.copy` : ...
    - `.save` : ...
    - `.getpixel` : ...
    - `.putpixel` : ...
    

En complétant ces lignes, vous venez de réécrire une partie de l'**interface du module PIL**. Vous pouvez vérifier vos réponses en consultant la [documentation officielle du module](https://pillow.readthedocs.io/en/stable/reference/Image.html#){target="_blank"}.


## ^^4. Création du module NSI_Traitement_Image^^

L'**implémentation** du notre module fera lui-même appel au module PIL de Python.

> Remarque : il est important que la ***syntaxe*** des fonctions de l'interface du module `NSI_Traitement_Image` soit parfaitement ***respectée***, sinon le programme `interface_graphique_Traitement_Image.py` de gestion des entrées/sorties ne fonctionnera pas :scream:. En effet, vous remarquerez l'instruction `import NSI_Traitement_Image`  dans ce programme.


* Créer un nouveau fichier `NSI_Traitement_Image.py` et ajouter au début l'instruction `from PIL import Image`.
* Écrire le code de la fonction `negatif` qui prend en argument deux chaines de caractères `image_in` et `image_out` et dont le rôle est de:

    - produire le négatif de l'image dont le nom de fichier est `image_in`
    - enregistrer ce négatif sous le nom `image_out`
    - renvoyer True (pour signaler une bonne exécution du code) 
    - pour tester votre code, vous pouvez lancer le programme `interface_graphique_Traitement_Image.py`.

```python
def negatif(image_in, image_out):
    pass # compléter ici
    return True
```


* Écrire le code de la fonction `miroir_horizontal` qui prend en argument deux chaines de caractères `image_in` et `image_out` et dont le rôle est de :

    - produire le "miroir horizontal" de l'image dont le nom de fichier est `image_in`
    - enregistrer cette image miroir sous le nom `image_out`
    - renvoyer True

```python
def miroir_horizontal(image_in, image_out):
    pass # compléter ici
    return True
```

* Écrire le code de la fonction `miroir_vertical` qui prend en argument deux chaines de caractères `image_in` et `image_out` et dont le rôle est de :

    - produire le "miroir vertical" de l'image dont le nom de fichier est `image_in`
    - enregistrer cette image miroir sous le nom `image_out`
    - renvoyer True

```python
def miroir_vertical(image_in, image_out):
    pass # compléter ici
    return True
```


## ^^5. Améliorations du module NSI_Traitement_Image^^ 

* Écrire la documentation (_docstring_) de chacune des trois fonctions du module. 
* Taper alors dans la console `help(negatif)`, puis `print(dir(NSI_Traitement_Image))` : vous voyez l'utilité de la bonne documentation de vos fonctions !


## ^^6. Bilan^^


!!! done "On retient que"
    Un partage des tâches entre 

    * vous, qui avez ***implémenté*** le module `NSI_Traitement_Image`,
    * et un autre programmeur, qui a écrit le programme de l'interface graphique,

    permet d'avoir un outil de traitement d'images facilement utilisable et plus convivial qu'un simple code Python.

    ![](data/bilan.png){: .center}

    Le partage des tâches est rendu possible par la **modularité** des programmes Python. 
    Chacun des deux programmeurs est responsable de l'**implémentation** de sa partie de programme. 
    Le bon fonctionnement de l'outil final repose sur une définition claire de l'**interface** du module de traitement d'image.

