pi_approx = 3.14

def perimetre_cercle(rayon) :
    perim = 2*pi_approx*rayon
    return perim

def aire_disque(rayon) :
    aire = pi_approx*rayon**2
    return aire
# corps du programme
r = 5
print ("le perimètre d'un cercle de rayon ", r , " vaut ", perimetre_cercle(r))
print("l'aire d'un disque de rayon ", r , " vaut ", aire_disque(r))