# Gestion des processus :cyclone:


## ^^1. Notion de processus^^

### 1.1. Définition d'un processus


!!! tip "Notion de processus"
    Quand on lance un programme ou une application sur un ordinateur, le système d'exploitation créé un **processus**. 
    On dit que ce processus est une **instance d'exécution** de ce programme.

    Le système d'exploitation alloue à ce processus des **ressources**, notamment du temps de calcul du processeur, de la mémoire vive et un accès aux entrées/sorties (écran, clavier, fichiers de données).

    Le système d'exploitation affecte à chaque processus un numéro appelé **PID** (_Process IDentification_). 



### 1.2. Observation des processus sous Linux

Sur un PC sous Linux, lancer au moins deux programmes, par exemple un navigateur Web (Firefox ou autre) et un éditeur Python (Thonny ou autre).

Puis tester les différentes commandes suivantes.

#### 1.2.1. La commande ```ps``` 

Dans un terminal, la commande ```ps``` va permettre d'afficher la liste des processus actifs. 

> Plus précisément, nous allons utiliser la commande ```ps xjf```, qui permet d'afficher des informations sur l'arborescence des processus.

Exemple :
```console
toto@machine:~$ ps xjf
PPID     PID    PGID     SID TTY  TPGID STAT   UID   TIME COMMAND
2510    2525    2525    2525 ?       -1 Ssl   1000   0:00 xfce4-session
2525    2806    2806    2806 ?       -1 Ss    1000   0:00  \_ /usr/bin/ssh-agent startxfce4
2525    2915    2525    2525 ?       -1 Sl    1000   0:13  \_ xfce4-panel
2915    4213    2525    2525 ?       -1 Sl    1000   7:25  |   \_ firefox
```

Dans cet exemple, on peut lire que le processus de **PID** 4213 correspond au programme `firefox`. Son processus parent, c'est-à-dire le processus qui a permis de le lancer, est donné par son **PPID** (_Parent PID_) : 2915, qui correspond au processus `xfce4-panel`. Il a consommé du temps de calcul du processeur pendant 7 minutes et 25 secondes (TIME) et il est actuellement en sommeil (STAT pour Status : S pour Sleep, R pour Running).


#### 1.2.2. La commande ```top``` 

La commande ```top``` permet de connaître en temps réel la liste des processus, classés par ordre décroissant de consommation de CPU. 

> Avec la commande ```top -i```, ne sont affichés que les processus actifs.

On ferme ```top``` en appuyant sur la touche  ++"q"++ (_quit_).


#### 1.2.3. La commande ```kill``` 
La commande ```kill``` permet de fermer un processus, en donnant son ```PID```  en argument.

Exemple : dans la situation de l'exemple du 1.2.1., la commande ```kill 4213``` provoquera la fermeture de Firefox.


#### 1.2.4. TP sur les commandes Linux


**1.** Ouvrir un premier terminal et créer/ouvrir un fichier `infini.py` grâce à la commande `nano infini.py`. 

Dans l'éditeur `nano`, taper le code Python suivant :
```python
while True :
    print("Hello")
```

Vous aurez compris qu'une boucle infinie est créée, ce qui n'est pas une situation souhaitable !

Sauvegarder avec les touches ++ctrl+"S"++ puis quitter avec ++ctrl+"X"++ 

**2.** Ouvrir un second terminal, vérifier avec la commande `ls` qu'il y a bien le fichier `infiny.py` puis le lancer avec la commande `python3 infini.py`. 

La boucle infinie commence à défiler !

**3.** Revenir dans le premier terminal, afficher la liste des processus avec la commande `ps xjf` et repérer le PID du processus lancé par la commande `python3 infini.py`.

Saisir alors la commande `kill XXXX` (_en remplaçant les XXXX par le PID repéré juste avant_) et vérifier dans le second terminal que la boucle infinie a été stoppée :sweat_smile:.



## ^^2. Ordonnancement^^

### 2.1. L'ordonnancement des processus

Un ordinateur donne l'illusion de réaliser plusieurs tâches simultanément : en réalité, ce n'est pas le cas (nous nous limitons ici au cas d'une machine mono-cœur).

Quand on ouvre plusieurs applications (un navigateur Web, un éditeur de texte, etc.), des processus sont créés par le système d'exploitation et ils sont ensuite en apparence tous «actifs en même temps» (les guillemets sont importants) : on parle de **programmation concurrente** (ils sont en **concurrence** les uns avec les autres pour l'accès aux ressources du système).

Revenons sur l'expression «actifs en même temps», car elle véhicule une fausse idée : ces processus sont bien vivants dans un même laps de temps, mais ils s'exécutent **les uns à la suite les autres** car le processeur ne peut en traiter qu'**un seul à la fois**. Le système d'exploitation procède ainsi à un **ordonnancement** des tâches.

Il existe **plusieurs méthodes d'ordonnancement**, que nous allons détailler sur quelques exemples :

* méthode du "premier arrivé, premier servi" ():flag_gb: :flag_us: FIFO pour _First In First Out_) ;
* méthode "du plus court d'abord" (:flag_gb: :flag_us: SJF, pour _Shortest Job First_) ;
* méthode du tourniquet (:flag_gb: :flag_us: _Round Robin_).

De manière générale, le temps est fractionné en ***temps de cycle du processeur***.

#### 2.1.1. Situation 1

Nous considérons la situation simplifiée de trois processus A, B et C qui sont soumis au processeur dans cet ordre _et au même instant 0_, avec les caractéristiques suivantes :

| Processus | Durée d'exécution |
| :---: | : ---: |
| A | 5 |
| B | 2 |
| C | 3 |


* méthode "premier arrivé, premier servi" : le premier processus arrivé est le premier à être exécuté et ce n'est que quand il est terminé que le processus suivant peut commencer. Puisque les processus ont été soumis au processeur dans l'ordre A, B puis C, on a alors l'ordonnancement suivant :
![](data/ordon2.png){: .center}

* méthode "du plus court d'abord" : à chaque temps de cycle, le processeur choisit d'exécuter le processus dont le temps d'exécution restant est le plus petit. 

![](data/ordon3.png){: .center}

* méthode du tourniquet : comme dans "premier arrivé, premier servi", les processus sont mis dans une file mais la durée d'exécution d'un processus ne peut pas dépasser une durée $Q$ appelée 
***quantum de temps*** et fixée à l'avance. Si le processus a besoin de plus de temps pour terminer son exécution, il doit retourner dans la file et attendre son tour pour poursuivre son exécution.

Avec un quantum $Q$ fixé à 2 temps de cycle, on a alors l'ordonnancement suivant :

![](data/ordon4.png){: .center}


#### 2.1.2. Situation 2

On a maintenant quatre processus A, B, C et D dont les instants d'arrivée et les durées d'exécution sont présentées sur le schéma ci-dessous :

![](data/ordon5.png){: .center}

* méthode "premier arrivé, premier servi" : on a le fonctionnement classique d'une file d'attente, d'où l'ordonnancement suivant :

![](data/ordon6.png){: .center}

* méthode "du plus court d'abord" : c'est **à chaque temps de cycle** que le processus ayant le plus court temps d'exécution restant est choisi parmi ceux qui sont soumis au processeur. Et s'il y a égalité des temps restants, on exécute le processus dont le temps d'arrivée est le plus ancien. D'où l'ordonnancement suivant :

![](data/ordon7.png){: .center}

??? danger "Quelques explications"
    À l'instant 1, le processeur doit choisir entre A (à qui il reste 4 unités de temps à exécuter) et C (3 unités de temps) : il choisit donc C.

    À l'instant 2, le processeur doit choisir entre A (4 unités de temps), C (2 unités de temps) et B (2 unités de temps) : puisque C est plus ancien, il continue avec C.

    Et ainsi de suite.


* méthode du tourniquet : même principe que dans la situation 1, avec la convention supplémentaire que l'entrée dans la file d'un nouveau processus est prioritaire par rapport au retour dans la file d'un processus qui vient de finir son quantum de temps. D'où l'ordonnancement suivant, toujours avec un quantum $Q$ égal à 2 :

![](data/ordon8.png){: .center}

??? danger "Quelques explications"
    À l'instant 1, A est en exécution et C entre dans la file d'attente.

    À l'instant 2 : B entre dans la file d'attente, en priorité sur A qui vient de terminer ses 2 temps d'exécution. La file d'attente est donc composée de A (en queue), B et C (en tête) et C part en exécution.

    À l'instant 3 : D entre dans la file d'attente, qui est donc composée de D (en queue), A et B (en tête).

    À l'instant 4 : C revient en queue de la file et B part en exécution.

    Et ainsi de suite.



#### 2.1.3. Quelques remarques

* Sous Linux, l'ordonnancement est effectué par un système hybride où les processus sont exécutés à tour de rôle (méthode du tourniquet) suivant un ordre de priorité dynamique.

* Dans le cas très fréquent maintenant d'un processeur *multi-cœurs*, le problème d'ordonnancement reste identique. Certes, sur 4 cœurs, 4 processus pourront être traités avec une **réelle** simultanéité mais il reste toujours beaucoup plus de processus à traiter que de cœurs dans le processeur... et un ordonnancement est donc toujours nécessaire.


### 2.2. Les différents états d'un processus

Selon que l'ordonnanceur aura décidé de le confier ou non au processeur pour son exécution, un processus peut donc se trouver dans 3 états :

- **Prêt** : il attend qu'arrive le moment de son exécution.
- **Actif** : il est en cours d'exécution par le processeur.
- **Bloqué** : pendant son exécution (état **Actif**), le processus réclame une ressource qui n'est pas immédiatement disponible. Son exécution s'interrompt. Lorsque la ressource sera disponible, le processus repassera par l'état **Prêt** et attendra à nouveau son tour. 


```mermaid
graph LR
    A[créé] --> B([Prêt])
    B == 1 ===> C([Actif / Running])
    C == 3 ===> E([Bloqué / en attente ])
    C == 2 ==> B
    C --> D[terminé]
    E == 4 ==>  B
    style B fill:#008000
    style C fill:#FF8000
    style E fill:#FF0000
    style A stroke-width:2px, color:#FFFFFF, stroke-dasharray: 5 5
    style D stroke-width:2px, color:#FFFFFF, stroke-dasharray: 5 5
```

!!! info "Légende"

    * Flèche 1 : l'ordonnanceur choisit ce processus et exécute quelques instructions
    * Flèche 2 : l'ordonnanceur passe la main à un autre processus
    * Flèche 3 : le processus se bloque, en attente d'une ressource
    * Flèche 4 : la ressource attendue est libre, le processus se débloque 

**Pourquoi l'accès à une ressource peut bloquer un processus ?**

Pendant son exécution, un processus peut être en attente d'une entrée-utilisateur (un ```input()``` dans un code Python par exemple) ou avoir besoin d'accéder à une ressource déjà occupée (un fichier déjà ouvert par exemple) . Dans ce cas-là, le processeur va passer ce processus à l'état **Bloqué**, pour pouvoir ainsi se consacrer à un autre processus.

Une fois débloqué, le processus va repasser à l'état **Prêt** et rejoindre la file d'attente des processus où il attend son tour pour être exécuté.


!!! question "Exercice"
    
    {{ multi_qcm(
        [
            """
            Lors d'un match, un joueur de football est dans trois états possibles : prêt à jouer sur le banc de touche, en train de jouer sur le terrain, ou exclu temporairement pour 10 minutes (_carton jaune_).

            Si on compare un joueur de foot à un processus d'ordinateur, à quoi correspond l'état `sur le banc de touche` ?
            """,
            ["Prêt", "Actif", "Bloqué"],[1],
        ],
        shuffle = False)}}

## ^^3. Interblocage^^

### 3.1 Définition et exemple
Comme nous venons de le voir, un processus peut être dans l'état bloqué dans l'attente de la libération d'une ressource. 

L'accès à certaines ressources (par exemple, l'accès en écriture à un fichier ou l'accès à la webcam ou à la carte son) ne peut pas être donné à deux processus à la fois. Deux processus souhaitant accéder à cette ressource sont donc en concurrence sur cette ressource. Un processus peut donc devoir attendre (en état Bloqué) qu'une ressource se libère avant de pouvoir y accéder (et alors passer à l'état Prêt).

!!! done "Exemple"
    Considérons deux processus 1 et 2, et deux ressources A et B. Le premier processus s'est accaparé la ressource A et le second la ressource B. Chacun des processus a par ailleurs besoin de la ressource de l'autre pour continuer son exécution. Les deux processus A et B sont donc dans l'état **Bloqué**, chacun en attente de la libération d'une ressource bloquée par l'autre : ils se bloquent mutuellement.


    ![image](data/interblocage.png){: .center}

Cette situation (critique) est appelée **interblocage** ou **deadlock**.



### 3.2 Le deadlock dans la vie courante

* Le carrefour maudit
![image](data/stop.png){: .center width=40%}

* Le chômage éternel
![image](data/job.png){: .center width=30%}

### 3.3. TP Visualisation d'un interblocage

Ce TP a pour but de mettre en pratique un cas d'interblocage sur système Linux. Si votre ordinateur ne tourne pas sur Linux, lancez Putty et demandez à votre professeur un accès au serveur Linux.

**1.** Lancer un terminal dans votre répertoire utilisateur (c'est déjà le cas si vous utilisez le serveur distant).

**2.** Télécharger les deux fichiers Python suivants et les sauvegarder dans votre répertoire utilisateur : [TP_fichier1.py](data/TP_fichier1.py){:target="_blank"} et  [TP_fichier2.py](data/TP_fichier2.py){:target="_blank"}


**3.** Exécuter la commande `ls` et vérifiez que les fichiers `TP_fichier1.py` et `TP_fichier2.py` sont présents.

**4.** Lire le code de ces programmes et conjecturer ce qu'ils font. 

??? info "Contenu de ces fichiers"

    Le code du premier fichier est 
    ```python linenums="1"
    import time
    import fcntl
    fichier1 = "fichierA.txt"
    fichier2 = "fichierB.txt"
    temps_attente = 10
    temps_ouvert = 5

    def ouvrir_fichier(nom_fichier):
        """
        Ouvre le fichier 'nom_fichier' en écriture avec verrouillage
        et renvoie le descripteur de fichier.
        """
        print("Tente d'ouvrir ", nom_fichier," en écriture")
        f = open(nom_fichier, "w")
        fcntl.lockf(f, fcntl.LOCK_EX) # verrouillage
        print(nom_fichier, " ouvert")
        return f

    def fermer_fichier(f):
        """
        Déverrouille et ferme le descripteur de fichier 'f'
        """
        fcntl.lockf(f, fcntl.LOCK_UN)
        f.close()
        return None

    f1 = ouvrir_fichier(fichier1)
    print("Attente de ", temps_attente, " secondes")
    time.sleep(temps_attente)
    f2 = ouvrir_fichier(fichier2)

    time.sleep(temps_ouvert)

    fermer_fichier(f1)
    fermer_fichier(f2)
    ```

??? danger "Réponse"

    Ils ouvrent en écriture les deux mêmes fichiers texte, chacun dans un ordre différent, en attendant 10 secondes entre l'ouverture du premier et l'ouverture du second. L'ouverture du fichier se fait avec un verrouillage du fichier texte.

**5.** Ouvrir un second terminal dans le même répertoire (si vous utilisez le serveur distant, établissez une seconde connexion avec ce serveur).

* Dans le premier terminal, préparez la commande suivante (_sans l'exécuter_) :`python3 TP_fichier1.py`

* Dans le second terminal, préparez la commande suivante (_sans l'exécuter_) : `python3 TP_fichier2.py`

* Exécutez ces commandes à moins de 10 secondes d'intervalle et attendez quelques instants. Qu'observez-vous ?

??? danger "Réponse"

    Un des deux programmes plante  car le système a détecté un cas d'interblocage (`Resource deadlock avoided`). À noter que le fait que l'un des deux programmes plante permet de libérer le verrou d'un des deux fichiers texte et donc le deuxième programme peut se terminer normalement.

## ^^4. Exercices^^


### 4.1. Exercice d'ordonnancement
Dans cet exercice, on prendra modèle sur ce qui est fait en partie 2 pour compléter les schémas de cette [feuille d'exercice](data/fiche_exos_ordo.pdf).

**Question 1** : on considère l'exécution de deux processus A et B selon le schéma temporel suivant :

![](data/exo_gantt1.png){: .center}

Donner l'ordonnancement de l'exécution de ces deux processus selon les méthodes d'ordonnancement suivantes :

* FIFO ("premier arrivé, premier servi")
* SJF ("le plus court d'abord")
* Tourniquet, avec un quantum de temps $Q = 2$.

??? danger "Solution"
    ![](data/exo_gantt1_corrige.png){: .center}


**Question 2** : on considère l'exécution de trois processus A, B et C décrits ci-dessous :

![](data/exo_gantt2.png){: .center}

Donner l'ordonnancement de l'exécution de ces trois processus selon les méthodes d'ordonnancement suivantes :

* FIFO ("premier arrivé, premier servi")
* SJF ("le plus court d'abord")
* Tourniquet, avec un quantum de temps $Q = 2$.

??? danger "Solution"
    ![](data/exo_gantt2_corrige.png){: .center}

**Question 3** : on considère l'exécution de quatre processus A, B, C et D décrits ci-dessous :

![](data/exo_gantt3.png){: .center}

Donner l'ordonnancement de l'exécution de ces quatre processus selon les méthodes d'ordonnancement suivantes :

* FIFO ("premier arrivé, premier servi")
* SJF ("le plus court d'abord")
* Tourniquet,  avec un quantum de temps $Q = 1$.

??? danger "Solution"
    ![](data/exo_gantt3_corrige.png){: .center}

### 4.2.  Exercice type Bac : D'après 2021, Métropole, Candidats Libres, J2

**1.** Les états possibles d'un processus sont : « *prêt* », « *élu* », « *terminé* » et « *bloqué* ».

**1.a.** Expliquer à quoi correspond l'état « *élu* ».

??? danger "Réponse"

    Un processus *élu* est actuellement en cours d'exécution par le processeur.

**1.b.** Proposer un schéma illustrant les passages entre les différents états.

??? danger "Réponse"

    ```mermaid
    flowchart LR
        A(prêt) --> B(élu)
        B --> A
        B --> C(bloqué)
        C --> A
        B ---> D(terminé)
    ```

**2.** On suppose que quatre processus C₁, C₂, C₃ et C₄ sont créés sur un ordinateur,
et qu'aucun autre processus n'est lancé sur celui-ci, ni préalablement ni pendant
l'exécution des quatre processus.

L'ordonnanceur applique une méthode _FIFO_ ("premier arrivé, premier sorti") pour exécuter les différents processus qui sont dans l'état "Prêt".

On suppose que les quatre processus arrivent en même temps dans la file d'attente et y sont placés dans l'ordre C₁, C₂, C₃ et C₄.

- Les temps d'exécution totaux de C₁, C₂, C₃ et C₄ sont respectivement 100 ms, 150 ms, 80 ms et 60 ms ;
  
- Après 40 ms d'exécution, le processus C₁ demande une opération d'écriture disque, opération qui dure 200 ms. Pendant cette opération d'écriture, le processus C₁ passe à l'état bloqué ;

- Après 20 ms d'exécution, le processus C₃ demande une opération d'écriture disque, opération qui dure 10 ms. Pendant cette opération d'écriture, le processus C₃ passe à l'état bloqué.

Sur la frise chronologique ci-dessous, les états du processus C₂ sont donnés. Compléter la frise avec les états des processus C₁, C₃ et C₄.

![Frise à compléter](data/ex2_base.svg)

??? danger "Réponse"

    ![Frise complétée](data/ex2.svg)

**3.** On trouvera ci- dessous deux programmes rédigés en pseudo-code.

Verrouiller un fichier signifie que le programme demande un accès exclusif au
fichier et l'obtient si le fichier est disponible.

| Programme 1             | Programme 2             |
| :---------------------- | :---------------------- |
| Verrouiller fichier_1   | Verrouiller fichier_2   |
| Calculs sur fichier_1   | Verrouiller fichier_1   |
| Verrouiller fichier_2   | Calculs sur fichier_1   |
| Calculs sur fichier_1   | Calculs sur fichier_2   |
| Calculs sur fichier_2   | Déverrouiller fichier_1 |
| Calculs sur fichier_1   | Déverrouiller fichier_2 |
| Déverrouiller fichier_2 |                         |
| Déverrouiller fichier_1 |                         |


**3.a.** En supposant que les processus correspondant à ces programmes s'exécutent "simultanément" (exécution concurrente), expliquer le problème qui peut être rencontré.

??? danger "Réponse"

    Il s'agit d'un problème d'interblocage car les deux processus verrouillent simultanément les fichiers 1 et 2.

**3.b.** Proposer une modification du programme 2 permettant d'éviter ce problème.

??? danger "Réponse"

    On échange simplement les deux premières lignes du programme 2, de sorte que le programme 2 va être en état Bloqué dès le départ et qu'il ne pourra être exécuté que quand le programme 1 sera terminé :

    | Programme 1             | Programme 2             |
    | :---------------------- | :---------------------- |
    | Verrouiller fichier_1   | Verrouiller fichier_1   |
    | Calculs sur fichier_1   | Verrouiller fichier_2   |
    | Verrouiller fichier_2   | Calculs sur fichier_1   |
    | Calculs sur fichier_1   | Calculs sur fichier_2   |
    | Calculs sur fichier_2   | Déverrouiller fichier_1 |
    | Calculs sur fichier_1   | Déverrouiller fichier_2 |
    | Déverrouiller fichier_2 |                         |
    | Déverrouiller fichier_1 |                         |


### 4.3. Exercice type Bac : D'après 2022, Polynésie, J1


Un système est composé de 4 périphériques, numérotés de 0 à 3, et d'une mémoire, reliés entre eux par un bus auquel est également connecté un dispositif ordonnanceur. À l'aide d'un signal spécifique envoyé sur le bus, l'ordonnanceur sollicite à tour de rôle les périphériques pour qu'ils indiquent le type d'opération (lecture ou écriture) qu'ils souhaitent effectuer, et l'adresse mémoire concernée.

Un tour a lieu quand les 4 périphériques ont été sollicités. **Au début d'un nouveau tour, on considère que toutes les adresses sont disponibles en lecture et écriture.**

Si un périphérique demande l'écriture à une adresse mémoire **à laquelle on n'a pas encore accédé** pendant le tour, l'ordonnanceur répond `"OK"` et l'écriture a lieu. Si on a déjà demandé la lecture ou l'écriture à cette adresse, l'ordonnanceur répond `"ATT"` (_Attente_) et l'opération n'a pas lieu.

Si un périphérique demande la lecture à une adresse à laquelle on n'a pas encore accédé en écriture pendant le tour, l'ordonnanceur répond `"OK"` et la lecture a lieu. Plusieurs lectures peuvent avoir donc lieu pendant le même tour à la même adresse.

Si un périphérique demande la lecture à une adresse à laquelle on a déjà accédé en écriture, l'ordonnanceur répond `"ATT"` et la lecture n'a pas lieu.

Ainsi, pendant un tour, une adresse peut être utilisée soit une seule fois en écriture, soit autant de fois qu'on veut en lecture, soit pas utilisée.

Si un périphérique ne peut pas effectuer une opération à une adresse, il demande la même opération à la même adresse au tour suivant.

**1.** Le tableau donné en annexe 1 indique, sur chaque ligne, le périphérique sélectionné, l'adresse à laquelle il souhaite accéder et l'opération à effectuer sur cette adresse. Compléter dans la dernière colonne de cette annexe, à rendre avec la copie, la réponse donnée par l'ordonnanceur pour chaque opération.


??? info "Annexe 1"

    |N° périphérique | Adresse | Opération | Réponse de l'ordonnanceur |
    |:---|:---|:---|:---|
    | 0 | `10` | écriture | `"OK"` |
    | 1 | `11` | lecture  | `"OK"` |
    | 2 | `10` | lecture  | `"ATT"` |
    | 3 | `10` | écriture | `"ATT"` |
    | 0 | `12` | lecture  |  |
    | 1 | `10` | lecture  |  |
    | 2 | `10` | lecture  |  |
    | 3 | `10` | écriture |  |

??? dansger "Réponse"

    |N° périphérique | Adresse | Opération | Réponse de l'ordonnanceur |
    |:---|:---|:---|:---|
    | 0 | `10` | écriture | `"OK"` |
    | 1 | `11` | lecture  | `"OK"` |
    | 2 | `10` | lecture  | `"ATT"` |
    | 3 | `10` | écriture | `"ATT"` |
    | 0 | `12` | lecture  | `"OK"` |
    | 1 | `10` | lecture  | `"OK"` |
    | 2 | `10` | lecture  | `"OK"` |
    | 3 | `10` | écriture | `"ATT"` |

    Il s'agit d'un nouveau tour : les lectures sont possibles, mais l'écriture  demandée par le périphérique n°3 ne l'est pas car on a déjà accédé en lecture pendant ce tour à l'adresse demandée.




On suppose dans toute la suite que :

- le périphérique 0 écrit systématiquement à l'adresse `10` ;
- le périphérique 1 lit systématiquement à l'adresse `10` ;
- le périphérique 2 écrit alternativement aux adresses `11` et `12` ;
- le périphérique 3 lit alternativement aux adresses `11` et `12` ;

Pour les périphériques 2 et 3, le changement d'adresse n'est effectif que lorsque l'opération est réalisée.



**2.** On suppose que les périphériques sont sélectionnés à chaque tour dans l'ordre 0 ; 1 ; 2 ; 3. Expliquer ce qu'il se passe pour le périphérique 1.

??? danger "Réponse"

    - À chaque début de tour, le périphérique 0 demande à écrire à l'adresse `10` ; c'est accepté.
    - Juste après, le périphérique 1 demande à lire à l'adresse `10` ; c'est refusé.

    Le périphérique 1 ne pourra jamais lire l'adresse `10`.

Les périphériques sont sollicités de la manière suivante lors de quatre tours successifs :

- au premier tour, ils sont sollicités dans l'ordre 0 ; 1 ; 2 ; 3 ;
- au deuxième tour, dans l'ordre 1 ; 2 ; 3 ; 0 ;
- au troisième tour, 2 ; 3 ; 0 ; 1 ;
- puis 3 ; 0 ; 1 ; 2 au dernier tour.
- Et on recommence...

**3.a.** Préciser pour chacun de ces tours si le périphérique 0 peut écrire et si le périphérique 1 peut lire.

??? danger "Réponse"

    - **Tour 1** : 0 ; 1 ; 2 ; 3
        - 0 peut écrire, puis
        - 1 ne peut pas lire
    - **Tour 2** : 1 ; 2 ; 3 ; 0
        - 1 peut lire, puis
        - 0 ne peut pas écrire
    - **Tour 3** : 2 ; 3 ; 0 ; 1
        - 0 peut écrire, puis
        - 1 ne peut pas lire
    - **Tour 4** : 3 ; 0 ; 1 ; 2
        - 0 peut écrire, puis
        - 1 ne peut pas lire

**3.b.** En déduire la proportion des valeurs écrites par le périphérique 0 qui sont effectivement lues par le périphérique 1.

??? danger "Réponse"
    - Au tour 1, la valeur écrite par le périphérique 0 sera lue par le périphérique 1 au tour suivant.
    - Au tour 2, rien n'est écrit par le périphérique 0.
    - Au tour 3, la valeur écrite par le périphérique 0 **ne sera jamais** lue par le périphérique 1 ; en effet, une autre écriture intervient avant la prochaine lecture.
    - Au tour 4, la valeur écrite par le périphérique 0 **ne sera jamais** lue par le périphérique 1 ; en effet, une autre écriture intervient avant la prochaine lecture.

    Ainsi, une seule valeur sur trois sera effectivement lue. La proportion est $\frac13$.


On change la méthode d'ordonnancement : on détermine l'ordre des périphériques au cours d'un tour à l'aide de deux listes d'attente `ATT_L` et `ATT_E` établies au tour précédent.


Au cours d'un tour, on place dans la liste `ATT_L` toutes les opérations de lecture mises en attente, et dans la liste d'attente `ATT_E` toutes les opérations d'écriture mises en attente.


Au début du tour suivant, on établit l'ordre d'interrogation des périphériques en procédant ainsi :

- on interroge ceux présents dans la liste `ATT_L`, par ordre croissant d'adresse,
- on interroge ensuite ceux présents dans la liste `ATT_E`, par ordre croissant
d'adresse,
- puis on interroge les périphériques restants, par ordre croissant d'adresse.

**4.** Compléter et rendre avec la copie le tableau fourni en annexe 2, en utilisant l'ordonnancement décrit ci-dessus, sur 3 tours.

!!! info "Annexe 2"

    | Tour | N° périphérique | Adresse | Opération | Réponse ordonnanceur | `ATT_L` | `ATT_E` |
    |:---|:---|:---|:---|:---|:---|:---|
    | 1 | 0 | `10` | écriture | `"OK"`  | vide      | vide |
    | 1 | 1 | `10` | lecture  | `"ATT"` | `(1, 10)` | vide |
    | 1 | 2 | `11` | écriture |         |           |      |
    | 1 | 3 | `11` | lecture  |         |           |      |
    | 2 | 1 | `10` | lecture  |         |           | vide |
    | 2 |   |      |          |         |           |      |
    | 2 |   |      |          |         |           |      |
    | 2 |   |      |          |         |           |      |
    | 3 | 0 | `10` | écriture |         | vide      | vide |
    | 3 | 1 | `10` | lecture  |         |           | vide |
    | 3 | 2 | `11` | écriture | `"OK"`  | `(1, 10)` | vide |
    | 3 | 3 | `12` | lecture  |         |           |      |

??? danger "Réponse"

    | Tour | N° périphérique | Adresse | Opération | Réponse ordonnanceur | `ATT_L` | `ATT_E` |
    |:---|:---|:---|:---|:---|:---|:---|
    | 1 | 0 | `10` | écriture | `"OK"`  | vide      | vide |
    | 1 | 1 | `10` | lecture  | `"ATT"` | `(1, 10)` | vide |
    | 1 | 2 | `11` | écriture | `"OK"`  | `(1, 10)` | vide |
    | 1 | 3 | `11` | lecture  | `"ATT"` | `(3, 11), (1, 10)` | vide |
    | 2 | 1 | `10` | lecture  | `"OK"`  | `(3, 11)` | vide |
    | 2 | 3 | `11` | lecture  | `"OK"`  | vide      | vide |
    | 2 | 0 | `10` | écriture | `"ATT"` | vide      | `(0, 10)` |
    | 2 | 2 | `12` | écriture | `"OK"`  | vide      | `(0, 10)` |
    | 3 | 0 | `10` | écriture | `"OK"`  | vide      | vide |
    | 3 | 1 | `10` | lecture  | `"ATT"` | `(1, 10)` | vide |
    | 3 | 2 | `11` | écriture | `"OK"`  | `(1, 10)` | vide |
    | 3 | 3 | `12` | lecture  | `"OK"`  | `(1, 10)` | vide |



### 4.4. Exercice type Bac : extrait de 2023, Polynésie, J2

- [énoncé](Processus_SujetBac_23NSIJ2PO1.pdf) à faire sur feuille.

---
??? summary "Sources :sparkles:"
    - [Cours de G. Lassus](https://glassus.github.io/terminale_nsi/)
    - [Cours de N. Buyle-Bodin](http://www.mathinfo.ovh/Terminale_NSI/00_Progression/index.html#theme_c_-_materiel_os_et_reseau)
---    
