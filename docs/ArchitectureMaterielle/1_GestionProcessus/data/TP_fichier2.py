import time
import fcntl
fichier1 = "fichierB.txt"
fichier2 = "fichierA.txt"
temps_attente = 10
temps_ouvert = 5

def ouvrir_fichier(nom_fichier):
    """
    Ouvre le fichier 'nom_fichier' en écriture avec verrouillage
    et renvoie le descripteur de fichier.
    """
    print("Tente d'ouvrir ", nom_fichier," en écriture")
    f = open(nom_fichier, "w")
    fcntl.lockf(f, fcntl.LOCK_EX) # verrouillage
    print(nom_fichier, " ouvert")
    return f

def fermer_fichier(f):
    """
    Déverrouille et ferme le descripteur de fichier 'f'
    """
    fcntl.lockf(f, fcntl.LOCK_UN)
    f.close()
    return None

f1 = ouvrir_fichier(fichier1)
print("Attente de ", temps_attente, " secondes")
time.sleep(temps_attente)
f2 = ouvrir_fichier(fichier2)

time.sleep(temps_ouvert)

fermer_fichier(f1)
fermer_fichier(f2)

