---
title: Adressage sur un réseau
---
# Adressage sur un réseau :mailbox_with_mail:


## ^^Introduction : quelques rappels de Première^^

Un terminal (un ordinateur, un smartphone, etc.) peut se connecter à un réseau local (LAN) ou à Internet via différentes **interfaces** : via sa carte Ethernet, via sa carte Wifi, via sa carte 3G/4G/5G, etc. Chacune de ces cartes dispose d'un numéro de série unique au monde, appelée **adresse MAC** (_Media Access Control_) ou encore **adresse physique**, qui permet à un routeur d'identifier de manière unique une machine qui lui est reliée. Une adresse MAC est constituée de 48 bits et elle est habituellement écrite sous la forme de 6 octets (car 6 × 8  = 48) notés en hexadécimal et séparés par des doubles-points : par exemple `5E:FF:56:A2:AF:15`.

Une **adresse IP** est attribuée à une interface à chaque fois qu'elle se connecte à un routeur. L'attribution de l'adresse IP se fait de manière habituellement de façon dynamique (l'adresse IP d'une même machine peut changer d'un jour sur l'autre), par le protocole DHCP (_Dynamic Host Configuration Protocol_). Une adresse IP  (pour être précis : IP version 4, notée _IPv4_ ) est de la forme ` a.b.c.d` avec `a` , `b` , `c`  et `d` des entiers compris entre 0 et 255 inclus. Cela correspond à un codage sur 32 bits, c'est-à-dire sur 4 octets (car 4 × 8 = 32), et chaque octet correspond à l'un des nombres  `a` , `b` , `c`  et `d` .

Par exemple, l'adresse IP `192.168.1.2` correspond au code binaire 11000000.10101000.00000001.00000010.

Un autre protocole (ARP, pour _Address Resolution Protocol_) permet d'associer l'adresse MAC d'une machine donnée à son adresse IP.

!!! tip "Identification de l'adresse MAC et de l'adresse IP en ligne de commande"
    Pour obtenir les informations réseaux de votre machine : 

    - dans un terminal sous Linux, entrer la commande `ip addr`(ou `ifconfig` sur les vieux systèmes et sur Filius)
    - dans l'invite de commande sous Windows, entrer la commande `ipconfig`

!!! done "Exemple sur un PC portable"
    ```console
    toto@machine:~$ ip addr
    1: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
        link/ether 20:FA:84:68:18:7C brd ff:ff:ff:ff:ff:ff
    2: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
        link/ether 2c:d0:a5:90:38:37 brd ff:ff:ff:ff:ff:ff
        inet 192.168.0.25/24 brd 192.168.0.255 scope global dynamic noprefixroute wlan0
    ```
    
    * la ligne 1 commence par le nom de l'interface (`eth` pour une carte Ethernet) puis on y trouve l'adresse MAC de cette carte Ethernet (20:FA:84:68:18:7C). Il n'y a rien qui ressemble à une adresse IP, ce qui signifie que le PC n'utilise pas en ce moment sa carte Ethernet.
    * la ligne 2 donne le nom de l'interface (`wlan` pour une carte Wifi) ; on y trouve l'adresse MAC (2c:d0:a5:90:38:37) ainsi que l'adresse IP de l'interface (192.168.0.25). Le PC est donc connecté en Wifi.

## ^^1. Découverte de la notion de masque de sous-réseau^^

### 1.1. Un premier réseau local (sous-réseau)

Avec le logiciel [Filius](https://www.lernsoftware-filius.de/Herunterladen){:target="_blank"}, créons le réseau local ci-dessous :

![](data/f1.png){: .center}

??? info "Petits rappels sur l'utilisation de Filius"
    - Dans la configuration de chaque ordinateur portable, cliquer sur "Utiliser l'adresse IP comme nom".
    - En mode simulation (_Triangle vert_), faire un clic droit sur le portable d'adresse 192.168.0.1 et sélectionner "Afficher le bureau". Double-cliquer ensuite dans "Installation de logiciels" et choisir "Ligne de commande". 


Testons le ```ping``` de la machine ```192.168.0.1```  vers la machine ```192.168.0.3```.

??? danger "Résultat du ping"
    ![](data/ft1.png){: .center}


### 1.2. Un deuxième réseau local (sous-réseau)

**1.2.1.** Rajoutons un deuxième sous-réseau de la manière suivante (penser à bien renommer les switchs).

![](data/f2.png){: .center}

**Comment relier ces deux sous-réseaux ?**

Une réponse pas si bête : avec un cable entre les deux switchs !

![](data/f3.png){: .center}

Testons cette hypothèse en essayant de pinger la machine ```192.168.1.2``` depuis la machine ```192.168.0.1```.  

??? danger "Résultat du ping"
    ![](data/ft2.png){: .center}
    Cela ne marche pas. L'ordinateur **refuse** d'envoyer le ping vers la machine ```192.168.1.2```.  
    *(spoil : car elle n'est pas dans son sous-réseau)*

Temporairement, renommons la machine ```192.168.1.2``` en ```192.168.0.33```. Testons à nouveau le ping depuis la machine ```192.168.0.1```.
??? danger "Résultat du ping"
    ![](data/ft3.png){: .center}
    Cela marche. Les paquets sont bien acheminés.

On se rend compte que la définition d'un sous-réseau n'est pas seulement une question de câbles : il y a aussi une *règle numérique* que l'on va découvrir.


**1.2.2.**  Dans Filius, lors de l'attribution de l'adresse IP à une machine, une ligne nous permet de spécifier le **masque de sous-réseau** (appelé simplement « Masque » dans Filius). C'est ce masque qui va permettre de déterminer si une machine appartient à un sous-réseau donné ou non, en fonction de son adresse IP.

![](data/f4.png){: .center}

!!! info "Explication basique du fonctionnement d'un masque de sous-réseau"
    <a id="basique"></a>

    * Si le masque est ```255.255.255.0```, toutes les machines partageant les mêmes **trois** premiers nombres de leur adresse IP appartiendront au même sous-réseau. Comme ceci est le réglage par défaut de Filius, cela explique pourquoi  ```192.168.0.33``` et ```192.168.0.1``` sont sur le même sous-réseau, et pourquoi  ```192.168.1.2``` et ```192.168.0.1``` ne sont pas sur le même sous-réseau.
    Dans cette configuration, 256 machines peuvent donc appartenir au même sous-réseau. Pour être précis, on ne peut réellement connecter que 254 machines sur ce sous-réseau car des adresses finissant par 0 ou par 255 sont réservées.

    * Si le masque est ```255.255.0.0```, toutes les machines partageant les mêmes **deux** premiers nombres de leur adresse IP appartiendront au même sous-réseau.  
    Dans cette configuration, 65536 (256 × 256  = 65536) machines peuvent être dans le même sous-réseau, en négligeant les deux adresses réservées. 



- Renommons ```192.168.0.33``` en ```192.168.1.2``` et modifions son masque en ```255.255.0.0```.
- Modifions aussi le masque de ```192.168.0.1``` en ```255.255.0.0```.
- Testons le ping de ```192.168.0.1``` vers ```192.168.1.2```.

??? danger "Résultat du ping"
    ![](data/ft4.png){: .center}
    Cela marche. La modification de la valeur du masque a comme conséquence que les deux machines appartiennent maintenant au même sous-réseau.



## ^^2. Un vrai réseau contenant deux sous-réseaux distincts : la nécessité d'un routeur^^

Notre solution initiale (relier les deux switchs par un cable pour unifier les deux sous-réseaux) n'est pas viable à l'échelle d'un réseau planétaire.

Pour que les machines de deux réseaux différents puissent être connectées, on va utiliser un **routeur** ou **passerelle**. C'est un équipement électronique équipé de **plusieurs cartes réseaux** qui va permettre d'interconnecter plusieurs réseaux locaux.



**2.1.** Reprenons le réseau précédent, avec les modifications suivantes :

- dans la configuration des six ordinateurs, on remet le masque à la valeur par défaut `255.255.255.0`

- on rajoute un **routeur** entre le SwitchA et le SwitchB.
![](data/f5.png){: .center}

**2.2. Configuration du routeur** 

L'interface reliée au Switch A doit avoir une adresse du sous-réseau A. On donne souvent une adresse finissant par ```254```, qui est en quelque sorte la dernière adresse du réseau (en effet l'adresse en ```255``` est l'une des deux adresses réservées ; elle est appelée __adresse de diffusion__ (:flag_gb: :flag_us: _broadcast_) et est utilisée pour _pinger_ en une seule fois l'intégralité d'un sous-réseau). 

On donne donc l'adresse ```192.168.0.254``` pour l'interface reliée au Switch A, et ```192.168.1.254``` pour l'interface reliée au Switch B.  
![](data/flsrouteur.png){: .center}  
Dans l'onglet général, sélectionner « Routage automatique ».  

Ainsi configuré, notre routeur peut jouer le rôle de **passerelle** entre les deux sous-réseaux. Lançons alors un `ping` entre ```192.168.0.1``` et ```192.168.1.2```.

??? danger " Résultat du ping entre ```192.168.0.1``` et ```192.168.1.2```"
    ![](data/ft2.png){: .center}
    Cela ne marche pas. Les paquets sont perdus.


Pourquoi cet échec ? Parce que nous devons dire à chaque machine qu'une passerelle est maintenant disponible pour pouvoir sortir de son propre sous-réseau. Il faut donc aller sur la machine ```192.168.0.1``` et lui donner l'adresse de sa passerelle, qui est ```192.168.0.254```.

![](data/passerelle.png){: .center}

Attention, il faut faire de même pour ```192.168.1.2``` (avec la bonne passerelle...)  
Testons à nouveau le ping... Cette fois cela marche.

Plus intéressant : effectuons un ```traceroute``` entre  ```192.168.0.1``` et ```192.168.1.2```.

![](data/traceroute.png){: .center}

On y aperçoit que la machine ```192.168.1.2``` est atteignable en deux sauts depuis ```192.168.0.1```, en passant par la passerelle ```192.168.0.254```


## ^^3. Retour la notion de masque de sous-réseau^^

Lorsqu'une machine A veut envoyer un message à une machine B, elle doit déterminer si cette machine B :

- appartient au même sous-réseau : auquel cas le message est envoyé directement via un ou plusieurs switchs.
- n'appartient pas au même sous-réseau : auquel cas le message doit d'abord transiter par un routeur.

On a vu dans la simulation sous Filius que c'est le **masque de sous-réseau** qui permet de repérer quelles sont les machines qui appartiennent à un même sous-réseau. Examinons de plus près en quoi consiste ce **masque** et comment il est utilisé.


Prenons le cas d'un masque de sous-réseau égal à `255.255.255.0` : il correspond au code binaire 11111111.11111111.11111111.00000000.

On a dit [plus haut](#basique) que, pour déterminer à quel sous-réseau appartient l'ordinateur d'adresse `192.168.1.2`, il fallait regarder les 3 premiers nombres. Mais un ordinateur ne peut pas _regarder_, il ne sait faire que des _calculs_ ! Il va procéder en faisant une opération bit par bit, selon le principe suivant :

1. On écrit le code binaire de l'adresse IP et celui du masque l'un en-dessous de l'autre :

    | Code binaire   |   Adresse    | 
    |-----------------|---------------|
    | 11000000 10101000 00000001 00000010 |  Adresse IP (192.168.1.2)   | 
    | 11111111 11111111 11111111 00000000 | Masque  (255.255.255.0)     | 

2. On recopie les bits de l'adresse IP uniquement s'il y a un 1 en même position sur le masque, et on met un 0 sinon :

    | Code binaire |  Adresse   | 
    |-----------------|---------------|
    | 11000000 10101000 00000001 00000000 | Adresse du sous-réseau (192.168.1.0)   |

L'adresse obtenue (ici 192.168.1.0) constitue l'adresse du sous-réseau (ou encore _préfixe_ du sous-réseau) : c'est la partie commune des adresses IP de toutes les machines du sous-réseau.

* On comprend pourquoi on parle de ***masque*** : les 0 du masque sont venus ***masquer*** les bits de l'adresse IP alors que les 1 du masque ne les ont pas modifiés.

* Vous vous rendrez facilement compte que cette opération bit par bit est un simple `AND`.

!!! info "Rappel : table de vérité de l'opération AND"

    | Bit IP      | Bit Masque       | Bit IP AND Bit Masque = Bit Sous-réseau  |
    |--------|----------------|-----------------|
    | 0 | 0 | 0 |
    | 1  |  0  | 0 |
    | 0 | 1 | 0 |
    | 1  |  1  | 1 |


!!! question "Exercice : codage en Python"
    En Python, on peut réaliser le `AND` bit par bit directement sur les nombres écrits dans le système décimal grâce à l'opérateur `&`. Par exemple, si on saisit en console 
    `192 & 255`, on obtient 192 ; et si on saisit `192 & 0`, on obtient 0.

    {{ terminal()}}

    Compléter les lignes 3 et 4 du script ci-dessous pour que la fonction `calcul_adresse_reseau` renvoie une liste contenant les 4 nombres correspondant à l'adresse réseau de la machine dont l'adresse IP et le masque sont donnés en argument sous forme de listes.

    {{ IDE('data/adresseMasque')}}

??? danger "Solution"
    On écrit les lignes 3 et 4 ainsi :
    ```python
    for k in range(len(adresseIP)) :
        adresse_reseau.append(adresseIP[k] & masque[k])
    ```

## ^^4. Écriture des masques de sous-réseau : notation CIDR^^

D'après ce qui précède, deux informations sont nécessaires pour déterminer le sous-réseau auquel appartient une machine : son adresse IP et le masque de sous-réseau. 

Une convention de notation permet d'écrire simplement ces deux renseignements : la notation CIDR (_Classless Inter Domain Routing_).

!!! done "Exemple de notation CIDR"
    Une machine d'IP ```192.168.0.33``` avec un masque de sous-réseau ```255.255.255.0``` sera désignée par ```192.168.0.33/24``` en notation CIDR.

    Le suffixe ```/24``` signifie que le masque de sous-réseau commence par 24 bits consécutifs de valeur 1 ; les bits restants (donc les 8 derniers bits) sont mis à 0.  
    Autrement dit, ce masque vaut ```11111111.11111111.11111111.00000000``` et cela correspond bien à l'écriture binaire de ```255.255.255.0```.  

De la même manière, le suffixe ```/16``` donnera un masque de ```11111111.11111111.00000000.00000000``` , soit ```255.255.0.0```.  
Ou encore, un suffixe ```/21``` donnera un masque de ```11111111.11111111.11111000.00000000``` , soit ```255.255.248.0```. 

!!! tip "Identification d'un masque de sous-réseau en ligne de commande"
    Pour obtenir le masque de sous-réseau (en notation CIDR) : 

    - dans un terminal sous Linux, entrer la commande `ip addr`(ou `ifconfig` sur les vieux systèmes et sur Filius)
    - dans l'invite de commande sous Windows, entrer la commande `ipconfig`

On se sert aussi de cette notation pour désigner l'**adresse d'un sous-réseau** (appelée aussi _préfixe_ du sous-réseau) : par exemple, dire que l'adresse d'un sous-réseau est ```192.168.0.0 / 24``` signifie que toutes les machines de ce sous-réseau ont une adresse IP de la forme ```192.168.0.d```, où ```d``` est un nombre entier compris entre 1 et 254 (le 255 correspond à une adresse _réservée_ l'adresse de diffusion ou _broadcast_).


## ^^5. Exercices type Bac^^

### Exercice 1 : extrait du sujet 2022 Centres Étrangers Jour 1

Un réseau local est relié à internet à l'aide d'une box faisant office de routeur. Un  utilisateur connecte un nouvel ordinateur à ce réseau et veut tester son fonctionnement.

Il utilise en premier la commande `ifconfig` qui correspond à `ipconfig` sous environnement Windows. Cela lui donne le résultat suivant.

```console
$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500
inet 10.0.2.15 netmask 255.255.255.0 broadcast
10.0.2.
inet6 fe80::761a:3e85:cc97:6491 prefixlen 64 scopeid
0x20<link>
ether 08:00:27:8b:c3:91 txqueuelen 1000 (Ethernet)
RX packets 136 bytes 13703 (13.3 KiB)
RX errors 0 dropped 0 overruns 0 frame 0
TX packets 180 bytes 17472 (17.0 KiB)
TX errors 0 dropped 0 overruns 0 carrier 0
collisions 0
device interrupt 9 base 0xd
```

**1.** L'indication `ether 08:00:27:8b:c3:91` correspond à une adresse MAC. Que représente-t-elle ?

??? danger "Réponse"
    L'adresse MAC est un numéro associé à un matériel (carte Etherner, carte Wifi, etc.). Il est unique au monde et ne change jamais.

**2.** On s'intéresse ensuite à l'indication `inet 10.0.2.15`

Que représente `10.0.2.15` ?

??? danger "Réponse"
    L'adresse `10.0.2.15` est une adresse IP qui permet d'identifier un matériel de manière unique sur un réseau. Elle peut changer.

**3.** Pour connaitre la passerelle, l'utilisateur fait alors un `traceroute` dont la première ligne sortie est la ligne suivante.

```console
1 _gateway (10.0.2.2) 0.328 ms 0.275 ms 0.267 ms
```

À quel type de matériel correspond l'adresse `10.0.2.2` ?

??? danger "Réponse"
    L'adresse `10.0.2.2` correspond à la passerelle (_gateway_), appelé aussi routeur, gérant le réseau et capable d'acheminer l'information aux machines extérieures.

### Exercice 2 : extrait du sujet 2022 Mayotte Jour 1 

Télécharger l'[énoncé](data/sujet2022_Mayotte1_enonce.pdf){:target="_blank"}.

??? danger "Éléments de correction"

    [Corrigé succinct](data/sujet2022_Mayotte1_corrige.pdf){:target="_blank"}.


## ^^6. Pour aller un peu plus loin^^

Un administrateur gère un réseau 192.44.78.0/24 : il peut donc connecter 2^8^ = 256 machines (le dernier octet de l'adresse IP peut prendre les valeurs entre 0 et 255, _en oubliant dans cet exercice les valeurs habituellement réservées 0 et 255_).

 Il aimerait décomposer ce réseau en quatre sous-réseaux (par exemple car il y a quatre bâtiments distincts dans son établissement), qu'il numérote de 0 à 3. 

Puisqu'il faut 2 bits pour coder les entiers de 0 (`00` en binaire) à 3 (`11` en binaire), il doit rajouter deux bits au masque de réseau :  chaque sous-réseau aura donc un masque qui commence par 26 bits de valeur 1.

=== "Question 1"
    Donner la valeur du nouveau masque, en notation binaire puis en notation décimale.
=== "Solution"
    Il y a 26 bits à 1 puis des 0 donc en binaire, le masque vaut ```11111111.11111111.11111111.11000000```. En notation décimale, cela s'écrit  ```255.255.255.192```.

Chaque sous-reseau peut connecter 256 ÷ 4 = 64 machines. Le premier sous-réseau gère alors les adresses de 192.44.78.0/26 à 192.44.78.63/26.

=== "Question 2"
    Donner les plages d'adresses des trois autres sous-réseaux.
=== "Solution"
    Les autres sous-réseaux ont pour plages d'adresses respectives :

    - le sous-réseau n° 2 regroupe les adresses de 192.44.78.64/26 à 192.44.78.127/26
    - le sous-réseau n° 3 regroupe les adresses de 192.44.78.128/26 à 192.44.78.191/26
    - le sous-réseau n° 4 regroupe les adresses de 192.44.78.192/26 à 192.44.78.255/26

<!-- void -->

=== "Question 3"
    La machine d'adresse IP 192.44.78.100/26 et celle d'adresse IP 192.44.78.150/26 appartiennent-elles au même sous-réseau ?
=== "Solution"
    Non, la première machine appartient au sous-réseau n°2 (car 63 ≤ 100 ≤ 127) et la seconde machine appartient au sous-réseau n°3 (car 128 ≤ 150 ≤ 191).


---
??? summary "Sources :sparkles:"

    - Cours de [G. Glassus](https://glassus.github.io/premiere_nsi/T3_Architecture_materielle/3.3_Architecture_reseau/cours/)
    - [Wikipédia](https://fr.wikipedia.org/wiki/Sous-réseau)

---
