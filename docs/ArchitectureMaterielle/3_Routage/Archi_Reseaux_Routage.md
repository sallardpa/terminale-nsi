
# Protocoles de routage :trolleybus:

## ^^Introduction : quelques rappels sur la communication sur un réseau^^

Lorsqu'une machine A, d'adresse IP_A veut communiquer (envoyer un paquet de données) avec une machine B, d'adresse IP_B :

- la machine A calcule (grâce au masque de sous-réseau) si B est dans le même sous-réseau qu'elle, ou pas.
- si oui, elle peut communiquer directement avec B.
- si non, elle envoie le paquet de données au routeur et le routeur va observer l'adresse IP_B à qui il doit transmettre ce paquet de données. C'est maintenant que vont intervenir les **protocoles** de routage qui vont permettre d'établir la **table de routage** du routeur.

!!! info "Un rappel sur les routeurs"
    Un routeur est équipée de plusieurs ***interfaces*** (chacune correspondant à une prise réseau), et chaque interface a sa propre adresse IP.
    ![](data/routeur.png){ .center width=35%}


!!! done "Exemple sur un réseau simple"

    Prenons l'exemple du réseau simplifié suivant (_remarque : mettre en mode Jour pour bien voir les détails_): 
    ![](data/MonRoutageRIP.png){ .center}

    Le routeur R1 dispose de trois interfaces :

    - l'interface d'adresse ```192.168.0.254/24```, connectée à l'ordinateur d'adresse ```192.168.0.10```
    - l'interface d'adresse ```192.168.3.1/24``` connectée au routeur R3
    - l'interface d'adresse ```192.168.4.10/24```, connectée au routeur R2

## ^^1. Tables de routage^^

Les tables de routage sont des informations stockées dans le routeur permettant d'aiguiller intelligemment les données qui lui sont transmises.

### 1.1. Un exemple de table de routage

Dans le réseau de l'exemple ci-dessus, la table de routage du routeur R1 pourrait être :

<figure markdown>
| Destination | Passerelle suivante | Via l'interface |
|:---:|:---:|:---:|
| 192.168.0.0/24 | | 192.168.0.254 |
| 192.168.3.0/24 | |  192.168.3.1 |
| 192.168.4.0/24 | | 192.168.4.10 |
| 192.168.15.0/24 | R3 |  192.168.3.1  |
</figure>


Quand l'ordinateur d'adresse ```192.168.0.10``` veut communiquer avec la machine ```192.168.15.15``` :

- les deux adresses n'appartient pas au même sous-réseau donc la requête est confiée au routeur R1 via son adresse passerelle (ici ```192.168.0.254```).
- le routeur R1 consulte sa table de routage et, pour atteindre les adresses du réseau ```192.168.15.0```, il doit transmettre le paquet de données sur son interface  ```192.168.3.1``` connectée au routeur R3.
- le routeur R3 consulte sa propre table de routage (qui n'est pas explicitée ici) et transfère le paquet à la machine ```192.168.15.15```.


### 1.2. Construction des tables de routage 

Une table de routage peut être construite :

- soit à la main par l'administrateur réseau, quand le réseau est petit : on parle alors de table **statique**.
- soit de manière **dynamique** : les réseaux s'envoient eux-mêmes des informations permettant de mettre à jour leurs tables de routages respectives. Pour cela, ils emploient des algorithmes de détermination de ***meilleur chemin***  : nous allons en découvrir deux, le protocole RIP et le protocole OSPF.

## ^^2. Le protocole RIP^^

Le ***Routing Information Protocol*** va chercher à ***minimiser le nombre de sauts***, c'est-à-dire à minimiser le nombre de serveurs par lesquels un paquet va passer pour atteindre sa destination.

!!! question "Exercice"
    === "Illustration dans le cas du réseau simplifié"

        Télécharger [ce fichier Filius](data/MonRoutageRIP.fls){:target="_blank"} puis l'ouvrir avec Filius.

        En mode simulation, afficher le bureau de l'ordinateur 192.168.0.10 puis ouvrir un terminal de ligne de commandes.

        Lancer alors la commande ```traceroute 192.168.15.15``` pour obtenir la liste des routeurs par lesquels les données ont transité.

        Les données ont-elles suivi un chemin qui minimise le nombre de sauts ?

    === "Résultat" 

        On constate que la machine 192.168.15.15 a été atteinte en 3 sauts : le routeur R1 (192.168.0.254), le routeur R3 (192.168.3.5) puis la destination finale (192.168.15.15).

        Ce mode de routage a bien minimisé le nombre de sauts : par défaut, Filius utilise le protocole RIP pour établir les tables de routage.

??? tip "Le fonctionnement du protocole RIP "
    Il est basé sur l'échange (toutes les 30 secondes) des tables de routage de chaque routeur.
    Au début, chaque routeur ne connait que les réseaux auquel il est directement connecté : ceux qui sont à la distance 1.

    Ensuite, chaque routeur reçoit périodiquement la table des routeurs auxquels il est connecté, suivant les règles ci-dessous :

    - s'il découvre une route vers un nouveau réseau inconnu, il l'ajoute à sa table en augmentant de 1 la distance annoncée par le routeur qui lui a transmis sa table.

    - s'il découvre une route vers un réseau connu mais plus courte (en rajoutant 1) que celle qu'il possède dans sa table, il actualise sa table.

    - s'il découvre une route vers un réseau connu mais plus longue que celle qu'il possède dans sa table, il ignore cette route.

    <!-- - s'il reçoit une route vers un réseau connu en provenance d'un routeur déjà existant dans sa table, il met à jour sa table car la topologie du réseau a été modifiée. -->

    - si un routeur ne reçoit pas pendant 3 minutes d'information de la part d'un routeur qui lui avait auparavant communiqué sa table de routage, ce routeur est considéré comme en panne, et toutes les routes passant par lui sont affectées de la distance infinie : 16.

    Si le réseau ne subit pas de modification, les tables de routage _convergent_ et donnent alors une représentation fiable du réseau.


**Remarques et inconvénients:** 

- Le protocole RIP n'admet qu'un nombre de sauts inférieur ou égal à 15, ce qui le limite aux réseaux de petite taille.

<!-- - Chaque routeur n'a jamais connaissance de la topologie du réseau tout entier : il ne le connaît que par ce que les autres routeurs lui ont raconté. On dit que ce protocole de routage est du _routing by rumor_. -->

- La _métrique_ utilisée (le nombre de sauts) ne tient pas compte de la qualité de la liaison, contrairement au protocole OSPF.    



## ^^3. Le protocole OSPF^^

Le protocole ***Open Shortest Path First*** va chercher à ***minimiser le temps de transmission*** des données : les tables de routage vont prendre en considération la vitesse de communication entre les routeurs.


### 3.1 Les différents types de liaison et leur coût
On peut, approximativement, classer les types de liaison suivant ce tableau de débits **théoriques** :

<figure markdown>
| Technologie | débit (descendant)  |
|:---:|:---:|
| Bluetooth | 3 Mbit/s | 
| Ethernet | 10 Mbit/s | 
| Wi-Fi |  de 10 Mbit/s à 10 Gbits/s | 
| ADSL | 13 Mbit/s | 
| 4G | 100 Mbit/s | 
| Satellite | 50 Mbit/s | 
| Fast Ethernet | 100 Mbit/s |
| Fibre optique | 10 Gbit/s | 
| 5G | 20 Gbit/s | 
</figure>

Dans une première phase d'initialisation, chaque routeur va déterminer (par succession de messages envoyés et reçus) la qualité technique des liaisons entre chaque routeur.

L'idée du protocole OSPF est de donner à chaque liaison un **coût** inversement proportionnel au débit de transfert.

Par exemple, si le débit $d$ est exprimé en bits par seconde, on peut calculer le coût de chaque liaison par une formule du type :

$$ \text{coût} = \frac{10^8}{d} $$


Le nombre 10^8^ au numérateur peut être différent suivant les exercices : ici, il a été fixé pour donner un coût égal à 1 aux liaisons Fast Ethernet (100 Mbits/s = 10^8^ bits par seconde). 

Avec cette convention, une liaison satellite de 20 Mbits/s aura un coût égal à 5 et une liaison par fibre optique de 10 Gbit/s aura un coût égal à 0,01. On constate bien que le coût est d'autant plus faible que la liaison a un fort débit.

La table de routage d'un routeur va ensuite être élaborée en **minimisant le coût total du parcours**.

### 3.2. Illustration sur un réseau simple

!!! question "Exercice"
    === "Énoncé"
        On reprend le même réseau que dans l'exemple précédent, mais en ayant précisé la nature des différentes liaisons entre les routeurs.
        ![](data/MonRoutageOSPF.png){ .center}

        Télécharger [ce fichier Filius](data/MonRoutageOSPF.fls){:target="_blank"} puis l'ouvrir avec Filius.

        En mode simulation, depuis l'ordinateur 192.168.0.10, lancer alors la commande ```traceroute 192.168.15.15```.

        Qu'observez-vous ? Les données ont-elles suivi le même chemin que précédemment ?
    === "Résultat" 
        On constate que la machine 192.168.15.15 a été atteinte en passant par le routeur R2 : cette route semble donc plus rapide que la route directe R1-R3.
    
        Ceci correspond à un fonctionnement sous protocole OSPF : avec la formule précédente, les deux chemins R1-R2 et R2-R3 ont un coût total de 1+1 = 2, ce qui est inférieur au coût du chemin direct R1-R3 qui vaut 10. 

        _Remarque_ : ce mode de fonctionnement n'est pas paramétrable dans Filius, il a ici fallu le simuler en gérant manuellement les tables de routage. 
    
## ^^4. Synthèse^^

Même si les processus d'élaboration des tables de routage sont différents, les deux protocoles RIP et OSPF ont ceci en commun qu'ils vont ***minimiser une somme de coûts*** pour déterminer le meilleur parcours :

- dans le protocole RIP, les chemins entre deux routeurs connectés ont tous un coût identique égal à 1 ;
- dans le protocole OSPF, les chemins ont un coût inversement proportionnel au débit (et calculé par une formule).

!!! done "Exemple avec le réseau simple  précédent"

    ![](data/mermaid-diagram-1.svg#only-light){ width=90%}
    ![](data/mermaid-diagram-1d.svg#only-dark){ width=90%}
    
<!-- 
```mermaid
    flowchart LR
        subgraph a [avec RIP]
            direction LR
            A([R1]) --- |1| B([R2])
            A --- |1| C([R3])
            B ---  |1| C
        end
        subgraph b [avec OSPF]
            direction LR
            D([R1]) --- |1| E([R2])
            D --- |10| F([R3])
            E ---  |1| F
        end
        a  x-..-x |Un même réseau, mais deux protocoles| b
``` 
-->


## ^^5. Exercices type Bac^^


### 5.1. Sujet 2022, Métropole Jour 2

Lien vers [le sujet](https://sallardpa.forge.apps.education.fr/terminale-nsiArchitectureMaterielle/3_Routage/22-ME2-ex3/){:target="_blank"}.


### 5.2. Extrait du sujet 2022, Amérique du Nord Jour 1

Télécharger l'[énoncé](data/2022AmeriqueNord1.pdf){:target="_blank"}.

??? danger "Éléments de correction"

    [Corrigé succinct](data/cor_2022AmeriqueNord1.pdf){:target="_blank"}

### 5.3. Extrait du sujet 2022, Centres Étrangers Jour 1

Télécharger l'[énoncé](data/Sujet2022_J1G11.pdf){:target="_blank"}.

??? danger "Éléments de correction"

    [Corrigé succinct](data/Sujet2022_J1G11_corrige.pdf){:target="_blank"}

## ^^6. Pour aller plus loin : trouver le plus court chemin dans un graphe pondéré^^

L'exemple précédent était très simple et de solution intuitive. Dans le cas d'un graphe pondéré complexe, existe-t-il un algorithme de détermination du plus court chemin d'un point à un autre ?

La réponse est **oui**, depuis la découverte en 1959 par Edsger Dijkstra de l'algorithme qui porte son nom, **l'algorithme de Dijkstra**.

Pour le comprendre, vous pouvez regarder la vidéo suivante :

<p align="center">
<iframe width="790" height="372" src="https://www.youtube.com/embed/rI-Rc7eF4iw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

Cet algorithme, ici exécuté de manière manuelle, est bien sûr programmable. Et c'est donc grâce à lui que chaque routeur calcule la route la plus rapide pour acheminer les données qu'il reçoit.

!!! question "Exercice d'application de l'algorithme de Dijkstra (HP)"
    
    Donner le plus court chemin pour aller de E à F dans le graphe ci-dessous :

    ![](data/mermaid-diagram-2.svg#only-light){ width=90%}
    ![](data/mermaid-diagram-2d.svg#only-dark){ width=90%}
    

    ??? danger "Solution"
    
        |E|A|B|C|D|F|Choix|
        |:---:|:---:|:---:|:---:|:---:|:---:|:---:|
        |**0**|--|--|--|--|--|E(0)|
        |.|30vE|--|40vE|**10vE**|--|D(10)|       
        |.|**20vD**|--|40vE|.|80vD|A(20)|
        |.|.|60vA|**30vA**|.|80vD|C(30)|
        |.|.|**50vC**|.|.|80vD|B(50)|
        |.|.|.|.|.|**70vB**|F(70)|
    
        Le meilleur trajet est donc E-D-A-C-B-F.  
        :warning: _Ici, ce trajet correspond à la colonne choix (dans l'ordre) mais ce n'est pas toujours le cas._ 

<!--     
```mermaid
    flowchart LR
    E([E]) --- |10| D([D])
    E ---  |30| A([A])
    E --- |40| C([C])
    A --- |40| B([B])
    A --- |10| C 
    B --- |20| F([F])
    D --- |70| F
    C --- |20| B
    D --- |10| A
    D --- |40| C
``` 
-->
