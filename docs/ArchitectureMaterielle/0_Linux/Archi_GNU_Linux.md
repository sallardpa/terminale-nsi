---
title: Système d'exploitation GNU Linux
---
# Autour du système d'exploitation GNU Linux 🐧


## ^^1. Rappels de quelques commandes usuelles^^ 

!!! info "Extrait des commandes de base Linux"

    | Commande | Action |
    |----------|--------|
    | `pwd`     | Afficher la position actuelle dans l'arborescence du système de fichiers |
    | `ls`     | Afficher le contenu d'un répertoire (ex. `ls Musique`) |
    | `cd`     | Changer de répertoire de travail (ex. `cd Documents`)|
    | `cp`     | Créer une copie d'un fichier (ex. `cp fichier_original.py fichier_copie.py`) |
    | `mv`     | Déplacer ou renommer un fichier ou un répertoire (ex. `mv fichier.txt myRepertoire`) |
    | `rm`     | Effacer un fichier ou un répertoire vide (ex. `rm mon_fichier.mp3`). Si le répertoire n'est pas vide, on peut l'effacer en ajoutant l'option `-R` (ex `rm -R mon_rep` ; à noter que le `R` est l'initiale de _Recursive_). |
    | `mkdir`  | Créer un répertoire vide (ex. `mkdir newRepertoire`) |
    | `cat`    | Afficher le contenu d'un fichier (ex. `cat monfic.txt`) |
    | `chmod`  | Modifier les permissions d'un fichier ou d'un répertoire |


    Pour un fichier , le format général de la commande `chmod` est du type  :

    `chmod droits_propriétaire,droits_groupe,droits_autres nom_fichier`

    où `droits_propriétaire`, `droits_groupe` et `droits_autres` indiquent les différents droits applicables et se construisent avec :

    - `+` ajouter
    - `-` supprimer
    - `=` affecter
    - `r` *read* pour lire
    - `w` *write* pour écrire
    - `x` *execute* pour exécuter
    - `u` désigne l'utilisateur courant, `g` désigne le groupe, `o` désigne les autres et `a` désigne tout le monde (_all_).

    Exemple : la commande `chmod u+x,o=r monfichier.txt` ajoute à l'utilisateur courant le droit d'exécution sur le fichier `monfichier.txt` et affecte aux autres le seul droit de lecture (_il n'y a pas de modification pour le groupe_) .

    On peut visualiser les permissions d'un fichier ou d'un répertoire en ajoutant l'option `-l` (un L minuscule, pas le chiffre 1) à la commande ` ls` :

    ```console
    toto@machine:~/Documents$ ls -l
    drwxr-xr-x 11 toto eleveNSI    4096 MonProjetNSI
    -rw-r--r--  1 toto eleveNSI   44388 puissance4.py
    ```

!!! info "Filtrer les fichiers"
    La commande `ls *.txt` permet de n'afficher que les fichiers dont le nom se termine par `.txt` : en fait, le symbole `*` remplace n'importe quelle chaîne de caractères.

    De la même façon, la commande `rm prog*` va supprimer tous les fichiers dont le nom commence par `prog`.

## ^^2. Chemin absolu, chemin relatif dans l'arborescence du système de fichiers^^

Dans les systèmes Linux, nous avons un système de fichier en arborescence :

![Exemple d'arborescence](data/nsi_prem_base_linux_2.png){: .center}

Dans le schéma ci-dessus on trouve des répertoires (_sur le schéma_ : noms entourés d'un rectangle, par exemple `home`) et des fichiers (_sur le schéma_ : uniquement des noms `grub.cfg`).

On parle d'arborescence, car ce système de fichier ressemble à un arbre à l'envers.
La base de l'arbre s'appelle la racine (_root_) de l'arborescence et se représente par un simple `/`.

Pour indiquer la position d'un fichier (ou d'un répertoire) dans l'arborescence, il existe 2 méthodes : indiquer un **chemin absolu** ou indiquer un **chemin relatif**. 

Le chemin absolu doit indiquer le chemin depuis la racine. Par exemple le chemin absolu du fichier `fiche.ods` est : `/home/elsa/documents/fiche.ods`.

On peut aussi repérer ce fichier depuis le répertoire où l'on se trouve, et on parle alors de **chemin relatif**. Par exemple, toujours pour le fichier `fiche.ods` :

- si on est déjà dans le répertoire `elsa`, son chemin relatif est `documents/fiche.ods` ;
- si on est dans le répertoire `boulot`, son chemin relatif est `../fiche.ods` (où le `..` désigne une remontée d'un cran dans l'arborescence) ;
- si on est dans le répertoire `images` de l'utilisateur `max`, son chemin relatif est `../../elsa/documents/fiche.ods`.

Remarquez l'absence du caractère `/` au début du chemin relatif  : c'est cela qui permet de distinguer un chemin relatif et un chemin absolu.


!!! question "Exercice"

    === "Questions"
    
        **1.** À l'aide du résultat de la commande ci-dessous, déterminer votre position dans le schéma ci-dessus.

        ```console
        elsa@machine:~/documents/boulot$ pwd
        /home/elsa/documents/boulot
        ```

        **2.** Quelle commande pouvez-vous exécuter pour supprimer le fichier `fiche.ods` ? On attend _une seule ligne de commande_, et il y a deux possibilités. 

    === "Solution"

        **1.** L'utilisatrice est clairement dans le répertoire `/home/elsa/documents/boulot`.

        **2.** On peut saisir `rm ../fiche.ods` (utilisation du chemin ***relatif***) ou bien `rm /home/elsa/documents/fiche.ods` (utilisation du chemin ***absolu***).

!!! info "Répertoire personnel de l'utilisateur"
    
    Dans un nom de chemin, le caractère `~` désigne le chemin absolu du répertoire personnel de l'utilisateur. Ainsi par exemple, si l'utilisatrice actuelle est `elsa`, la commande `cd ~/documents` est synomyne de `cd /home/elsa/documents`.

    À noter enfin que la commande `cd` (sans aucun argument) est synonyme de `cd ~`, ce qui signifie que cela ramène l'utilisateur dans son répertoire personnel (`/home/elsa`).

## ^^3. Travaux Pratiques^^

### 3.1. TP Commandes de base (encore un Escape Game)

Suivre les instructions de votre professeur.

### 3.2. TP Python en ligne de commande

Vous avez l'habitude d'utiliser un outil comme Thonny ou Spyder pour écrire et exécuter un programme Python (ces outils sont des IDE : _Integrated Development Editor_). Mais on peut s'en passer en travaillant juste en ligne de commande sous Linux. 


**1.** Ouvrir un terminal et créer/ouvrir un fichier `monprog.py` grâce à la commande `nano monprog.py`. 

Dans l'éditeur `nano`, taper le code Python suivant :

```python
rep = input("Entrer un nombre :")
n = int(rep)
S = 0
for k in range(n+1):
    S += k
print("La somme des entiers de 1 à ", n , " vaut ", S)
```

Sauvegarder avec les touches ++ctrl+"S"++ puis quitter avec ++ctrl+"X"++ 

**2.** Vérifier avec la commande `ls` qu'il y a bien le fichier `monprog.py` puis le lancer avec la commande `python3 monprog.py`. 

:thumbsup: C'est simple, pas vrai ? 


## ^^4. Exercice type Bac^^


Télécharger cet [énoncé ](data/extrait_2022_Asie_J1_Linux.pdf){:target="_blank"} fortement inspiré du sujet 2022 - Asie - Jour1. 



??? danger "Correction succincte"
    * Question 1.a : la commande est `cd ../projet`
    * Question 1.b : la commande est `cd /home/sam/projet`
    * Question 2.a : la commande est `ls projet` ou bien `cd projet` suivie de `ls`
    * Question 2.b : la commande est `chmod u+w projet/config.txt`
    * Question 2.c : la commande est `cat projet/idees/idee1.txt` ou bien `cd projet/idees` suivie `cat idee1.txt`
    * Question 3.a : la commande est `mv projet.py superprog.py`
    * Question 3.b : la commande est `cp images/ico.png idees/` ou bien `cd images` suivie de `cp ico.png ../idees/`
    * Question 3.c : la commande est `mv images creations`
    * Question 3.d : la commande est `rm idees/idee1.txt`
    * Question 3.e : la commande est `mkdir archives `

## 5. ^^Pour aller plus loin : découvrir la programmation Shell (_hors-programme_)^^

Quand vous programmez en Python, vous pouvez saisir vos commandes :

- soit directement dans la console ;
- soit en les écrivant dans un fichier `prog.py` et en exécutant ensuite ce programme.

C'est la même chose avec les commandes Shell de Linux, mais pour l'instant (dans les TPs par exemple), vous n'avez utilisé que le terminal (l'équivalent de la console). 

Voyons sur un exemple comment écrire et exécuter un programme Shell.

!!! done "Premier script Shell"
    1. Dans un terminal, créer/ouvrir un fichier avec la commande `nano first.sh` puis, dans l'éditeur `nano`,  saisir les instructions suivantes :
    ``` bash
    echo "Hello world" >> monfichier.txt
    ls -l >> monfichier.txt 
    echo "Il y a un nouveau fichier dans votre répertoire!"
    ```
    > Remarque : la commande `echo` est l'équivalent de `print` en Python ; et l'opérateur `>>` permet d'envoyer le résultat de la commande vers ce qui suit (ici : vers le fichier `monfichier.txt`) au lieu de l'afficher dans le terminal.

    2. Enregistrer ce fichier avec ++ctrl+"O"++ et quitter avec ++ctrl+"X"++ puis, toujours dans un terminal, **modifier les permissions** de ce fichier pour vous accorder les droits d'exécution.

    3. Toujours dans le terminal, exécuter le fichier à l'aide de la commande `sh first.sh`. Observer le résultat dans le terminal, et surtout en examinant le contenu du fichier `monfichier.txt` qui est apparu dans votre répertoire.

La programmation Shell est plutôt utilisée pour l'administration des systèmes et la gestion des fichiers, mais vous pouvez très bien traduire un programme Python en un script Shell.

=== "Petit programme Python"

    ```python
    S = 0
    k = 1
    while k <= 10 :
        S = S + k
        k = k + 1
    print("La somme de 1 à 10 vaut ", S)
    ```
=== "Traduction en Shell"
    
    ```bash
    S=0
    k=1
    while [ $k -le 10 ]
    do
        S=$((S+k))
        k=$((k+1))
    done
    echo "La somme de 1 à 10 vaut $S"
    ```

La programmation Shell n'est pas du tout un attendu du Bac. Vous pouvez toutefois vous y former à l'aide de ressources sur Internet (par exemple [ici](https://frederic-lang.developpez.com/tutoriels/linux/prog-shell/)).