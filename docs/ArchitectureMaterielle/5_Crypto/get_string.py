def get_string(tab_utf8) :
    """
    Renvoie la chaine de caractères dont les codes ASCII/UTF8 sont les valeurs de tab_utf8
    Entrée : un tableau (type list) d'entiers 
    Sortie : une chaine de caractères (type str)
    """
    # compléter

assert get_string([78, 83, 73]) == "NSI"