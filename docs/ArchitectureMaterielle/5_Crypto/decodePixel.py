def decode_un_pixel(codeRGB) :
    """
    Renvoie une chaine de 3 caractères donnant les 3 bits de poids faible de chaque couleur
    Entrées : un triplet de trois entiers donnant le code RGB d'un pixel
    Sortie : une chaine de 3 caractères (type str)
    """
    (r,g,b) = codeRGB
    # à compléter

assert decode_un_pixel((73,61,138)) == '110', "problème ici"