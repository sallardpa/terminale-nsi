def codage_longueur(message) :
    """
    Renvoie une chaine binaire de 15 caractères codant la longueur du message
    Entrée : message est de type str de longueur < 2**15
    Sortie : de type str 
    """
    pass # à remplacer par votre code

# test provisoire
message = "les sanglots longs des violons de l'automne blessent mon coeur d'une langueur monotone"
assert codage_longueur(message) == '000000001010110', "probleme ici"