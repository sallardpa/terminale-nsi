def chiffre_XOR(texte, clef) :
    """ Renvoie le tableau d'entiers obtenu par chiffrement XOR de texte avec clef comme clé de  chiffrement
    Entrées : texte et clef sont de type str
    Sortie : un tableau (type list) d'entiers
    """
    tab_utf8_texte = ...
    tab_utf8_clef = ...
    tab_xor = []
    for k in range(len(texte)) :
        nb_xor = ... ^ tab_utf8_clef[k%len(clef)]
        tab_xor.append(...)
    return ...

assert chiffre_XOR("UN MESSAGE TRÈS SECRET", "NSI") == \
       [27, 29, 105, 3, 22, 26, 29, 18, 14, 11, 115, 29, 28, 155, 26, 110, 0, 12, 13, 1, 12, 26]