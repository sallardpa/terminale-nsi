def get_utf8(texte) :
    """
    Renvoie le tableau des codes ASCII/UTF8 de chaque caractère du message
    Entrée : texte (type str)
    Sortie : un tableau (type list) d'entiers
    """
    # compléter

assert get_utf8("NSI") == [78, 83, 73]