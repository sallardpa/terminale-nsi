def modif_un_pixel(codeRGB, chaine3bits) :
    """
    Entrées : 
    - code RGB est un triplet (r,g,b) de 3 entiers (les couleurs d'un pixel, codées en décimal) 
    - chaine3bits est  une chaine de 3 bits qu'on veut cacher dans r, g et b
    Sortie : 
        La fonction renvoie un tuple contenant les entiers (r, g, b) ainsi modifiés
    """
    (r,g,b) = codeRGB
    # conversion de (r,g,b) en binaire
     ...
    # modification de chaque dernier bit
    ...
    # retour en écriture décimale
    return (... , ... , ...) # parenthèses pour avoir un tuple

assert modif_un_pixel((72,61,139), "110" ) == (73,61,138), "problème de codage"