# Architectures matérielles, systèmes d'exploitation et réseaux 🐧

Cette partie est décomposée en :

* Systèmes d'exploitation :

    - Rappels et compléments sur le système d'exploitation GNU/Linux 🐧
    - Gestion des processus :cyclone:

    
* Réseaux :

    - Adressage sur un réseau :mailbox_with_mail:
    - Protocoles de routage :trolleybus:

<!-- * Systèmes sur puce (Systems on Chips) :iphone: -->
* Sécurisation des communications : cryptographie