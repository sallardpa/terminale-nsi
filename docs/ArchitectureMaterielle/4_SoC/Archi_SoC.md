# Systèmes sur puce :iphone:


## ^^1. Structure classique d'un ordinateur^^

Voici la structure classique d'un ordinateur
![](data/schemaComposantsOrdi.png){: .center width=60%}

Il possède au moins un **processeur** (Unité Centrale), au moins une **mémoire** et des **dispositifs d'entrée/sortie**. De plus, tous ces composants communiquent entre eux par le biais de **bus** (connections spécifiques). La **carte mère** désigne la carte électronique qui regroupe la plupart de ces composants.

Chaque composant est remplaçable, et il est possible d'ajouter de nouveaux composants sur la carte mère qui possède des slots d'extension.

Pour examiner plus en détail les composants d'un ordinateur classique : voir par exemple [ici](https://www.malekal.com/composants-fonctionnement-ordinateur/)


## ^^2. Tout un ordinateur sur une seule puce : les SoC^^

Le principe d'un **système sur puce** ou **System On a Chip** (SoC) est d'intégrer au sein d'une puce unique un ensemble de composants habituellement physiquement dissociés dans un ordinateur classique.

On peut retrouver ainsi au sein d'une **même puce** :

- le microprocesseur (CPU)
- la carte graphique (GPU)
- la mémoire RAM

\+ éventuellement des composants de communication (WiFi, Bluetooth...)
 

!!! tip "Les SoC sont parmi nous : les atouts des SoC"
    Vos smartphones et tablettes sont équipés de SoC. 
    En effet, l'intérêt premier d'un SoC est sa taille extrêmement réduite. En outre, il n'y a souvent pas besoin d'un système de refroidissement lourd et volumineux.

!!! warning "L’inconvénient d'un SoC"
    Le principal inconvénient d'un SoC est son absence de modularité : il est impossible de lui ajouter un composant et s'il tombe en panne, il faut remplacer le SoC en intégralité.

Les SoC gagnent de nombreux domaines, depuis l'informatique domestique (le mini-ordinateur Raspberry Pi 400 est équipé d'un SoC : voir la photo ci-dessous) jusqu'au secteur industriel (par exemple dans les automobiles : système de freinage ABS, système de guidage par GPS, etc.).

![raspi](data/raspi400.jpg){: .center width=60%}


Pour aller plus loin : 

* [Cours de G. Lassus](https://glassus.github.io/terminale_nsi/T5_Architecture_materielle/5.1_Systemes_sur_puce/cours/)
* [Cours qkzk](https://qkzk.xyz/docs/nsi/cours_terminale/architecture/composants/cours/)


## ^^3. Exercice type Bac^^

<!-- ### 3.1. D'après 2022, Centres étrangers, J1, Ex. 5 -->

Un nano-ordinateur est un ordinateur possédant une taille inférieure à un micro-ordinateur. Les nano-ordinateurs (sans l'alimentation, le clavier, la souris et l'écran) tiennent dans la paume de la main. Le SOC (_System on a chip_), littéralement un système sur une puce, est un système complet embarqué sur une seule puce (circuit intégré) pouvant comporter de la mémoire, un ou plusieurs microprocesseurs, des périphériques d'interface ou tout autre composant. 

On souhaite comparer les performances de deux nano-ordinateurs contenant chacun un SOC différent dont les caractéristiques sont détaillées ci-dessous :



|                | SOC_1 | SOC_2 |
|----------------|-------|-------|
| Processeur | Broadcom BCM271 | Broadcom BCM |
| Architecture | ARMv8-A (64-bit) | ARMv6Z (32-bit) |
| Microarchitecture | Cortex-A72 | ARM11 |
| Famille du processeur | BCM | BCM |
| Nb cœurs | 4 | 1 |
| Fréquence de base | 1,5 GHz | 700 MHz |
| Fréquence turbo | - | 1,0 GHz |
| Mémoire cache | 1 MB | 128 KB |
| Capacité mémoire maxi | 8 GB | 512 MB |
| Types de mémoire | LPDDR4-3200 SDRAM | SDRAM |
| GPU (processeur graphique) integer | Broadcom VideoCore VI | Aucun |
| GPU, unités d'exécution | 4 |  |
| GPU, unités shader | 64 |  |
| GPU, cadence | 500 MHz |  |
| GPU, flottant FP32 | 32 GFLOPS |  |
| Drystone MIPS | 22 740 DMIPS | 1 190 DMIPS |
| Résol. affichage max | 4K@60fps | 1080p@30fps |
| Décodage vidéo | H.265 4K@60fps, H.264 1080p@60fps | H.264 1080p@30fps |
| Encodage vidéo | H.264 1080p@30fps | H.264 1080p@30fps |
| Interface réseau | 10/100/1000M Gigabit Ethernet |  |
| Connectivité | USB 2.0, USB 3.0, HDMI 2.0 | USB 2.0, HDMI 1. |
| Wifi | 2.4GHz/5GHz 802.11 b/g/n/ac |  |
| Bluetooth | Bluetooth 4.2 |  |
| Audio | I2S | I2S |


**1.** Expliquer ce qui différencie le SOC d'un nano-ordinateur d'un microprocesseur classique ?

??? danger "Réponse"
    Un SOC est un système miniaturisé qui intègre à la fois le microprocesseur et la mémoire vive (RAM), mais aussi parfois la mémoire de stockage (flash) et des périphériques d'interface (Bluetooth, Wifi).

    Outre sa taille réduite, on a aussi une plus faible consommation d'énergie et un refroidissement passif donc silencieux.

    Certaines performances sont améliorées par la réduction des distances entre composants par rapport à un ordinateur classique.

    D'un autre côté, on ne peut pas changer les composants d'un SOC comme on le fait avec un ordinateur classique : pas de maintenance, ni d'évolution du bloc.

**2.** Lequel de ces SOC peut être connecté à un réseau filaire. Justifier la réponse.

??? danger "Réponse"
    Le BCM271 comporte une interface réseau Ethernet qui permet donc une connexion au réseau filaire.

**3.** Citer 2 caractéristiques permettant de comparer la puissance de calcul de ces deux SOC.

??? danger "Réponse"
    
    1. La fréquence indique la vitesse d'exécution du processeur, c'est un bon indicateur de performance avec le nombre de cœurs.
    2. La mémoire cache peut être vue comme une mémoire tampon entre le processeur et la mémoire vive : elle permet de travailler bien plus rapidement qu'avec la RAM. Une forte capacité est un autre bon indicateur de performance (surtout pour les processus qui ont besoin d'un peu de données).
    3. Une architecture 64 bits permet d'effectuer certaines taches courantes (comme liées à la sécurisation des données) de manière bien plus rapide qu'avec une architecture en 32 bits.

    Le BCM271 sort bien plus performant de cette comparaison.
