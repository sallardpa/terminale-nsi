# Gestion des bases de données : jointure de tables :telescope:

!!! note ""
    Le véritable intérêt d'un système de gestion de base de données (SGBD) et du langage SQL se révèle quand on traite des données stockées dans plusieurs tables différentes.

    On peut vouloir "croiser des données" : ceci est réalisable par la "jointure" de tables.



## ^^1. Recherche sur plusieurs tables : jointure de tables^^

On considère par exemple une base de données composée de deux tables :

- la table `artistes`, dont le schéma relationnel est (<u>Nom</u>: TEXT, Prenom : TEXT, PaysNaissance : TEXT, AnneeNaissance : INT) :

<figure markdown>
| Nom | Prenom| PaysNaissance | AnneeNaissance | 
| :------: | :-------: | :----: | :----: |
| Dylan | Bob | États-Unis | 1942 |
| Franklin | Aretha | États-Unis | 1942 |
| Lennon | John | Angleterre | 1940 |
| Jackson | Michael | États-Unis | 1958 |
| Dion | Céline | Canada | 1968 |
| Redding | Otis | États-Unis | 1941 |
</figure>

> À noter qu'***il n'est pas très judicieux*** d'avoir pris le nom comme clé primaire : cela interdit d'avoir deux artistes ayant le même nom de famille (par exemple, il sera impossible d'insérer dans cette table la chanteuse Janet Jackson, sœur de Michael Jackson).

- la table `morceaux`, dont le schéma relationnel est (<u>id_morceau</u>: INT, Titre : TEXT, Annee : INT, Nom_Artiste : TEXT) : 

<figure markdown>
| id_morceau | Titre| Annee | Nom_Artiste | 
| :------: | :-------: | :----: | :----: |
| 1 | Like a Rolling Stone | 1965 | Dylan |
| 2 | Imagine | 1970 | Lennon |
| 3 | Respect | 1967 | Franklin |
| 4 | My Heart Will Go On | 1997 | Dion |
| 5 | Beat It | 1982 | Jackson |
| 6 | Respect | 1965 | Redding |
</figure>

!!! tip "Jointure de tables"
    Pour faire une requête qui fait intervenir les deux bases simultanément, on utilise le mot clé `JOIN ... ON...` . 
    
    On dit que l'on effectue une ***jointure*** de tables.

**Exemple 1** : rechercher le nom et l'année de naissance de tous les artistes qui ont chanté un morceau de musique intitulé "Respect".

``` sql 
SELECT artistes.Nom, artistes.AnneeNaissance
FROM artistes 
JOIN morceaux ON artistes.Nom = morceaux.Nom_Artiste
WHERE morceaux.Titre = 'Respect' ; 
```

**Exemple 2** : rechercher le titre de tous les morceaux de musique qui ont été chanté par un artiste né hors des États-Unis.

``` sql 
SELECT morceaux.Titre
FROM artistes 
JOIN morceaux ON artistes.Nom = morceaux.Nom_Artiste
WHERE artistes.PaysNaissance  != 'États-Unis' ; 
```

!!! tip "Notion de clé étrangère"
    La table `morceaux` contient l'attribut `nom_artiste` qui est censé correspondre à la clé primaire `nom` de la table `artistes` : on dit que l'attribut `nom_artiste` de la table `morceaux`  est une ***clé étrangère*** vers la table  `artistes`.
    En spécifiant une telle clé étrangère lors de la création de la table, on ne pourra insérer un p-uplet avec le nom d'artiste X dans la table `morceaux` que si le nom X existe déjà dans la table `artistes`.

!!! question "Exercice"
    1. Télécharger le fichier [musique_short.db](musique_short.db) qui contient les deux tables de données `artistes` et `morceaux`, et choisir un outil pour effectuer vos requêtes : en ligne  sur [basthon.fr](https://notebook.basthon.fr/?kernel=sql) ou bien en local avec DB Browser ou en ligne de commande.
    
    2. Saisir les requêtes des exemples 1 et 2 ci-dessus.

    3. Écrire une requête qui affiche le titre du morceau de musique et le nom de l'artiste si le morceau a été chanté par un artiste de moins de 25 ans.

??? Solution
    ``` sql 
    SELECT morceaux.Titre, artistes.Nom 
    FROM artistes 
    JOIN morceaux ON artistes.Nom = morceaux.Nom_Artiste
    WHERE morceaux.Annee < artistes.AnneeNaissance +25 ;
    ```


## ^^2. Pour aller plus loin^^

### 2.1. Contraintes d'intégrité d'un système de gestion de base de données (SGBD) 

Prenons l'exemple d'une bibliothèque qui gère les prêts des ouvrages à l'aide d'une base de données composée de trois tables (_relations_) :

* **table «Livres»**  :


| id_livre | Titre   | Auteur | ISBN |
|---------------|--------|--------|-------|
| 942         | Les Misérables | Victor Hugo  | 9782211215350 |
| 1023          | La carte et le territoire | Michel Houellebecq | 9782081386099 |
| 486           | La place | Annie Ernaux   |  9782070779222 |

* **table «Emprunteurs»**  :

| id_emprunteur | Nom    | Prénom | 
|---------------|--------|--------|
| 129           | DULAC | Monique  | 
| 845           | ARMANI | Leïla |
| 125           | MARTIN | Jean   |

* **table «Emprunts»**  :

| id_emprunt| id_emprunteur |  code_livre | date |
|-----------|---------------|------------|------|
| 1 | 845  |  942  | 12/10/2022 |
| 2 | 125  | 1023 | 13/10/2022 |
| 3 | 125  |  486  | 13/10/2022 |

On remarque l'absence de **redondance** : on ne retrouve pas des données identiques dans des tables différentes.

**a) Contrainte de domaine**

Dans le langage SQL, le domaine d'un attribut correspond à ce qu'on appelle le type de la variable en informatique classique : nombre entier, texte, etc. Les types usuels en SQL sont :

- pour les nombres : INT, REAL/FLOAT, DECIMAL
- pour les textes : TEXT, CHAR(N) (texte composé de N caractères exactement), VARCHAR(N) (texte composé de N caractères maximum)
- pour les dates : DATE (format "AAAA-MM-JJ"), TIME (format "HH:MM:SS"), DATETIME (format "AAAA-MM-JJ HH:MM:SS")

Tout attribut d'un enregistrement doit **respecter le domaine** indiqué dans le schéma relationnel. Par exemple, si la table `Livres` a été créée en spécifiant que l'attribut `ISBN` doit être de type INT, la saisie du code ISBN "978-2211215350" provoquera une erreur (_à cause du tiret_ :angry:).

**b) Contrainte de relation**

La contrainte de relation impose que tout enregistrement soit unique : cette contrainte est réalisée par l'existence obligatoire d'une clé primaire.
Cette clé primaire est souvent créée de manière artificielle, à l'aide d'un "code d'identifiant" comme ```id_emprunteur``` ou ```id_emprunt```.

À noter que la clé primaire peut être un p-uplet. Ainsi, pour la table "Emprunt", au lieu de créer un attribut `id_emprunt` qui sert de clé primaire, on aurait pu définir la clé primaire par le triplet `(id_emprunteur, code_livre, date)`.

**c) Contrainte de référence**

La cohérence entre les différentes tables d'une base de données est assurée par les clés étrangères : dans une table, la valeur d'un attribut qui est clé étrangère doit obligatoirement pouvoir être retrouvée dans la table dont cet attribut est clé primaire.

Par exemple, s'il n'existe pas dans la table `Livres` d'ouvrage ayant 511 comme valeur d'attribut `id_livre`, il ne sera pas possible d'exécuter la requête suivante :

```sql
INSERT INTO Emprunts
VALUES (4,129, 511, 28/10/2022)
```


### 2.2. Perfectionnement

Vous voulez en savoir plus que votre prof sur SQL et sur les bases de données relationnelles ? Suivez alors ce [MOOC](https://ressources.unisciel.fr/sillages/informatique/bdd/ch00_menu/co/presentation.html).

## ^^3. Exercices et TP^^

### 3.1. Travaux Pratiques n°1 : à chacun sa voiture :red_car:

On s'intéresse ici à la base de données `car_database` regroupant diverses informations sur des voitures :

* marque et modèle,
* numéro d'immatriculation (*vehicle identification number*, `vin` dans les tables),
* année de fabrication,
* nom du propriétaire,
* options,
* *etc*...

Les différentes tables sont représentées ci-dessous :

![La base `car_database`](cars.png#only-light){: .center width 80%}
![La base `car_database`](cars_dark.png#only-dark){: .center width 80%}

Sur cette figure :

* chaque tableau correspond à une table dont le nom est indiqué sur la première ligne ;

* les lignes suivantes listent les attributs et leur type. `VARCHAR(255)` signifie que l'attribut est un texte de 255 caractères au maximum ;

* les clés primaires de chaque table sont indiquées en gras. Notez que la table `ownerships` a une clé primaire multiple, à savoir le couple `(customer_id, vin)`.

* les clés étrangères sont représentées par des liaisons entre les tables.

Cette base est téléchargeable [ici](car_database.db). Les requêtes à effectuer sont données dans [ce carnet Jupyter](TP_SQL_Jointures_Cars.ipynb), à ouvrir avec [basthon.fr](https://notebook.basthon.fr/?kernel=sql){target="_blank"}.


### 3.2. Travaux Pratiques n°2 : une enquête policière 🕵️‍♀️ 🕵️‍♂️

Le but de cette activité, proposé sur le site de l'université américaine  [Northwerstern University](https://knightlab.northwestern.edu/){target=_blank}, est de résoudre un crime en utilisant des requêtes SQL dans une base de données dont voici le schéma relationnel :
![schemabase](schema_enquete.png){: .center width=80%}

> _Traduction approximative de la description de l'activité_ : un crime a eu lieu et le détective a besoin de votre aide. Le détective vous a donné le rapport de la scène de crime mais vous l'avez perdu. Vous vous souvenez vaguement que le crime est un assassinat ayant eu lieu le 15 janvier 2018 à SQL City. La première étape est de retrouver le rapport de scène de crime à partir de la base de données de la police. Tous les indices sont enfouis dans cette base de données.


Toutes les requêtes peuvent être exécutées directement sur le site [ici](https://mystery.knightlab.com/walkthrough.html){target=_blank} ; cependant vous pouvez télécharger la  [base](sql-murder-mystery.db) afin d'exécuter vos requêtes en local.


À vous de trouver le coupable !

### 3.3. Exercice Type bac n°1

L'énoncé de cet exercice utilise les mots du langage SQL suivants :

`SELECT FROM, WHERE, JOIN ON, INSERT INTO VALUES, UPDATE, SET, DELETE, COUNT, AND, OR`.

Pour la gestion des réservations clients, on dispose d'une base de  données nommée « gare » dont le schéma relationnel est le suivant :

> Train (<u>numT</u>, provenance, destination, horaireArrivee, horaireDepart)

> Reservation (<u>numR</u>, nomClient, prenomClient, prix, #numT)

Les attributs soulignés sont des clés primaires. L'attribut précédé de # est une clé étrangère. La clé étrangère `Reservation.numT` fait référence à la clé primaire `Train.numT`.

Les attributs `horaireDepart` et `horaireArrivee` sont de type TIME et s'écrivent selon le format "hh:mm", où "hh" représente les heures et  "mm" les minutes.

**1)** Quel nom générique donne-t-on aux logiciels qui assurent, entre  autres, la persistance des données, l'efficacité de traitement des  requêtes et la sécurisation des accès pour les bases de données ?

??? danger "Solution"
    Le nom générique de ce type de logiciels est SGBD : Système de Gestion de Bases de Données.

**2)** 

a) On considère les requêtes SQL suivantes :

```sql
DELETE FROM Train WHERE numT = 1241 ;
DELETE FROM Reservation WHERE numT = 1241 ;
```

Sachant que le train n°1241 a été enregistré dans la table `Train` et que des  réservations pour ce train ont été enregistrées dans la table  `Reservation`, expliquer pourquoi cette suite d'instructions renvoie une  erreur.

b) Citer un cas pour lequel l'insertion d'un enregistrement dans la table `Reservation` n'est pas possible.

??? danger "Solution"
    a. La première requête supprime de la table `Train` le train dont l'attribut `numT` vaut 1241. Mais la seconde 
    requête génère une erreur : en effet, `numT` est une clé étrangère de la table `Reservation` qui fait référence à l'attribut `numT` de la table `Train` mais il n'y a plus de train dont le numéro `numT` vaut 1241 dans cette table `Train`.

    Pour ne pas avoir d'erreur, il faut inverser l'ordre des requêtes : d'abord supprimer les enregistrements dans la table `Reservation`, puis ceux dans la table `Train`.

    b. Il n'est pas possible d'insérer un enregistrement dans la table `Reservation` concernant une réservation sur un train qui n'existe pas dans la table `Train`.

**3)** Écrire des requêtes SQL correspondant à chacune des instructions suivantes :

a. Donner tous les numéros des trains dont la destination est « Lyon ».

b. Ajouter une réservation n°1307 de 33 € pour M. Alan Turing dans le train n°654.

c. Suite à un changement, l'horaire d'arrivée du train n°7869 est programmé à 08 h 11. Mettre à jour la base de données en conséquence.

??? danger "Solution"

    a. Donner tous les numéros des trains dont la destination est « Lyon » :
    ```sql 
    SELECT numT
    FROM Train
    WHERE destination = 'Lyon'
    ```

    b. Ajouter une réservation n°1307 de 33 € pour M. Alan Turing dans le train n°654.
    ```sql
    INSERT INTO Reservation
    VALUES (1307, 'Turing', 'Alan', 33, 654)
    ```

    c.  Suite à un changement, l'horaire d'arrivée du train n°7869 est programmé à 08 h 11.
    ```sql
    UPDATE Train
    SET horaireArrivee = '08:11'
    WHERE numT = 7869
    ```

**4)** Que permet de déterminer la requête suivante ?

```sql
SELECT COUNT(*) FROM Reservation 
WHERE nomClient = "Hopper" AND prenomClient = "Grace";
```

??? danger "Solution"
    Cette requête permet d'obtenir le nombre de réservations effectuées par la cliente Grace Hopper.

**5)** Écrire la requête qui renvoie les destinations et les prix des réservations effectuées par Grace Hopper.

??? danger "Solution"
    ```sql
    SELECT Train.destination, Reservation.prix
    FROM Train
    JOIN Reservation ON Train.numT = Reservation.numT
    WHERE Reservation.nomClient = "Hopper" AND Reservation.prenomClient = "Grace";
    ```

### 3.4. Exercice Type bac n°2

!!! done ""
    Sujet original [en version PDF](SQLtypeBac2.pdf)

Une restauratrice a mis en place un site Web pour gérer ses  réservations en ligne. Chaque client peut s'inscrire en saisissant ses  identifiants. Une fois connecté, il peut effectuer une réservation en  renseignant le jour et l'heure. Il peut également commander son menu en  ligne et écrire un avis sur le restaurant. Le gestionnaire du site Web a créé une base de données associée au site nommée `restaurant`, contenant les quatre relations du schéma relationnel  ci-dessous :

![](exercice_bac_2.png)
    
Dans le schéma relationnel précédent, un attribut souligné indique  qu'il s'agit d'une clé primaire. Un attribut précédé du symbole # indique qu'il s'agit d'une clé étrangère et la flèche associée indique  l'attribut référencé. Ainsi, par exemple, l'attribut `idPlat` de la  relation `Commande` est une clé étrangère qui fait référence à l'attribut  `idPlat` de la relation `Plat`. 

Dans la suite, les mots clés suivants du langage SQL pourront être  utilisés dans les requêtes :  `SELECT, FROM, WHERE, JOIN, ON, DELETE, UPDATE, SET, INSERT INTO, AND,  OR`.

**1)**  Parmi les trois requêtes suivantes, écrites dans le langage SQL,  laquelle renvoie les valeurs de tous les attributs des plats de la catégorie 'entrée' :

```sql
R1
SELECT nom, prix
FROM Plat
WHERE categorie  = 'entrée';

R2
SELECT *
FROM Plat
WHERE categorie  = 'entrée';

R3
UPDATE Plat
SET categorie  = 'entrée'
WHERE 1;
```

??? danger "Solution"
    Il s'agit de la requête R2.

**2)** Écrire, dans le langage SQL, des requêtes d'interrogation sur la base  de données `restaurant` permettant de réaliser les tâches suivantes :

a) Afficher les noms et les avis des clients ayant effectué une réservation pour la date du '2021-06-05' à l'heure '19:30:00'.

??? danger "Solution"
    ```sql
    SELECT Client.nom, Client.avis
    FROM Client
    JOIN Reservation ON Client.idClient = Reservation.idClient
    WHERE Reservation.jour = '2021-06-05' AND Reservation.heure = '19:30:00'
    ```

b) Afficher le nom des plats des catégories 'plat principal' et 'dessert', correspondant aux commandes de la date '2021-04-12'.

??? danger "Solution"
    ```sql
    SELECT Plat.nom
    FROM Plat
    JOIN Commande ON Plat.idPlat = Commande.idPlat
    JOIN Reservation ON Reservation.idReservation = Commande.idReservation
    WHERE (Plat.categorie = 'plat principal' OR Plat.categorie = 'dessert') AND Reservation.jour = '2021-04-12'
    ```

**3)**  Que réalise la requête SQL suivante ?

```sql
INSERT INTO Plat
VALUES(58,'Pêche Melba', 'dessert', 'Pêches et glace vanille', 6.5);
```

??? danger "Solution"
    Cette requête permet d'ajouter un plat qui aura pour idPlat 58, pour nom "Pêche Melba", pour catégorie "dessert", pour description "Pêches et glace vanille" et pour prix 6,5 euros.

**4)** Écrire des requêtes SQL permettant de réaliser les tâches suivantes :

a) Supprimer les commandes ayant comme `idReservation` la valeur 2047.

??? danger "Solution"
    ```sql
    DELETE FROM Commande
    WHERE idReservation = 2047
    ```

b) Augmenter de 5% tous les prix de la relation `plat` strictement inférieurs à 20 euros.

??? danger "Solution"
    ```sql
    UPDATE Plat
    SET prix = prix + 0.05*prix
    WHERE prix < 20.0
    ```

### 3.5. Exercice Type bac n°3

!!! done ""
    Sujet original [en version PDF](SQLtypeBac3.pdf)

On considère dans cet exercice une gestion simplifiée des emprunts des ouvrages d'un CDI. La base de données utilisée sera constituée de trois relations (ou tables) nommées `Eleves`, `Livres` et `Emprunts` selon le schéma relationnel suivant :
    
![](exercice_bac_3.png) 
    
Dans ce schéma relationnel, un attribut souligné indique qu'il s'agit d'une clé primaire. Le symbole # devant un attribut indique qu'il s'agit d'une clé étrangère et la flèche associée indique l'attribut référencé. Ainsi, l'attribut `idEleve` de la relation `Emprunts` est une clé étrangère qui fait référence à la clé primaire `idEleve` de la relation `Eleves`.
    
**1)** Expliquer pourquoi le code SQL ci-dessous provoque une erreur.
    
```sql
INSERT INTO Eleves VALUES (128, 'Dupont', 'Jean','T1') ;
INSERT INTO Eleves VALUES (200, 'Dupont', 'Jean','T1') ;
INSERT INTO Eleves VALUES (128, 'Dubois', 'Jean','T2') ;
```

??? danger "Solution"
    La première et la troisième requête utilisent toutes les deux la même valeur pour l'attribut `idEleves`, à savoir 128. 
    L'attribut `idEleve` étant une clé primaire, nous allons donc avoir une erreur : on ne doit pas trouver dans toute la relation 2 fois la même valeur pour une clé primaire.

**2)** Dans la définition de la relation `Emprunts`, qu'est-ce qui assure qu'on ne peut pas enregistrer un emprunt pour un élève qui n'a pas encore été inscrit dans la relation `Eleves` ?

??? danger "Solution"
    Dans la relation `Emprunts` l'attribut `idEleve` est une clé étrangère, c'est ce qui assure que l'on ne pourra pas enregistrer un emprunt pour un élève qui n'a pas encore été inscrit dans la relation `Eleves`.

**3)** Écrire une requête SQL qui renvoie les titres des ouvrages de Molière détenus par le CDI.

??? danger "Solution"
    ```sql
    SELECT titre
    FROM Livres
    WHERE auteur = 'Molière'
    ```

**4)** Décrire le résultat renvoyé par la requête ci-dessous.

```sql
SELECT COUNT(*)
FROM Eleves
WHERE classe = 'T2' ;
```

??? danger "Solution"
    Cette requête permet d'avoir le nombre d'élèves de la classe T2 inscrits au CDI.

**5)** Camille a emprunté le livre "Les misérables". Le code ci-dessous a permis d'enregistrer cet emprunt.
    
```sql    
INSERT INTO Emprunts
VALUES (640, 192, '9782070409228','2020-09-15',NULL);
```
    
Puis Camille a restitué le livre le 30 septembre 2020. Écrire une requête SQL de manière à mettre à jour la date de retour dans la base de données.

??? danger "Solution"
    ```sql
    UPDATE Emprunts
    SET dateRetour = '2020-09-30'
    WHERE idEmprunt = 640
    ```

    
**6)** Décrire le résultat renvoyé par la requête ci-dessous.
```sql
SELECT DISTINCT Eleves.nom, Eleves.prenom
FROM Eleves 
JOIN Emprunts ON Eleves.idEleve = Emprunts.idEleve
WHERE Eleves.classe = 'T2' ;
```

??? danger "Solution"
    Cette requête permet d'avoir le nom et le prénom de tous les élèves de la classe T2 qui ont déjà emprunté un livre au CDI.

**7)** Écrire une requête SQL qui permet de lister les noms et prénoms des élèves qui ont emprunté le livre "Les misérables".

??? danger "Solution"
    ```sql
    SELECT Eleves.nom, Eleves.prenoms
    FROM Emprunts
    JOIN Livres ON Livres.isbn = Emprunts.isbn
    JOIN Eleves ON Eleves.idEleve = Emprunts.idEleve
    WHERE Livres.titre = 'Les Misérables' ;
    ```



### 3.6. Exercices Type bac n°4 et 5 ; notion de contraintes d'intégrité

!!! done ""
    Sujet original [en version PDF](SQLtypeBac4.pdf)

!!! done ""
    Sujet original [en version PDF](SQLtypeBac5.pdf)