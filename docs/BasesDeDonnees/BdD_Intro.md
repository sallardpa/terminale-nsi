# Bases de données relationnelles et langage SQL :telescope:

## ^^1. Introduction au langage SQL et à la gestion des bases de données^^

On note `countries` la table de données ci-dessous qui présente (_en anglais_ ) quelques informations sur certains pays : 

<figure markdown>
| ISO_Code | Name | Population | Continent | Currency |
| :------: | :-------: | :----: | :----: | :----: |
| FR | France | 64768389 |  Europe |  Euro | 
| CA |  Canada |  33679000 | North America | Dollar |
| DK | Denmark | 5484000 | Europe | Krone |
| FI | Finland | 5244000 | Europe | Euro |
| GB | United Kingdom | 62348447 | Europe | Pound |
| US | United States | 310232863 | North America | Dollar |
</figure>

!!! question "Découverte des requêtes SQL"

    Essayer de deviner le résultat des ***requêtes*** ci-dessous.

* Requête 1 :

``` sql 
SELECT Population FROM countries WHERE Name = 'Finland' ; 
```

??? danger "Solution"
    On obtiendra à l'affichage : `5244000`, c'est-à-dire ce que contient l'***attribut*** `Population` pour l'enregistrement qui a un ***attribut***  `Name`  égal à 'Finland'. En clair, cela donne la population du pays appelé `Finland`.

* Requête 2 :
``` sql 
SELECT Name FROM countries WHERE Currency = 'Euro' ; 
```

??? danger "Solution"
    On obtiendra à l'affichage : `France, Finland`, c'est-à-dire tous les ***attributs*** `Name` des enregistrements qui ont un ***attribut***  `Currency`  égal à 'Euro'. En clair, cela donne tous les noms de pays dont la monnaie est l'Euro.

* Requête 3 :
``` sql 
SELECT DISTINCT Currency FROM countries WHERE Continent = 'Europe' ; 
```

??? danger "Solution"
    On obtiendra à l'affichage : `Euro, Krone, Pound`, c'est-à-dire tous les ***attributs*** `Currency` ***distincts*** des enregistrements qui ont un ***attribut***  `Continent`  égal à 'Europe'. En clair, cela donne toutes les monnaies, **sans doublons**, utilisées dans des pays d'Europe.

* Requête 4 : 
``` sql 
SELECT Name, Continent FROM countries WHERE Population > 50000000 AND Population < 100000000 ;
```

??? danger "Solution"
    On obtiendra à l'affichage : `(France, Europe), (United Kingdom, Europe)`, c'est-à-dire tous les couples `(Name, Continent)` des enregistrements qui ont un ***attribut***  `Population`  supérieur à 50 millions et inférieur à 100 millions.

* Requête 5 :
``` sql 
SELECT Name FROM countries ORDER BY Population ASC ;
```
??? danger "Solution"
    On obtiendra à l'affichage : `Finland, Denmark, Canada, United Kingdom, France, United States`, c'est-à-dire tous les noms de pays classés par **ordre croissant** de population.

* Requête 6 :
``` sql 
SELECT COUNT(*) FROM countries WHERE Continent = 'Europe' ;
```
??? danger "Solution"
    On obtiendra à l'affichage le nombre `4`, c'est-à-dire le nombre total (`COUNT`) de lignes dans lesquelles le continent est égal à 'Europe'.
    *À noter* : le caractère `*` est synonyme de "Tout". Ainsi, pour afficher une table de données en entier, on peut saisir `SELECT * FROM countries`.


* Requête 7 : 
``` sql 
INSERT INTO countries VALUES ('EG', 'Egypt', 80471869, 'Africa', 'Pound') ;
```

??? danger "Solution"
    Cela permet d'introduire dans la base de données un nouvel enregistrement : celui du pays `Egypt`. Il ne se produit rien à l'affichage.

    Pour afficher toute la table, on peut saisir la requête `SELECT * FROM countries`. Le caractère `*` est synonyme de "Tout".

* Requête 8 : 
``` sql 
UPDATE countries SET Population = 66732538 WHERE ISO_Code = 'FR' ;
```
??? danger "Solution"
    Cela permet de mettre à jour l'enregistrement qui a un ***attribut*** `ISO_Code` égal à `FR` en changeant la valeur de son ***attribut*** `Population`. En clair, cela a permis de mettre à jour la population de la France (estimation de l'INSEE au 1er janvier 2021). Il ne se produit rien à l'affichage.

* Requête 9 : 
``` sql 
DELETE FROM countries WHERE Currency = 'Krone' ;
```
??? danger "Solution"
    Cela permet de supprimer l'enregistrement qui a un ***attribut*** `Currency` égal à `Krone`. En clair, cela a supprimé la ligne du pays `Denmark`. Il ne se produit rien à l'affichage.

!!! tip "Un nouveau langage"
    Vous venez de découvrir un nouveau langage de programmation : le langage SQL (:flag_gb: :flag_us: _Structured Query Language_, que l'on peut traduire par _Langage de Requêtes Structurées_). 

    Comme tout langage de programmation, il a ses propres mots-clés : SELECT, FROM, WHERE, etc. Dans le jargon SQL, on ne parle pas d'_instruction_ mais de ***requête*** pour parler d'une ligne de code individuelle. Il faut séparer deux requêtes successives par un point-virgule. 


## ^^2. Mise en pratique des requêtes SQL^^ 

Il faut commencer par télécharger le fichier [countries_short.db](countries_short.db) qui contient la table de données `countries`.

Puis différents outils peuvent être utilisés pour exécuter des requêtes SQL sur cette base de données.


=== "Basthon (en ligne)"
    1. Télécharger le carnet Jupyter [NSI_Term_BdD_Intro.ipynb](NSI_Term_BdD_Intro.ipynb)
    2. Aller sur le site [basthon.fr](https://notebook.basthon.fr/?kernel=sql){target="_blank"}.
    3. Ouvrir (menu _Fichier/Ouvrir_ ) les deux fichiers téléchargés : le carnet Jupyter `NSI_Term_BdD_Intro.ipynb` ainsi que le fichier `countries_short.db`.
    4. Exécuter ensuite les différentes requêtes.

=== "Logiciel BD Browser"
    1. Le logiciel _DB Browser for SQLite_ est disponible gratuitement sur [ce site](https://sqlitebrowser.org/dl/){target="_blank"} : choisir la version "Windows PortableApp" pour installer ce logiciel sur votre PC Windows (même sans les droits Administrateur) ou bien la version "AppImage" pour un PC sous Linux (faire ensuite un `chmod` pour rendre ce fichier exécutable).
    2. Lancer le logiciel _DB Browser_ puis aller dans le menu `Fichier/Ouvrir une Base de Données` pour ouvrir ce fichier `countries_short.db`.
    3. Dans le menu `Edition > Préférences`, mettre le logiciel en français.
    4. Dans le menu `Vue`, décocher `Éditer les pragmas`.
    5. Dans l'onglet `Parcourir les données` : vous pouvez visualiser toute la base de données.
    6. Dans l'onglet `Exécuter le SQL` : saisir votre requête SQL (recopier les requêtes 1 à 8 ci-dessus les unes après les autres) et cliquer sur l'icône `Exécuter`.

=== "En ligne de commande sous Linux"
    1. Ouvrir un terminal dans le répertoire où se trouve le fichier `countries_short.db`.
    2. Saisir la commande `sqlite3 countries_short.db`
    3. Saisir ensuite chacune des requêtes 1 à 8 ci-dessus, en veillant bien à mettre un point-virgule à la fin.
    4. _Une astuce_ : pour plus de lisibilité, saisir au départ `.headers on` et `.mode column`.


!!! question "Exercice"
    Toujours avec la table `countries`, dans l'outil de votre choix :

    1. Écrire une requête SQL permettant d'afficher le code ISO et le continent de chaque pays dont la population dépasse 63 millions d'habitants.
    2. Écrire une requête SQL permettant d'afficher par ordre alphabétique le nom des pays dont la monnaie (`Currency`) est `Pound` .
    3. Écrire une requête SQL permettant de calculer la somme (`SUM`) de la population de tous les pays d'Europe.
    4. Écrire une requête SQL permettant d'ajouter à la table `countries` l'enregistrement correspondant à l'Afrique du Sud : son code ISO est "ZA", son nom anglais est "South Africa", sa population est de 49 millions d'habitants et sa monnaie est le "Rand". N'oubliez pas d'indiquer son continent !
    5. Les données de population de la table `countries` sont un peu vieilles : mettre à jour la population des États-Unis qui est estimée à 334 millions d'habitants en 2022.

??? danger "Solution"
    Les requêtes sont :

    1. ```SELECT ISO_Code, Continent FROM countries WHERE Population > 63000000;```
    2. ```SELECT Name FROM countries WHERE Currency = 'Pound' ORDER BY Name ASC;```
    3. ```SELECT SUM(Population) FROM countries WHERE Continent = 'Europe';```
    4. ```INSERT INTO countries VALUES('ZA','South Africa', 49000000, 'Africa', 'Rand') ;```
    5. ```UPDATE countries SET Population = 334000000 WHERE ISO_Code = 'US';```

## ^^3. Création d'une table^^

La table `countries` (contenue dans le fichier `countries_short.db`) vous a été fournie par votre professeur, 
mais il a bien fallu qu'il crée cette table de données.

La **création** de la table de données a été réalisée par le code suivant : 
``` sql 
CREATE TABLE countries (
    ISO_Code VARCHAR(2),
    Name TEXT,
    Population INT,
    Continent TEXT,
    Currency TEXT,
    PRIMARY KEY(ISO_Code)
);
```

!!! tip "Notion de schéma relationnel, notion de clé primaire"
    1. Le ***schéma relationnel*** d'une table de données est la description de ce que contient chaque enregistrement : le nom des attributs (ici `ISO_Code`, `Name`, etc.) et éventuellement son type (`INT` pour un nombre entier, `TEXT` pour un texte de grande longueur, `VARCHAR(n)` pour une chaîne de `n` caractères au maximum). 
    Dans les exercices de niveau Bac, on précisera rarement le type de la donnée. Ainsi, on dira que le schéma de la table `countries` est : (ISO_Code, Name, Population, Continent, Currency).
    2. Le fait que l'attribut `ISO_Code` soit indiqué comme ***clé primaire*** signifie qu'il sera impossible de créer deux enregistrements qui ont le même code ISO. On aurait aussi pu prendre `Name` comme ***clé primaire***, mais pas `Continent` ni `Currency`. Une table de données doit _obligatoirement_ avoir une ***clé primaire*** : par convention, cet attribut sera <u>souligné</u> dans le ***schéma*** de la table de données.
    À noter qu'une clé primaire peut être composée d'un couple d'attributs.


Ensuite, les enregistrements des différents pays ont été **insérés** les uns après les autres dans cette table initialement vide, à l'aide de requêtes du type `INSERT INTO countries VALUES (...)`
``` sql 
INSERT INTO countries VALUES ('FR','France',64768389,'Europe','Euro'),
    ('DK','Denmark',5484000,'Europe','Krone'),
    ('CA','Canada',33679000,'North America','Dollar'),
    ('FI','Finland',5244000,'Europe','Euro'),
    ('GB','United Kingdom',62348447,'Europe','Pound'),
    ('US','United States',310232863,'North America','Dollar');
```

!!! cite "Compétences attendues au Bac"
    Savoir créer une table de données **n'est pas** une compétence attendue au Bac. Par contre, vous devez savoir :
    
    * décrire une table existante (_écrire son schéma relationnel_)
    * extraire des informations à l'aide de requêtes du type `SELECT...`
    * modifier une table existante (`INSERT...`, `UPDATE...`, `DELETE...`).


!!! info "Création d'une table de données à partir d'un fichier CSV"

    Avec le logiciel *DB Browser for SQLite*, on peut aussi importer un fichier CSV pour créer une table de données. On crée d'abord une nouvelle base de données vide puis, dans le menu `Fichier`, on sélectionne `Importer`.

## ^^4. Le vocabulaire des bases de données^^

Il existe un vocabulaire très spécifique à ce monde des bases de données. Voici quelques "traductions" :

<figure markdown>
| Mot du monde SQL | Traduction  |
| :------ | :------- |
| Relation | Table de données |
| Schéma relationnel | Description du contenu de chaque enregistrement|
| Base de données | Ensemble de plusieurs tables de données |
| Requête | Instruction, ligne de code |
| Attribut | Champ, descripteur |
</figure>

![](schema_bdd.png){: .center}


## ^^5. Pour aller plus loin : un peu d'histoire et de théorie^^


<div class="centre"><iframe width="560" height="315" src="https://www.youtube.com/embed/pqoIBiM2AvE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>


!!! question "Questions"
    En utilisant la vidéo ci-dessus et en faisant vos propres recherches sur le *Web*, répondre aux questions suivantes :
    
    1. Quel mathématicien est à l'origine de la théorie des bases de données ? En quelle année ?
    2. Avant l'avènement des bases de données, les données étaient stockés sous la forme de simples fichiers, quels étaient les inconvénients de ce fonctionnement ?
    3. Que signifie l'*absence de redondance* pour une base de données ?
    4. Que signifie l'*indépendance logique* pour une base de données ?
    5. Que signifie l'*intégrité* pour une base de données ?
    6. Donner les noms de quelques ***SGBD*** connus en indiquant s'il s'agit de logiciels libres ou propriétaires.



## ^^6. Exercices et TP^^


Aller sur la page des [TP SQL](https://sallardpa.forge.apps.education.fr/terminale-nsiBasesDeDonnees/TP_SQL/).
