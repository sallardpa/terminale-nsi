# Bases de données relationnelles et langage SQL :telescope:

Cette partie est décomposée en :

* Introduction au langage SQL et à la gestion des bases de données

* Exercices et Travaux Pratiques en SQL

* Croisement de données et jointures de tables
