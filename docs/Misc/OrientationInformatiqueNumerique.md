# Études PostBac :rocket:

### Informations générales

* Un fichier fait par un enseignant-chercheur pour voir quelle fillière vous correspond le mieux : [conseilOrientation.ods](conseilOrientation.ods)

* [Guide ONISEP "Informatique et Réseaux"](https://www.onisep.fr/decouvrir-les-metiers/des-metiers-par-secteur/informatique-et-reseaux){target="_blank"}.

* [Studyrama](https://www.studyrama.com/formations/specialites/informatique-numerique/tout-savoir-sur-etudes-informatique)

* Décrypter Parcoursup pour une orientation réussie : [SupEasy](https://supeasy.fr//).

### Licence Informatique

* [Paris 6 (Jussieu - 5ème arrond.) / Sorbonne Université Sciences](https://sciences.sorbonne-universite.fr/formation-sciences/licences/licences-generales-l2-l3/licence-dinformatique){target="_blank"} :

    * Licence Informatique (1ère année généraliste)
    * Double licence Informatique / Mathématiques ou Physique ou Biologie ou Électronique


* [Paris 7 (Diderot - 13ème arrond.) / Université Paris Cité](http://www.informatique.univ-paris-diderot.fr/formations/licences/informatique-generale/accueil){target="_blank"} :
    - Licence Informatique
    - Double licence Informatique/Mathématiques ou Biologie ou Japonais

* [Paris 8 (Saint-Denis - 93) / Université Paris Lumières](https://informatique.up8.edu/licence-iv/){target="_blank"}
    - Licence Informatique, avec mineure "Conception et programmation de jeux vidéo"

* [Paris 13 (Villetaneuse - 93) / Université Sorbonne Paris Nord](https://galilee.univ-paris13.fr/licence/licence-informatique/){target="_blank"} :
    - Licence Informatique

* [Cergy Paris Université (Cergy - 95)](https://www.cyu.fr/licence-informatique-1){target="_blank"} :
    - Licence Informatique (1ère année généraliste)

* [Université Gustave Eiffel (Marne-la-Vallée - 77)](https://formations.univ-gustave-eiffel.fr/index.php?tx_kesearch_pi1%5Bfilter%5D%5B8%5D%5B50%5D=licence&tx_kesearch_pi1%5Bfilter%5D%5B10%5D%5B61%5D=Sciencestechnologiessant%C3%A9&id=1415&tx_kesearch_pi1%5Bpage%5D=1&tx_kesearch_pi1%5BresetFilters%5D=0&tx_kesearch_pi1%5BsortByField%5D=&tx_kesearch_pi1%5BsortByDir%5D=asc){target="_blank"} :
  
    * Licence Informatique (1ère année généraliste)



### Bachelor Universitaire de Technologie (BUT, ex-DUT) Informatique, R&T ou M.M.I. :


* [IUT de Paris - Rives de Seine / Université Paris Cité (16ème arrond.)](https://iutparis-seine.u-paris.fr/informatique/bachelor-universitaire-de-technologie-informatique/){target="_blank"}
    * B.U.T. Informatique

* [IUT de Montreuil (93) / Paris 8 - Université Paris Lumières](https://www.iut.univ-paris8.fr/){target="_blank"} :

    * B.U.T. Informatique

* [IUT de Villetaneuse (93) / Université Sorbonne Paris Nord](http://odf.univ-paris13.fr/fr/offre-de-formation/feuilleter-le-catalogue-1/sciences-technologies-sante-STS/dut-CB.html){target="_blank"} :

    * B.U.T. Informatique
    * B.U.T. Réseaux et télécommunications
    * B.U.T. Métiers du Multimédia et de l'Internet


* [IUT de Seine et Marne Sud (94) - Université de Paris-Est Créteil Val de Marne](https://www.u-pec.fr/fr/formation/but-sciences-technologie-sante){target="_blank"} :

    * B.U.T. Informatique
    * B.U.T. Réseaux et télécommunications
    * B.U.T. Métiers du Multimédia et de l'Internet

* [IUT de Marne-La-Vallée (77) - Université Gustave Eiffel](https://iut.univ-gustave-eiffel.fr/but){target="_blank"} :

    * B.U.T. Informatique
    * B.U.T. Métiers du Multimédia et de l'Internet

* [IUT d'Orsay (91) - Université Paris Saclay](https://www.iut-orsay.universite-paris-saclay.fr/formations/but/but_informatique){target="_blank"} :

    * B.U.T. Informatique

* [IUT de Vélizy (78) - Université Paris Saclay](https://www.uvsq.fr/les-dut){target="_blank"}

    * B.U.T. Informatique
    * B.U.T. Réseaux et télécommunications
    * B.U.T. Métiers du Multimédia et de l'Internet

### BTS Informatique 

Formation assurée par des lycées publics ou des écoles privées.

* BTS ["Cyber-Défense" de Saint-Cyr](https://rh-terre.defense.gouv.fr/formation/lyceesmilitaires/lycee-militaire-de-saint-cyr-l-ecole/enseignement-lycee-militaire-de-saint-cyr-l-ecole) (CIEL : Cybersécurité, Informatique et réseaux, Électronique, anciennement BTS SNIR), présenté [ici](https://korben.info/bts-cybersecurite-armee.html).

* BTS Serices Informatiques aux Organisations (SIO) : 
    * option [Solutions Infrastructure, Systèmes et Réseaux (SLIR)](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/bts-services-informatiques-aux-organisations-option-a-solutions-d-infrastructure-systemes-et-reseaux){target="_blank"}
    * option [Solutions Logicielles et Applications Métiers (SLAM)](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/bts-services-informatiques-aux-organisations-option-b-solutions-logicielles-et-applications-metiers){target="_blank"}


### Écoles spécialisées, avec admission directe PostBac

##### École nationale du jeu et des médias interactifs numériques (Angoulême) : [ici](https://enjmin.cnam.fr){target="_blank"}

##### Formation d'Ingénieurs assurées par des établissements publics :

* [Polytech Sorbonne (Paris)](https://www.polytech.sorbonne-universite.fr/){target="_blank"}
* [UTC Université de Technologie de Compiègne](https://www.utc.fr/){target="_blank"}


##### Formation d'Ingénieurs assurées par des écoles privées :

* [EFREI (Paris)](https://www.efrei.fr/){target="_blank"}
* [ESILV (Paris-La-Défense)](https://www.esilv.fr/ecole-dingenieurs-generaliste/ecole-ingenieur-informatique-paris/){target="_blank"}
* [ISEP (Paris)](https://www.isep.fr/){target="_blank"}
* [ECE (Paris)](https://www.ece.fr/){target="_blank"}
* [ESIEA (Paris/Ivry-sur-Seine)](https://www.esiea.fr/){target="_blank"}
* [CESI (Nanterre)](https://www.cesi.fr/domaines/informatique-numerique/){target="_blank"}
* [ESIEE (Paris)](https://www.esiee.fr/fr){target="_blank"}
* [EPITA (Paris)](https://www.epita.fr/){target="_blank"}

### Classes Préparatoires aux Grandes Écoles d'ingénieurs (CPGE)

  * filières généralistes : MPSI, PCSI, PTSI
  * filière dédiée aux profils "Informatique" : MP2I