#--- HDR ---#
docs = {
    "Administratif":{
        "certificat_JDC.pdf": 1500,
        "attestation_recensement.pdf": 850
    },
    "Cours": {
        "NSI": {
            "TP.html": 60,
            "dm.odt": 345
        },
        "Philo": {
            "Tractatus_logico-philosophicus.epub": 2600
        }
    },
    "liste_de_courses.txt": 24
}
#--- HDR ---#
def parcourt(racine, adr):
    repertoire = racine
    for nom_repertoire in adr:
        repertoire = ...
    return repertoire

# un test
print(parcourt(docs, ["Cours", "Philo"]))