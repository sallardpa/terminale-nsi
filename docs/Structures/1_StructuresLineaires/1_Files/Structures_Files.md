
# Les files (files d'attente)

Voici ci-dessous une scène classique : la file d'attente devant le chaudron de potion magique que le druide Panoramix va distribuer à tous les Gaulois (sauf à Obélix qui est tombé dedans quand il était petit).

![](data/asterix.png){: .center width 60%}

On voit dans cette file d'attente :

* Astérix
* Cétautomatix
* Ordralfabétix
* Bonemine
* Abraracourcix
* et deux autres Gaulois non identifiés.

## ^^1. Fonctionnement d'une file d'attente^^


Le fonctionnement d'une file d'attente est simple : 

* quand une nouvelle personne arrive, elle se place à la fin de la file d'attente ;
* quand la personne qui est en tête de la file d'attente est servie, elle quitte la file d'attente.

!!! tip "Règle de fonctionnement d'une file :heart:"
    Une file d'attente suit la règle du **"premier arrivé, premier servi/sorti"** (:flag_gb: :flag_us: _FIFO_, pour _First In, First Out_).

> Remarque : on considère qu'il n'y a pas d'autre action possible sur la file d'attente. Par exemple, il est impossible à une personne au milieu de la file de partir avant d'avoir été servie ; ou bien il est impossible de resquiller (il est interdit de rentrer dans la file en passant devant des personnes déjà présentes).

Par ailleurs, à tout moment, on peut savoir si la file d'attente est vide ou pas.

## ^^2. Concept de structure de données abstraite^^

La file d'attente devant le chaudron de potion magique peut donc être vue comme :

* un ensemble de données : ici les noms des personnages ("Astérix", "Cétautomatix", etc.) ;
* muni d'une structure : ici l'ordre d'arrivée des personnages ;
* sur lequel on peut agir de deux manières uniquement :
    
    - en ajoutant une nouvelle donnée à la fin de la file d'attente (on dit aussi : en queue de la file) ;
    - en enlevant la première donnée située en tête de la file ;

La file d'attente est le premier type de ***structure de données abstraite*** que nous rencontrons. 

## ^^3. Interface d'une file d'attente^^

L'**interface** minimale d'une file d'attente comporte les opérations suivantes :

| Fonction | Description |
| :------ | :------- | 
| `creer_file_vide()` | renvoyer une file d'attente vide |
| `enfiler(f,donnée)` | ajouter ("en-filer") dans la file `f`, en dernière position, la valeur `donnée`|
| `defiler(f)` | enlever ("dé-filer") la donnée qui est en tête de la file `f` et renvoyer cette donnée  |
| `est_vide(f) ` | renvoyer `True` si `f` est vide et `False` sinon |

Dans le cas de la file d'attente devant le chaudron de potion magique, on a donc eu la séquence d'événements suivants :

    fileChaudron = creer_file_vide() # quand Panoramix installe son chaudron
    enfiler(fileChaudron, "Asterix")
    enfiler(fileChaudron, "Cétautomatix")
    enfiler(fileChaudron, "Ordralfabétix")
    enfiler(fileChaudron, "Bonemine")
    ....

puis, quand Panoramix commence sa distribution, on aura :

    defiler(fileChaudron) # c'est Astérix qui est servi
    defiler(fileChaudron) # c'est Cétautomatix qui est servi
    ....

!!! faq "Exercice 1"
    Voici une succession d'actions :
        
        1) maFile = creer_file_vide()
        2) enfiler(maFile, "Alice")
        3) enfiler(maFile, "Bob")
        4) defiler(maFile)
        5) enfiler(maFile, "Charlie")
        6) enfiler(maFile, "Daphney")
        7) defiler(maFile)
        
    Donner l'état de la file `maFile` à chaque étape, en convenant que la tête de la file est l'élément le plus à droite (comme dans la file devant le chaudron de potion magique). 

??? danger "Solution"
    Voici l'état de la file d'attente `maFile` aux différentes étapes :

        étape 1 : (file vide)
        étape 2 : Alice
        étape 3 : Bob, Alice
        étape 4 : Bob
        étape 5 : Charlie, Bob
        étape 6 : Daphney, Charlie, Bob
        étape 7 : Daphney, Charlie 

    ![](data/schemaFileAlice.png){: .center width 60%)}

!!! info "Quelques situations qui font intervenir des files"
    Outre les situations de la vie courante (files d'attente à la caisse d'un supermarché, à l'entrée d'un cinéma ou d'un stade), on rencontre les files en informatique :

    * ***services client/serveur (Web)*** : une demande de connexion correspond à une entrée dans la file, le traitement de la demande de connexion par le serveur correspond à une sortie de la file.
    *  ***ordonnancement*** des processus par un système d'exploitation multi-tâches : une façon rudimentaire pour un système d'exploitation de cadencer plusieurs processus devant s'exécuter "en même temps" est de le gérer selon la règle _First In First Out_ (voir le cours dans le chapitre dédié).
  

## ^^4. Implémentation d'une file d'attente : version procédurale^^

Pour implémenter en Python une file d'attente, on pense naturellement à utiliser un tableau (de type `list`) pour stocker les données, avec la convention que la tête de la file d'attente correspond à la case n°0 du tableau . Cela revient à entrer dans la file par la droite et à en sortir par la gauche.  

Une possibilité d'implémentation (_de mettre en œuvre concrètement_) la ***structure abstraite*** de **file** est alors la suivante :

```python
def creer_file_vide()  :
    return []

def enfiler(f, donnée)  :
    f.append(donnée) # ajouter en queue de la file
    return f # en fait, c'est superflu : en Python, la liste "file" est modifiée en place

def defiler(f)  :
    tête_de_file = f.pop(0) # enlever la première valeur de la file
    return tête_de_file

def est_vide(f)  :
    return len(f) == 0 # renvoie le booléen True si la longueur est nulle et False sinon
```

Une façon d'utiliser cette implémentation est alors :

{{ IDE("data/fileAttenteClassiqueCorps") }}:

!!! faq "Exercice 2"

    Modifier le code du corps du programme afin de :

    * mettre le personnage Obélix dans la file d'attente, juste après Astérix ;
    * et que, dans la boucle `while`, quand vient le tour d'Obélix le message soit "Non, pas toi Obélix" au lieu de "Obélix a reçu sa gorgée de potion"

??? danger "Solution"
    
    * On ajoute après la ligne 3 `enfiler(fileChaudron,"Obélix"`).
    * à l'intérieur de la boucle `while`, on ajoute une instruction conditionnelle :

    ```python
    if servi == "Obélix" :
        print(""Non, pas toi Obélix")
    else :
        print(servi, " a reçu sa gorgée de potion")
    ```


!!! faq "Exercice 3"

    Si on a une file d'attente vide, on ne peut évidemment pas enlever la première donnée de cette file : cela signifie que l'exécution de la fonction `defiler` sur une file d'attente vide va provoquer une erreur.
    
    Quelle instruction de type `assert` faudrait-il rajouter au début de la fonction `defiler` afin de "protéger" cette fonction ?

??? danger "Solution"
    On ajoute l'instruction `assert not est_vide(f), "impossible de défiler une file vide"`.

## ^^5. Implémentation d'une file d'attente : version Programmation Orientée Objet^^

Une autre façon d'implémenter une file d'attente est de créer un objet "File d'attente", c'est-à-dire en Python de créer une **classe File** et de lui adjoindre :

* comme **attribut** : un tableau `contenu`, qui est initialisé dans la méthode constructeur `init` avec un tableau vide ;
* et les **méthodes** spécifiées par l'**interface** : `enfiler`, `defiler`, etc.

```python
class File :
    def __init__(self) :
        self.contenu = [] # création d'une file vide

    def enfiler(self, donnée) :
        self.contenu.append(donnée) # ajouter en queue de la file
    
    def defiler(self) :
        tête_de_file = self.contenu.pop(0) # enlever la première valeur de la file
        return tête_de_file

    def est_vide(self) :
        return len(self.contenu) == 0 # renvoie le booléen True si la longueur est nulle et False sinon
```

:warning: _À noter :_ on n'a pas créé de méthode `créer_file_vide` car cela est équivalent à instancier un objet de cette classe `File`.


On peut alors utiliser cette classe de la façon suivante, en créant une **instance** `fileChaudron` de la classe `File` :

{{ IDE("data/fileAttentePOOCorps") }}:


!!! faq "Exercice 4"

    En appuyant sur la bouton Lancer, vous allez exécuter un code caché qui instancie un objet `maFile` de la classe `File`. 

    {{IDE("data/fileMystere", SANS="print")}}

    {{ multi_qcm(
        [
            "Quelle valeur y a-t-il en tête de la file ?",
            ["1","8","13","21"],[2],
        ],
        )}}

    ??? danger "Solution"
        On tape en console `maFile.defiler()`, ce qui renvoie la valeur 8. 

    

## ^^6. Exercices et TP sur les files^^

* Généralités : 
    - Files en programmation procédurale : télécharger le [carnet Jupyter](TP_Exos_Files_ProgProcedurale.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.
    - Files en POO : télécharger le [carnet Jupyter](TP_Exos_Files_ProgOO.ipynb).

* Exercice type Bac n°1 :
    - [énoncé](Structures_Files_SujetBac_22NSIJ1AN1.pdf), à faire d'abord sur feuille ;
    - puis codage sur un [carnet Jupyter](Structures_Files_SujetBac_22NSIJ1AN1.ipynb).
    - en dernier recours, un corrigé [ici](https://pixees.fr/informatiquelycee/term/suj_bac/index.html){target="_blank"} (année 2022, sujet 6).


* Exercice type Bac n°2 :
    - [énoncé](Structures_Files_SujetBac_22NSIJ1G11.pdf), à faire d'abord sur feuille ;
    - puis codage et applications sur un [carnet Jupyter](Structures_Files_SujetBac_22NSIJ1G11.ipynb).
    - en dernier recours, un corrigé [ici](https://e-nsi.gitlab.io/ecrit/Struct/22-G11-J1-ex2/){target="_blank"}.






