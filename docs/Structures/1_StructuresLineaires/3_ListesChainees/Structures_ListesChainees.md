# ⛓️ Les listes chainées ⛓️

!!! warning "Attention au vocabulaire"
    Ne pas confondre la structure de données abstraites **Liste chainée** (ou simplement **liste** dans certains manuels) avec le type `list` de Python.

    Le type `list` de Python s'apparente à une implémentation de la structure de **tableau dynamique** (_hors-programme_).


## ^^1. Notion de liste chainée^^

La structure **liste chainée** fait partie, comme les files (d'attente) et les piles, des **structures de données linéaires** : les données sont "alignées" les unes après les autres. 

Ce qui caractérise une **liste chainée**, ce n'est pas le mode de gestion des entrées et sorties des données (_FIFO_ pour les files, _LIFO_ pour les piles) mais c'est qu'elle offre un moyen de passer d'une donnée à la suivante.

L'idée de base de la notion de structure de **liste chainée** est :

* on forme une "chaine" avec des "maillons" ;
* un ***maillon*** de la chaine contient 
    - la donnée elle-même
    - et un lien ou "pointeur" vers le maillon suivant ;
* et **on "tient" la chaine par son premier maillon**.

!!! done "Exemple : liste chainée des rois de France"

    La dynastie des Bourbons, rois de France qui ont régné de 1589 à 1792, peut être représentée par la liste chainée suivante :

    ![](data/schema-liste-chainee2.png){: .center width=60%}


!!! tip "Définition récursive d'une **liste** en informatique :heart:"
    L'idée de base exposée plus haut laisse entrevoir qu'une liste, c'est :

    - soit une liste vide, 
    - soit un élément suivi d'une autre liste (éventuellement vide).

    On est donc devant une définition ***récursive*** d'une liste !

!!! info "Analogie avec les poupées russes"
    Une ***poupée russe*** est une poupée en bois qui contient :

    - soit rien du tout,
    - soit une autre ***poupée russe***.

    ![](data/matreshka.png){.center width = 50%}

    Si on prénomme Alicia, Elena, Natacha, Svetlana et Tatyana les cinq poupées russes de cette photo (de la plus grande à la plus petite), alors on peut les représenter par la **liste chainée**
    "Alicia" → "Elena" → "Natacha" → "Svetlana" → "Tatyana" →  ⟂.

    _Remarque :_ le symbole "⟂" représente une liste vide, ce qui marque la fin de la liste chainée.

!!! question "Exercice : utiliser la définition récursive d'une liste chainée"

    On donne la construction suivante :

    - L est la liste dont le maillon vaut "Alice" et dont la liste suivante est vide ;
    - M est la liste dont le maillon vaut "Bob" et dont  la liste suivante est L ;
    - N est la liste dont le maillon vaut "Gaston" et dont  la liste suivante est M ;
    - P est la liste dont le maillon vaut "Jeanne" et dont  la liste suivante est N.

    Dessiner la liste chainée P.

??? danger "Solution"
    La liste chainée P est  "Jeanne" → "Gaston" → "Bob" → "Alice" → ⟂.

    Ceci illustre bien le principe : **on "tient" la chaine par son premier maillon.**

## ^^2. Interface d'une liste chainée^^

L'**interface** minimale d'une liste chainée comporte les opérations  (_les primitives_) suivantes :

| Fonction | Description |
| :------ | :------- | 
| `creer_liste_vide()` | renvoyer une nouvelle liste chainée vide |
| `tete(lst)` ou `head(lst)` | renvoyer la valeur du maillon qui est en tête de la liste chainée `lst`  |
| `queue(lst)` ou `tail(lst)` | renvoyer la liste chainée qui forme la queue de la liste chainée`lst`   |
| `inserer_en_tete(lst, val)` | modifie la liste chainée `lst` en lui ajoutant en tête la valeur `val` |
| `est_vide(lst) ` | renvoyer `True` si `lst` est vide et `False` sinon |

!!! done "Exemple d'utilisation avec la liste chainée des rois de France"
    Si on note `Dynastie_Bourbons` la liste chainée de l'exemple initial, alors :

    - `tête(Dynastie_Bourbons)` renvoie la valeur `"Henri IV"`
    - `queue(Dynastie_Bourbons)` renvoie la liste chainée "Louis XIII" → "Louis XIV" → "Louis XV" → "Louis XVI" → ⟂, c'est-à-dire tout ce qui vient après la "tête" de la liste `Dynastie_Bourbons`.

!!! question "Une liste chainée créée à partir d'un tableau"
    
    {{ multi_qcm(
        [
            "
            On considère le code Python suivant :
            
            ```python
            tab = ['Alice', 'Bob', 'Charlie']
            chain = creer_liste_vide()
            for elt in tab :
                inserer_en_tete(chain, elt)
            print(tete(chain))
            ```
            
            Qu'obtient-on à l'affichage ?
            ",
            ["Alice", "Bob", "Charlie", "Error"],[3],
        ],
        shuffle = False) }}
    


## ^^3. Implémentation d'une liste chainée : version Programmation Orientée Objet^^

Une façon d'implémenter une liste chainée est de créer d'abord un objet "Maillon", c'est-à-dire en Python de créer une **classe Maillon** qui a deux **attributs** :

* la donnée à stocker,
* et le maillon suivant, qui est lui-même un objet de la classe `Maillon` (éventuellement vide).

Un maillon vide est représenté par la valeur `None`. On peut noter que cette classe `Maillon` n'a pas de _méthodes_ dédiées.

```python
class Maillon :
    def __init__(self, val = None, suiv = None)  :
        self.donnée = val
        self.suivant = suiv
```

> Remarque : on a mis `None` en valeur par défaut à la fois pour la donnée et pour le maillon suivant.

 
Puis on crée une classe `ListeChainée` qui a :

- un seul attribut `premier_maillon`, de la classe `Maillon`
- et toutes les méthodes demandées par l'interface.

{{ IDE("data/listeChainesPOO_Full") }}

Voici un exemple d'utilisation :
```python
bourbons = ListeChainée()
bourbons.inserer_en_tete("Louis XVI")
bourbons.inserer_en_tete("Louis XV")
bourbons.inserer_en_tete("Louis XIV")
bourbons.inserer_en_tete("Louis XIII")
bourbons.inserer_en_tete("Henri IV")
print(bourbons.queue().tete())
```

!!! question "Exercice  : création d'une chaine alimentaire"

    Voici une chaine alimentaire :

    ![](data/chaineAlimentaire.png){: .center width 60%}

    **1.** Créer une variable `chaineAlimentaire` de la classe `ListeChainée` qui représente cette chaine alimentaire .

    **2.** Quelle est la valeur affichée par l'instruction `print(chaineAlimentaire.queue().tete())` ?

    {{ IDE() }}

    ??? danger "Solution"

        On écrit le code suivant :
    
        ```python
        chaineAlimentaire = ListeChainée()
        chaineAlimentaire.inserer_en_tete("owl")
        chaineAlimentaire.inserer_en_tete("snake")
        chaineAlimentaire.inserer_en_tete("mouse")
        chaineAlimentaire.inserer_en_tete("corn")
        ```


!!! question "Exploration d'une liste chainée "
    En appuyant sur le bouton "Lancer", vous allez exécuter un code masqué qui crée (_qui instancie_) un objet `maListeMystere` de la classe `ListeChainée`. 

    En vous servant des méthodes de cet objet, explorer cette liste chainée puis la représenter.

    {{ IDE("data/listeChainesPOO_Mystere") }}

    ??? danger "Solution"
        La liste chainée est : Or → Argent → Bronze → Chocolat → ⟂.

        On l'obtient en saisissant `maListeMystere.tete()`, puis `maListeMystere.queue().tete()`, etc.

## ^^4. Implémentation d'une liste chainée : version procédurale, à l'aide de tableaux^^

On peut choisir d'implémenter la structure de données de **liste chainée** par un tableau de type `list` en Python.

Dans le code ci-dessous, on fait le choix de placer la donnée de tête *à la fin du tableau*, de sorte que la fonction `insérer_en_tête` se code avec la méthode `append`.

{{ IDE("data/listeChainesProcedural") }}

Voici un exemple d'utilisation :
```python
bourbons = creer_liste_vide()
inserer_en_tete(bourbons, "Louis XVI")
inserer_en_tete(bourbons, "Louis XV")
inserer_en_tete(bourbons, "Louis XIV")
inserer_en_tete(bourbons, "Louis XIII")
inserer_en_tete(bourbons, "Henri IV")
print(tete(queue(bourbons)))

```


!!! question "Exercice  : retour sur la chaine alimentaire"

    ![](data/chaineAlimentaire.png){: .center width 60%}

    **1.** Coder avec cette nouvelle implémentation la variable `chaineAlimentaire` en utilisant uniquement les fonctions de l'interface.

    **2.** Que doit-on saisir, en utilisant uniquement la variable `chaineAlimentaire`et les fonctions de l'interface, pour obtenir l'affichage de `"snake"` ?

    {{ IDE() }}

    
    ??? danger "Solution" 
        
        ```python
        chaineAlimentaire = creer_liste_vide()
        inserer_en_tete(chaineAlimentaire, "owl")
        inserer_en_tete(chaineAlimentaire, "snake")
        inserer_en_tete(chaineAlimentaire, "mouse")
        inserer_en_tete(chaineAlimentaire, "corn")
        print(tete(queue(queue(chaineAlimentaire))))
        ```


## ^^5. Longueur d'une liste chainée^^

Supposez que l'on vous demande la longueur d'une liste chainée `maListeChainée` qui a une queue (`queue(maListeChainée)`) de longueur 7 : vous répondrez sans hésitation que la longueur vaut 1+7= 8.

![](data/longueurListe.png){: .center width=60%}

Cette "structure imbriquée" (en "poupée russe") d'une liste chainée permet de définir une **fonction récursive** de calcul de longueur d'une liste chainée :

    FONCTION LongueurListeChainée(lst)

        SI est_vide(lst) ALORS
            RENVOYER 0 # cas de base
        SINON
            RENVOYER 1 + LongueurListeChainée(queue(lst))

En exercice, vous devrez coder en Python cette fonction pour chacun des deux modes de représentation (_pour chaque implémentation_) d'une liste chainée présentés plus haut.


## ^^6. Exercices et TP^^

* Généralités : télécharger le [carnet Jupyter](TP_Exos_ListesChainees.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac :

    - [énoncé](Structures_ListesChainees_SujetBac_22NSIJ2AN1.pdf) à faire d'abord sur feuille,
    - puis codage dans un [carnet Jupyter](Structures_ListesChainees_SujetBac_22NSIJ2AN1.ipynb).
    - en cas de blocage : éléments de correction [ici](https://pixees.fr/informatiquelycee/term/suj_bac/index.html) (année 2022, sujet 5).


