def creer_liste_vide() :
    return []

def inserer_en_tete(lst, val) :
    lst.append(val)

def tete(lst) :
    assert not est_vide(lst), "Pas de tête à une liste chainée vide !"
    return lst[len(lst)-1] # la valeur en dernière position du tableau

def queue(lst) :
    assert not est_vide(lst), "Pas de queue à une liste chainée vide !"
    return [lst[k] for k in range(0, len(lst)-1)] # tout le tableau sans la dernière valeur

def est_vide(lst) :
    return len(lst) == 0