#--- HDR ---#

class Maillon :
    def __init__(self, val = None, suiv = None)  :
        self.donnée = val
        self.suivant = suiv

class ListeChainée :
    def __init__(self, first = None):
        self.maillon_de_tete = first
    
    def tete(self):
        return self.maillon_de_tete.donnée
    
    def queue(self) :
        return ListeChainée(self.maillon_de_tete.suivant)
    
    def inserer_en_tete(self, val) :
        self.maillon_de_tete = Maillon(val, self.maillon_de_tete)
    
    def est_vide(self) :
        return self.maillon_de_tete is None

maListeMystere = ListeChainée()
maListeMystere.inserer_en_tete("Chocolat")
maListeMystere.inserer_en_tete("Bronze")
maListeMystere.inserer_en_tete("Argent")
maListeMystere.inserer_en_tete("Or")

#--- HDR ---#

print("Merci d'avoir executé le code : à vous de découvrir la liste chainée maListeMystere")