# Les piles

Considérons une pile d'enveloppes, chacune contenant un message envoyé par un destinataire.

![](data/pileEnveloppes.jpg){: .center width 60%}

Le fonctionnement d'une pile (:flag_gb: :flag_us: _stack_) est simple : 

* quand une nouveau message arrive, il se place en tête de la pile ;
* quand on veut consulter un message, on prend celui qui est en haut de la pile et ce message quitte la pile.

!!! tip "Règle de fonctionnement d'une pile :heart:"
    Une pile suit la règle du **"dernier arrivé, premier servi/sorti"** (:flag_gb: :flag_us: _LIFO_, pour _Last In, First Out_).


Par ailleurs, à tout moment, on peut savoir si la pile est vide ou pas.


## ^^1. La pile, une autre structure de données abstraite^^

La pile d'enveloppes peut donc être vue comme :

* un ensemble de données : ici les noms des personnes ayant écrits les messages ;
* muni d'une structure : ici l'ordre d'arrivée des messages ;
* sur lequel on peut agir de deux manières uniquement :
    
    - en ajoutant une nouvelle donnée au sommet de la pile ;
    - en enlevant la première donnée située au sommet de la pile ;


Une pile est donc un autre type de ***structure de données abstraite***. Elle se distingue de l'autre structure que nous connaissons (la file d'attente) par son mode de fonctionnement : "FIFO" pour une file, "LIFO" pour une pile. 



## ^^2. Interface d'une pile^^

L'**interface** minimale d'une pile comporte les opérations suivantes :

| Fonction | Description |
| :------ | :------- | 
| `creer_pile_vide()` | renvoyer une nouvelle pile vide |
| `empiler(p, donnée)` | ajouter ("em-piler") au sommet de la pile la valeur `donnée`|
| `depiler(p)` | enlever ("dé-piler") la donnée  qui est au sommet de la pile `p`  et renvoyer cette donnée  |
| `est_vide(p) ` | renvoyer `True` si `p` est vide et `False` sinon |

<!-- bla -->

!!! done "Exemple d'utilisation"

    ![](data/pile_exemple_cours_1.png){: .center width=60%}

    ![](data/pile_exemple_cours_2.png){: .center width=60%}

!!! faq "Exercice 1"
    Voici une succession d'actions :
        
        1) maPile = creer_pile_vide()
        2) empiler(maPile, "Alice")
        3) empiler(maPile, "Bob")
        4) depiler(maPile)
        5) empiler(maPile, "Charlie")
        6) empiler(maPile, "Daphney")
        7) depiler(maPile)
        
    Donner les états successifs de la pile `maPile`, en convenant d'écrire la pile comme une colonne de prénoms, le sommet de la colonne étant la tête de la pile. 

??? danger "Solution"
    Voici l'état de la pile `maPile` aux différentes étapes :
    
    ![](data/schemaPileAlice.png){: .center width 60%}

!!! info "Quelques situations qui font intervenir des piles"
    Outre les situations de la vie courante (pile d'assiettes, pile de journaux, etc.), on rencontre les piles en informatique :

    * ***Historique des sites visités et retour en arrière*** : chaque site Web visité est stocké dans une pile (`empiler`) et on peut revenir sur un site précédemment visité en cliquant sur le bouton "Retour en arrière" (`depiler`).
    *  ***Historique des modifications et annulation des modifications*** dans un logiciel (traitement de texte, etc.) : chaque modification est stockée dans une pile (`empiler`) et on peut annuler (`depiler`) la modification avec le bouton "annuler" ou les touches ++ctrl+"Z"++.
    * ***Pile d'appels des fonctions d'un programme*** : lorsqu'une fonction `a` appelle une autre fonction `b`, l'exécution de `a` est interrompue et son état (valeurs des variables, etc.) est stocké dans une pile. Une fois que la fonction `b` est terminée, l'exécution de `a` reprend en retrouvant son état de départ grâce au dépilage de la pile. Dans le cas de fonctions récursives, la pile d'appels peut être volumineuse, avec le risque d'un "débordement de pile" (:flag_gb: :flag_us: _stack overflow_).
    
## ^^3. Implémentation d'une pile: version procédurale^^

Pour implémenter en Python une pile, on peut procéder comme pour les files d'attente et utiliser un tableau (de type `list`) pour stocker les données.

Mais on se rend compte que, pour pouvoir utiliser les méthodes usuelles `append` et `pop` dans les fonctions `empiler` et `depiler`, le sommet de la pile doit correspondre à la fin du tableau. Un code possible est alors le suivant :

```python
def creer_pile_vide()  :
    return []

def empiler(p, donnée) :
    p.append(donnée) # on ajoute la donnée à la fin du tableau
    return p # en fait, c'est superflu : en Python, la liste "pile" est modifiée en place 

def depiler(p) :
    aEnlever = p.pop( len(p) - 1 ) # la donnée à enlever est positionnée à la fin du tableau
    return aEnlever

def est_vide(p)  :
    return len(p) == 0 # renvoie le booléen True si la longueur est nulle et False sinon
```

À titre pédagogique, on va garder le même type de données que dans l'exemple des files d'attente et on va supposer que :

* le druide Panoramix a reçu des messages d'Astérix, puis de Cétautomatix, puis d'Ordralfabétix, puis de Bonemine et enfin d'Abraracourcix ;
* et qu'il lit ensuite les messages.

{{ IDE("data/pileClassiqueCorps") }}:

!!! faq "Exercice 1"

    1. On suppose maintenant que Panoramix a reçu un message d'Obélix juste après celui d'Astérix. Quel message sera lu en premier par Panoramix : celui d'Astérix ou celui d'Obélix ?

    2. Vérifier votre réponse en insérant au bon endroit dans le corps du programme l'instruction correspondant à la réception du message d'Obélix.

??? danger "Solution"
    Le dépilage d'une pile se fait dans l'ordre inverse de l'empilage : puisque Obélix est le dernier arrivé, c'est lui le premier servi donc Panoramix lit d'abord le message d'Obélix.

!!! faq "Exercice 2"

    Si on a une pile vide, on ne peut évidemment pas enlever la première donnée de cette pile : cela signifie que l'exécution de la fonction `depiler()` sur une pile vide va provoquer une erreur.

    Quelle instruction de type `assert` faut-il rajouter au début de la fonction `depiler()` afin de "protéger" cette fonction ? 

    ??? danger "Solution"
    On ajoute l'instruction `assert not est_vide(p), "impossible de dépiler une pile vide"`.

## ^^4. Implémentation d'une pile : version Programmation Orientée Objet^^

Une autre façon d'implémenter une pile est de créer un objet "Pile", c'est-à-dire en Python de créer une **classe Pile** et de lui adjoindre :

* comme **attribut** : un tableau `contenu`, qui est initialisé dans la méthode constructeur `init` avec un tableau vide ;
* et les **méthodes** spécifiées par l'**interface** : `empiler`, `depiler` et `est_vide`.

```python
class Pile :
    def __init__(self) :
        self.contenu = [] # création d'une pile vide

    def empiler(self, donnée) :
        self.contenu.append(donnée)  # ajouter en tête de la pile = à la fin du tableau
    
    def depiler(self) :
        aEnlever= self.contenu.pop( len(self.contenu) - 1) #  la donnée à enlever est positionnée à la fin du tableau
        return aEnlever

    def est_vide(self) :
        return len(self.contenu) == 0
```

:warning: _À noter :_ on n'a pas créé de méthode `créer_pile_vide` car cela est équivalent à instancier un objet de cette classe `Pile`.

On peut alors utiliser cette classe de la façon suivante, en créant une **instance** `pileMessages` de la classe `Pile` :

{{ IDE("data/pilePOOCorps") }}:


!!! faq "Exercice 3"

    Protéger par une instruction de type `assert` la méthode `depiler`.

??? danger "Solution"

    On ajoute l'instruction `assert not self.est_vide(), "impossible de défiler une pile vide"`.


!!! faq "Exercice 4"

    En appuyant sur la bouton Lancer, vous allez exécuter un code caché qui instancie un objet `maPile` de la classe `Pile`. 

    {{IDE("data/pileMystere")}}

    {{ multi_qcm(
        [
            "Quelle valeur y a-t-il tout en bas de la pile ?",
            ["1","8","13","21"],[2],
        ],
        )}}

    ??? danger "Solution"
        On tape en console `maPile.depiler()` jusqu'à obtenir un message d'erreur, qui signale qu'on essaie de dépiler une pile vide. La dernière valeur rencontrée est 8, qui est donc celle en bas de la pile.

## ^^5. Exercices et TP^^

* Généralités : 

    - Piles en programmation procédurale : télécharger le [carnet Jupyter](TP_Exos_Piles_Procedurale.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.
    - Piles en POO : télécharger le [carnet Jupyter](TP_Exos_Piles_POO.ipynb).

* Exercice type Bac n°1 :

    - [énoncé](Structures_Piles_SujetBac_J2ME1.pdf), à faire d'abord sur papier
    - mise en pratique et compléments avec un [carnet Jupyter](Structures_Piles_SujetBac_J2ME1.ipynb), à faire ensuite
    - en dernier recours, un corrigé [ici](https://pixees.fr/informatiquelycee/term/suj_bac/index.html){target="_blank"} (année 2022, sujet 3).


* Exercice type Bac n°2 (_piles et files_) :

    - [énoncé](Structures_PilesFiles_SujetBac_21AmNord1.pdf), à faire sur papier
    - en dernier recours, un corrigé [ici](Structures_PilesFiles_SujetBac_21AmNord1_corrige.pdf).



