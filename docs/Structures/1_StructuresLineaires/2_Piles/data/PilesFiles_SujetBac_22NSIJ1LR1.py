# Structure de file, avec un tableau
def creer_file_vide()  :
    return []

def enfiler(f, donnée)  :
    f.append(donnée) 

def defiler(f)  :
    tête_de_file = f.pop(0)
    return tête_de_file

def est_file_vide(f)  :
    return len(f) == 0

def taille_file(f) : # ajout par rapport à la structure minimale du cours
    return len(f)

# Structure de pile, avec un tableau
def creer_pile_vide()  :
    return []

def empiler(p, donnée) :
    p.append(donnée)

def depiler(p) :
    aEnlever = p.pop( len(p) - 1 ) 
    return aEnlever

def est_pile_vide(p)  :
    return len(p) == 0

def sommet(p) : # ajout par rapport à la structure minimale du cours
    return p[len(p) - 1]

# Recopier à partir d'ici le code de la fonction knuth


# créer à partir d'ici une file de 4 éléments ou plus


# test avec cette file
knuth(f)
print(f)