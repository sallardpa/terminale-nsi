#--- HDR ---#
class Pile :
    def __init__(self) :
        self.pile = [] # création d'une pile vide

    def empiler(self, donnée) :
        self.pile.append(donnée)  # ajouter en tête de la pile = à la fin du tableau
    
    def depiler(self) :
        assert not self.est_vide(), "impossible de défiler une pile vide"
        aEnlever= self.pile.pop( len(self.pile) - 1) #  la donnée à enlever est positionnée à la fin du tableau
        return aEnlever

    def est_vide(self) :
        return len(self.pile) == 0


maPile = Pile()
maPile.empiler(8)
maPile.empiler(13)
maPile.empiler(21)
#--- HDR ---#
print("Merci d'avoir executé le code : à vous de découvrir la pile 'maPile'")
