---
title: Coloration d'un graphe
author: LFT, Franck Chambon
---

# TD : Coloration d'un graphe

!!! abstract "Le problème"
    ![](data/23-lft-gr-col.jpg){ align=right }

    Vous pouvez voir ci-contre une carte de France représentant les 12 régions métropolitaines (sans la Corse pour des raisons de simplification du problème).

    On se demande s'il est possible de colorier la carte en respectant les contraintes suivantes :

    - utiliser un minimum de couleurs ;
    - faire en sorte que deux régions limitrophes (ayant une frontière commune) soient coloriées de deux couleurs différentes.

    La figure ci-contre utilise 12 couleurs, une par région ; on peut faire mieux. Il y a des solutions optimales à 4 couleurs, d'autres qui s'en approchent. Nous allons voir plusieurs algorithmes.

!!! info "Modélisation"
    Pour tenter de résoudre ce problème, nous n'allons pas travailler directement sur la carte, mais sur un graphe associé à celle-ci.

    Chaque région sera un sommet du graphe et, si deux régions sont limitrophes, il existera alors une arête entre ces deux sommets. Pour nommer les sommets nous utiliserons les codes suivants :

    | Identifiant | Région                     |
    | ----------- | -------------------------- |
    | BRE         | Bretagne                   |
    | PLO         | Pays de Loire              |
    | NAQ         | Nouvelle Aquitaine         |
    | OCC         | Occitanie                  |
    | PAC         | Provence-Alpes-Côte d'Azur |
    | ARA         | Auvergne-Rhône-Alpes       |
    | BFC         | Bourgogne Franche-Comté    |
    | CVL         | Centre-Val de Loire        |
    | IDF         | Île de France              |
    | GES         | Grand-Est                  |
    | HDF         | Hauts-de-France            |
    | NOR         | Normandie                  |


## Partie I : Implémentation du graphe

**1.** Représentez sur votre copie le graphe correspondant à la carte "Régions de France" en utilisant les identifiants pour les sommets.

??? danger "Réponse"

    ![](data/mermaid-diagram-8.svg#only-light){ width=40%}
    ![](data/mermaid-diagram-8d.svg#only-dark){ width=40%}

<!--
```mermaid
    flowchart TB
        HDF --- NOR
        HDF --- IDF
        HDF --- GES
        NOR --- BRE
        NOR --- PLO
        NOR --- CVL
        NOR --- IDF
        IDF --- CVL
        IDF --- BFC
        GES --- IDF
        GES --- BFC
        BRE --- PLO
        PLO --- NAQ
        PLO --- CVL
        CVL --- NAQ
        CVL --- ARA
        CVL --- BFC
        BFC --- ARA
        NAQ --- OCC
        NAQ --- ARA
        ARA --- OCC
        ARA --- PAC
        PAC --- OCC
``` 
-->

**2.** On choisit de représenter un graphe en Python grâce à un dictionnaire où les clés sont les sommets et où la valeur associée à une clé est la liste de ses voisins.

Donner le code Python de `graphe_Régions` qui représente par un tel dictionnaire le graphe dessiné à la question précédente.

!!! question ""
    {{ IDE() }}

??? sucess "Réponse"
    ```python
    graphe_Régions = {
    "HDF" : ["IDF", "GES", "NOR"],
    "NOR" : ["BRE", "PLO", "CVL", "IDF", "HDF"],
    "IDF" : ["BFC", "CVL", "GES", "HDF", "NOR"],
    "GES" : ["BFC", "IDF", "HDF"],
    "BRE" :["PLO", "NOR"],
    "PLO" : ["BRE", "NAQ", "CVL", "NOR"],
    "CVL" : ["PLO", "NAQ", "ARA", "BFC", "IDF", "NOR"],
    "BFC" : ["ARA", "CVL", "IDF", "GES"],
    "NAQ" : ["PLO", "OCC", "ARA", "CVL"],
    "ARA" : ["NAQ", "OCC", "PAC", "BFC", "CVL"],
    "OCC" : ["NAQ", "PAC", "ARA"],
    "PAC" : ["OCC", "ARA"],
    }
    ```

    en ayant fait le choix de saisir les régions du Nord vers le Sud, et d'Ouest en Est.

**3.** Écrire en Python une fonction `voisins` telle que `voisins(G, k)` renvoie une liste contenant les voisins du sommet `k` dans le graphe qui est modélisé par le dictionnaire `G` de listes de voisins. On aura, avec l'exemple précédent, et sans tenir compte de l'ordre dans lequel apparaissent les voisins :

```pycon
>>> voisins(graphe_Régions, 'BRE')
['PLO', 'NOR']
```

!!! question ""
    {{ IDE() }}


??? danger "Réponse"
    ```python
    def voisins(G, k):
        return G[k]
    ```



## Partie II : Coloration séquentielle ; un premier algorithme

Une première approche pour colorer le graphe est de prendre ses sommets les uns après les autres afin de leur affecter une couleur, tout en veillant à ce que deux sommets adjacents n'aient jamais la même couleur : c'est l'algorithme de coloration séquentielle. 

On propose le code ci-dessous qui utilise la fonction `voisins` programmée à la question précédente :

!!! question ""
    {{IDE('data/colorationSeq')}}


**1.** À Quelle famille d'algorithme appartient le Code 1 ci-dessus ?

??? danger "Réponse"
    Il s'agit d'un algorithme glouton, il choisit la première couleur disponible et **ne remet pas en cause son choix** ensuite, même s'il n'est pas optimal.

**2.** Donner les couleurs que va attribuer ce code à chaque sommet si l'on considère le Graphe 1 ci-dessous. On considère que la boucle principale va envisager les sommets dans l'ordre lexicographique : tout d'abord A, puis B, puis C ...


!!! info "Graphe 1"

    ![](data/mermaid-diagram-9.svg#only-light){ width=50%}
    ![](data/mermaid-diagram-9d.svg#only-dark){ width=50%}

<!-- 
```mermaid
    flowchart LR
        A(A)
        B(B)
        C(C)
        D(D)
        E(E)
        F(F)
        A --- D
        A --- C
        C --- D
        D --- B
        C --- E
        B --- E
        E --- F
``` 
-->

??? danger "Réponse"
    Pour les sommets :

    - A, le choix est rapide : Rouge ;
    - B, le choix est également rapide : Rouge ;
    - C, le Rouge est indisponible, le choix est Bleu ;
    - D, Rouge et Bleu sont indisponibles, le choix suivant est Vert ;
    - E, Rouge et Bleu sont indisponibles, le choix suivant est Vert ;
    - F, Vert est indisponible, le premier choix Rouge est validé.

    Conclusion

    | Sommet | Couleur |
    | ------ | ------- |
    | A      | Rouge   |
    | B      | Rouge   |
    | C      | Bleu    |
    | D      | Vert    |
    | E      | Vert    |
    | F      | Rouge   |

    ![](data/mermaid-diagram-10.svg#only-light){ width=70%}
    ![](data/mermaid-diagram-10d.svg#only-dark){ width=70%}

<!--     
```mermaid
    flowchart LR
        A(A)
        B(B)
        C(C)
        D(D)
        E(E)
        F(F)
        A --- D
        A --- C
        C --- D
        D --- B
        C --- E
        B --- E
        E --- F
        style A fill:#f00,stroke:#000,stroke-width:2px,color:#000
        style B fill:#f00,stroke:#000,stroke-width:2px,color:#000
        style F fill:#f00,stroke:#000,stroke-width:2px,color:#000
        style C fill:#00f,stroke:#000,stroke-width:2px,color:#000
        style D fill:#0f0,stroke:#000,stroke-width:2px,color:#000
        style E fill:#0f0,stroke:#000,stroke-width:2px,color:#000
``` 
-->

**3.** Appliquer cet algorithme au graphe des régions de France. De combien de couleurs avez-vous besoin ?

!!! question ""
    {{ terminal() }}

??? danger "Réponse"
    On saisit dans le terminal la commande `coloration(graphe_Régions)`.
    
    Le résultat va dépendre de l'ordre dans lequel est parcouru les clés du dictionnaires (c'est-à-dire de _leur ordre de création_ dans le dictionnaire).

    Avec le code du graphe donné en correction plus haut, on a besoin de 4 couleurs, avec les colorations suivantes :

    | id Région | Couleur |
    | --------- | ------- |
    | HDF       | Rouge   |
    | NOR       | Bleu    |
    | IDF       | Vert    |
    | GES       | Bleu    |
    | BRE       | Rouge   |
    | PLO       | Vert    |
    | CVL       | Rouge   |
    | BFC       | Jaune   |
    | NAQ       | Bleu    |
    | ARA       | Vert    |
    | OCC       | Rouge   |
    | PAC       | Bleu   |



**4.** On modélise maintenant un nouveau graphe avec le dictionnaire `G_2` ci-dessous, et on appelle la fonction `coloration` avec ce graphe en paramètre. Représentez ce graphe sur votre copie et montrez qu'il existe une solution avec moins de couleurs.

```pycon
>>>G_2 = {"A": ["C"], "B": ["D"], "C": ["A", "D"], "D": ["B", "C"]}
>>> coloration(G_2)
{'A': 'Rouge', 'B': 'Rouge', 'C': 'Bleu', 'D': 'Vert'}
```

??? danger "Réponse"

    ![](data/mermaid-diagram-11.svg#only-light){ width=50%}
    ![](data/mermaid-diagram-11d.svg#only-dark){ width=50%}

    On peut proposer une coloration avec seulement deux couleurs :

    | Sommet | Couleur |
    | ------ | ------- |
    | A      | Rouge   |
    | B      | Bleu    |
    | C      | Bleu    |
    | D      | Rouge   |


    ![](data/mermaid-diagram-12.svg#only-light){ width=50%}
    ![](data/mermaid-diagram-12d.svg#only-dark){ width=50%}


<!--
```mermaid
    flowchart LR
    A(A)
    B(B)
    C(C)
    D(D)
    A --- C
    D --- B
    C --- D
    style A fill:#f00,stroke:#000,stroke-width:2px,color:#000
    style B fill:#f00,stroke:#000,stroke-width:2px,color:#000
    style C fill:#00f,stroke:#000,stroke-width:2px,color:#000
    style D fill:#0f0,stroke:#000,stroke-width:2px,color:#000
``` 
```mermaid
    flowchart LR
    A(A)
    B(B)
    C(C)
    D(D)
    A --- C
    D --- B
    C --- D
    style A fill:#f00,stroke:#000,stroke-width:2px,color:#000
    style D fill:#f00,stroke:#000,stroke-width:2px,color:#000
    style B fill:#00f,stroke:#000,stroke-width:2px,color:#000
    style C fill:#00f,stroke:#000,stroke-width:2px,color:#000
```
-->


## Partie III : L'algorithme de Welsh et Powell

Il s'agit maintenant d'utiliser l'algorithme de coloration séquentiel avec un ordre judicieux, en vue d'obtenir une coloration valide la plus « acceptable » possible. 

L'algorithme de Welsh et Powell consiste ainsi à colorer séquentiellement le graphe en visitant les sommets par ordre de degré décroissant. 

L'idée est que les sommets ayant beaucoup de voisins sont plus difficiles à colorer : il faut les colorier en premier.

!!! info "Rappel : degré d'un sommet"
    On rappelle que le degré d'un sommet est le nombre d'arêtes issues de ce sommet : cela donne également le nombre de ses voisins.

    Par conséquent, l'expression Python `len(G['A'])`  nous donne le degré d'un sommet `A` dans un graphe `G`.

**1.** On souhaite implémenter en Python une fonction `tri_sommets` telle que `tri_sommets(G)` renvoie la liste des étiquettes des sommets du graphe `G` passé en paramètre triés par degrés **décroissants**.

Recopiez les lignes incomplètes de code en les complétant.

!!! question ""
    {{IDE('data/colorationTri')}}

??? danger "Réponse"

    ```python hl_lines="4 7 9 10 13"
    def tri_sommets(G) :
        """Renvoie la liste des sommets, triée par degrés décroissants"""
        sommets = [sommet for sommet in G]
        for i in range(len(sommets)):
            i_sommet_max = i
            degre_sommet_max = len(G[sommets[i]])
            for j in range(i + 1, len(sommets)):
                if len(G[sommets[j]]) > degre_sommet_max:
                    i_sommet_max = j
                    degre_sommet_max = len(G[sommets[j]])
            tmp = sommets[i]
            sommets[i] = sommets[i_sommet_max]
            sommets[i_sommet_max] = tmp
        return sommets
    ```


**2.** Quel type de tri est implémenté dans la fonction `tri_sommets` dans le Code 2 ci-dessus ?

??? danger "Réponse"
    Il s'agit d'un tri par sélection : on cherche le plus grand élément dans le tableau qui reste à trier et on le sélectionne pour être échangé au début de la section à trier, on poursuit avec le reste du tableau.


**3.** Quelle sera la liste renvoyée par l'appel de fonction `tri_sommets(graphe_Régions)` où on passe en paramètre le dictionnaire `graphe_Régions` de la partie I.


??? danger "Réponse"
    ```pycon
    >>> tri_sommets(R)
    ['CVL', 'ARA', 'IDF', 'NOR', 'PLO', 'BFC', 'NAQ', 'GES', 'HDF', 'OCC', 'PAC', 'BRE']
    ```

**4.** Pour implémenter cet algorithme de Welsh-Powell, il suffit de faire une légère modification à l'algorithme séquentiel (celui en partie II). Écrire alors le code de cet algorithme.


!!! question ""
    {{IDE('data/colorationWP')}}


??? danger "Réponse"

    ```python hl_lines="8"
    def coloration_WelshPowell(G) :
        """Renvoie une coloration du graphe G
        selon l'algorithme de Welsh-Powell, 
        limité à 6 couleurs"""

        couleur = ['Rouge', 'Bleu', 'Vert', 'Jaune', 'Noir', 'Blanc']
        coloration_sommets = {s_i: None for s_i in G}
        for s_i in tri_sommets(G):
            couleurs_voisins_s_i = [coloration[s_j] for s_j in voisins(G, s_i)]
            k = 0
            while couleur[k] in couleurs_voisins_s_i:
                k = k + 1
            coloration_sommets[s_i] = couleur[k]
        return coloration_sommets
    ```

**5.** Donnez alors la couleur attribuée à chaque région par votre fonction. De combien de couleurs au total avez-vous besoin pour colorier la carte des régions de France ?

??? danger "Réponse"
    On a besoin de 4 couleurs, avec les colorations suivantes (en gras, les changements de  couleurs par rapport au résultat du premier algorithme). Il s'avère que, dans ce cas particulier, 
    le premier algorithme donnait déjà une solution optimale.

    | id Région | Couleur |
    | --------- | ------- |
    | HDF       | Rouge   |
    | NOR       | Bleu    |
    | IDF       | Vert    |
    | GES       | Bleu    |
    | BRE       | Rouge   |
    | PLO       | Vert    |
    | CVL       | Rouge   |
    | BFC       | Jaune   |
    | NAQ       | **Jaune**  |
    | ARA       | **Bleu**   |
    | OCC       | **Rouge**  |
    | PAC       | **Vert**   |
