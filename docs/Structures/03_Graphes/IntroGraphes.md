# Introduction aux Graphes 🕸️

## ^^1. Exemples introductifs^^


#### Réseau social

![](data/GameThrones.png){: .center width=80%}

Sur ce graphe, les **sommets** sont les personnages de Game of Thrones et les **arêtes** ou **arcs** représentent leur degré d'interaction dans le 3e volume de la série. 

#### Réseau de transports, cartes

![](data/python-cours-metro-paris.jpg){: .center width=80%}

Sur ce graphe, les **sommets** sont des stations de métro et les **arcs** sont des portions de lignes de métro ou de RER.

La quasi-totalité des **arcs** fonctionnent à double-sens mais ce n'est pas vrai sur certaines portions : par exemple, en bout de ligne 7b, le métro circule à sens unique Botzaris → Place des Fêtes → Pré St-Gervais → Danube. Il y a une autre ligne qui a des portions à sens unique : la repérez-vous ?


#### Labyrinthe

![](data/laby.png){: .center width=80%}


À droite, on voit une modélisation sous forme de graphe, dont on pourra faire des **parcours** de graphe (en largeur ou en profondeur) pour résoudre des problèmes.


#### Graphes de prérequis

![](data/dépendances.jpg){: .center width=80%}

Ce graphe indique les modules requis pour accéder à d'autres dans un cursus universitaire en informatique, ou bien les lectures conseillées de chapitres avant d'aborder d'autres.


## ^^2. Définition et vocabulaire^^

Un **graphe** est un ensemble fini de **sommets** (ou nœuds) reliés entre eux par des **arcs**. 

La disposition des sommets et des arcs n'a pas d'importance : dans l'illustration ci-dessous, on a représenté un **même graphe de trois façons différentes**. 

![](data/graph_variantes.png){: .center width=80%}

* Un graphe peut être **orienté** : les arcs ont chacun un sens, ils indiquent une direction du sommet de départ vers le sommet d'arrivée.

```mermaid
flowchart LR
    A(A) --> B(B)
    B --> C(C)
    C --> D(D)
    C --> B
    D --> B
    D --> A
```


* Un graphe peut être **non orienté**, les arcs n'ont alors pas de sens en particulier. Dans ce cas, on parle plutôt d'**arêtes**.

![](data/mermaid-diagram-1.svg#only-light){ width=40%}
![](data/mermaid-diagram-1d.svg#only-dark){ width=40%}
<!-- 
```mermaid
flowchart LR
    A(A) --- E(E)
    E --- B(B)
    A --- C(C)
    C --- D(D)
    C --- B
    D --- B
    D --- A
``` 
-->



* Un graphe est **pondéré** quand chaque arc se voit attribuer une valeur numérique. Cette valeur peut représenter un coût d'utilisation de l'arc ou bien le temps nécessaire pour parcourir cet arc.

![](data/mermaid-diagram-2.svg#only-light){ width=60%}
![](data/mermaid-diagram-2d.svg#only-dark){ width=60%}
<!-- 
```mermaid
flowchart LR
    A(Routeur 1) ---|10| B(Routeur 2)
    A ---|1| C(Routeur 3)
    C ---|2| F(Routeur 4)
    C ---|10| D(Routeur 5)
    B ---|100| D
    F ---|3| D
    B ---|1| F
``` 
-->

* Un graphe est **connexe** s'il est d'un seul tenant (s'il est toujours possible d'aller d'un sommet à un autre en passant par des arcs du graphe ). Le graphe ci-dessous dont les sommets portent les lettres de A à G n'est pas connexe car on ne peut pas aller de A à E par exemple (mais il est formé de deux sous-graphes connexes).

![](data/mermaid-diagram-4.svg#only-light){ width=30%}
![](data/mermaid-diagram-4d.svg#only-dark){ width=30%}

<!-- 
```mermaid
flowchart LR
    A(A) --- B(B)
    A --- C(C)
    C --- D(D)
    C --- B
    D --- B
    E(E) --- F(F)
    E --- G(G)
``` 
-->

* La **taille** (ou l'***ordre***) d'un graphe est le nombre de ses sommets.

* Dans un graphe non orienté, le **degré** d'un sommet est le nombre d'arêtes partant de ce sommet.

* Deux sommets d'un graphe non orienté sont **adjacents** ou **voisins** s'ils sont reliés par une arête. 

* Un **chemin** entre deux sommets est une suite d'arêtes reliant ces deux sommets. Quand le sommet d'arrivée est le même que le sommet de départ, on parle d'un **cycle**.  

<!--     
```mermaid
    flowchart LR
    A(A) --- E(E)
    E --- B(B)
    A --- C(C)
    C --- D(D)
    C --- B
    D --- B
    D --- A
    ``` 
-->

!!! question "Caractériser un graphe"

    On considère le graphe suivant : 

    ![](data/mermaid-diagram-3.svg#only-light){ width=30%}
    ![](data/mermaid-diagram-3d.svg#only-dark){ width=30%}

    {{ multi_qcm(
        [
            "Quel est la taille du graphe ?",
            ["3", "4", "5", "6", "7"],[3],
        ],
        [
            "Quel est le degré du sommet C ?",
            ["3", "4", "5", "6", "7"],[1],
        ],
        [
            "Tous les sommets de ce graphe ont 3 voisins sauf un : lequel ?",
            ["A", "B", "C", "D", "E"],[5],
        ],
        [
            "Ce graphe comporte-t-il au moins un cycle ?",
            ["Oui", "Non"],[1],
        ],
        shuffle = False) }}



* Les **arbres** sont des graphes particuliers : ce sont des graphes connexes sans cycle !

![](data/mermaid-diagram-5.svg#only-light){ width=30%}
![](data/mermaid-diagram-5d.svg#only-dark){ width=30%}

<!-- 
```mermaid
flowchart TD
    A(A) --- B(B)
    A --- C(C)
    C --- D(D)
    C --- E(E)
    C --- F(F)
``` 
-->

!!! question "Exercices d'introduction aux graphes"

    Télécharger  la [feuille d'exercices](Fiche_Exo_Graphes.pdf).

    _Pour le premier exercice, vous pourrez vous aider du_ [plan de métro ci-dessus](data/python-cours-metro-paris.jpg){target=_blank}.

    ??? danger "Solution de l'exercice 1"
        ![](data/solution_exo_metro.png){: width=360px .center}

        Ce graphe n'est pas orienté ; il est connexe et contient plusieurs cycles (par exemple N → E → SL → N ).

        Le sommet de degré minimum est A (de degré 2). Les sommets de degré maximum sont N et SL (de degré 5).

        Le chemin A → N → L relie les sommets A et L (mais il y a d'autres chemins possibles)).


    ??? danger "Solution de l'exercice 2"
        * Questions 1 et 2 : on constate que la somme des degrés est égal au double du nombre d'arêtes. C'est logique puisque chaque arête est comptée deux fois, à chacune de ses deux extrémités.

        * Question 3 : il faut effacer les arêtes du sommet de degré minimal. Dans le `graphe_2`, ce sont les arêtes du sommet A qu'il faut effacer.


    ??? danger "Solution de l'exercice 3"
        * Question 1 : on obtient le tableau suivant.

        | Taille du graphe complet | Nombre total d'arêtes |
        | :---: | :---:|
        | 2 | 1 |
        | 3 | 3 |
        | 4 | 6 |
        | 5 | 10 |
        | 6 | 15 |

        * Le code de la fonction est 
        ```python
        def nb_aretes(n) :
            if n == 2 :
                return 1
            else :
                return (n-1) + nb_aretes(n-1)
        ```

## ^^3. Représentation d'un graphe en Python^^

### 3.1. Représentation par une liste d'adjacences.

Prenons le cas de ce graphe non orienté :

![](data/mermaid-diagram-1.svg#only-light){ width=30%}
![](data/mermaid-diagram-1d.svg#only-dark){ width=30%}

On peut le représenter par le dictionnaire suivant, où chaque clé est un sommet et sa valeur est la liste des voisins de ce sommet  :

```python
graphe1 = { 'A' : ['E', 'C', 'D'],
            'C' : ['A', 'B', 'D'],
            'E' : ['A', 'B'],
            'D' : ['A', 'C', 'B'],
            'B' : ['E', 'C', 'D']
            }
```

Le principe est le même pour un graphe orienté :
```mermaid
flowchart LR
    A(A) --> B(B)
    B --> C(C)
    C --> D(D)
    C --> B
    D --> B
    D --> A
```

Ce graphe orienté peut être réprésenté par le dictionnaire suivant, où la valeur d'une clé est la liste de ses successeurs :

```python
graphe1 = { 'A' : ['B'],
            'B' : ['C'],
            'C' : ['B', 'D'],
            'D' : ['A', 'B'],
            }
```

!!! question "Identifier le dictionnaire représentant un graphe"

    On donne les trois dictionnaires suivants :
    ```python
    graph1 = {
    "A": ["B", "C", "D"],
    "B": ["A", "C", "D"],
    "C": ["A", "B", "D"],
    "D": ["A", "B", "C"],
    }

    graph2 = {
    "A": [],
    "B": ["A", "C"],
    "C": ["A"],
    "D": ["A", "B", "C"],
    }

    graph3 = {
    "A": ["B", "C", "D"],
    "B": ["D"],
    "C": ["B", "D"],
    "D": [],
    }
    ```
    {{ multi_qcm(
        [
            "Lequel représente correctement le graphe orienté ci-dessous ?",
            ["graph1", "graph2", "graph3"],[3],
        ],
        shuffle=False) }}

    ![](data/solution_exo_dictionnaire_2.png){: width=360px .center}

!!! question "Tracer un graphe à partir d'une liste d'adjacences"

    Tracer le graphe non orienté qui est représenté en Python par le dictionnaire suivant :

    ```python
    grapheExo = {
    "A": ["B", "C"],
    "B": ["A", "C", "D"],
    "C": ["A", "B", "D", "E", "F"],
    "D": ["B", "C", "E", "F"],
    "E": ["C", "D", "F"],
    "F": ["C", "D", "E"],
    }
    ```

    ??? danger "Solution"
        ![](data/solution_exo_dictionnaire_1.png){: width=360px .center}

### 3.2. Représentation par une matrice d'adjacence

#### 3.2.1. Notion de matrice d'adjacence

!!! tip "Définition d'une matrice"
    Une **matrice** est le nom mathématique donné à un tableau de nombres.
    Par exemple, ceci est une matrice :

    $$\begin{pmatrix}
    7 & 1 & 4  \\
    0 & 5  & 2  \\
    1 & 0  & 8  \\
    \end{pmatrix}$$


On prend l'exemple du graphe orienté suivant à 4 sommets :
<div class="centre">
```mermaid
graph LR
A(("A"))
B(("B"))
C(("C"))
D(("D"))
A --> B
B --> C
A --> C
C --> A
B --> D
C --> D
```
</div>
        
On va utiliser un tableau dans lequel les lignes et les colonnes représentent les sommets et dans lequel on indique par un **1** la présence d'un arc allant du sommet de la ligne vers celui de la colonne et par un **0** son absence. Compléter alors le tableau ci-dessous avec cette méthode.

![Matrice1](data/matrice1.png){: width=240px .imgcentre}

Si on numérote les sommets du graphe (numéro 0 pour A, numéro 1 pour B, numéro 2 pour C, etc.), il n'est plus nécessaire d'indiquer les noms des sommets sur les lignes et les colonnes. On peut donc représenter ce graphe par ce que l'on appelle sa **matrice d'adjacence**, que voici :

$$\begin{pmatrix}
    0 & 1 & 1 & 0 \\
    0 & 0  & 1 & 1 \\
    1 & 0  & 0 & 1 \\
    0 & 0  & 0 & 0 \\
    \end{pmatrix}$$


!!! question "Tracer un graphe à partir de sa matrice d'adjacence"
    En nommant les sommets $S_0, S_1, S_2, S_3$ et $S_4$, dessiner le graphe orienté dont la matrice d'adjacence est :

    $$\begin{pmatrix}
    0 & 0 & 1 & 1 & 0\\
    0 & 0 & 1 & 0 & 0\\
    1 & 0 & 0 & 1 & 0 \\
    0 & 1 & 0 & 0 & 1 \\
    0 & 1 & 0 & 0 & 0
    \end{pmatrix}$$

    ??? danger "Solution"
        On obtient le graphe orienté suivant :

        ```mermaid
        graph LR
        S0(("S0"))
        S1(("S1"))
        S2(("S2"))
        S3(("S3"))
        S4(("S4"))
        S0 --> S2
        S0 --> S3
        S1 --> S2
        S2 --> S0
        S2 --> S3
        S3 --> S1
        S3 --> S4
        S4 --> S1
        ```


<!--     
```mermaid
    graph LR
    S0(("S0"))
    S1(("S1"))
    S2(("S2"))
    S3(("S3"))
    S4(("S4"))
    S0 --- S1
    S1 --- S2
    S1 --- S3
    S2 --- S4
    S3 --- S4
``` 
-->


!!! question "Cas d'un graphe non orienté"
    Donner la matrice d'adjacence du graphe non orienté suivant.

    ![](data/mermaid-diagram-6.svg#only-light){ width=50%}
    ![](data/mermaid-diagram-6d.svg#only-dark){ width=50%}
    
    Quelle particularité a cette matrice ?

    ??? danger "Solution"
        La matrice est la suivante :

        $$\begin{pmatrix}
        0 & 1 & 0 & 0 & 0\\
        1 & 0 & 1 & 1 & 0\\
        0 & 1 & 0 & 0 & 1 \\
        0 & 1 & 0 & 0 & 1 \\
        0 & 0 & 1 & 1 & 0
        \end{pmatrix}$$

        On observe une symétrie par rapport à la diagonale de la matrice. Les mathématiciens parlent d'une ***matrice symétrique***.



<!-- 
```mermaid
    graph LR
        S0(("S0"))
        S1(("S1"))
        S2(("S2"))
        S3(("S3"))
        S4(("S4"))
        S5(("S5"))
        S0 ---|20| S1
        S0 ---|35| S3
        S1 ---|100| S2
        S3 ---|70| S4
        S4 ---|30| S2
        S1 ---|5| S3
        S3 ---|8| S5
```    
-->


!!! question "Cas d'un graphe pondéré" 
    Proposer une méthode pour représenter par une matrice d'adjacence un graphe pondéré, par exemple celui ci-dessous.

    ![](data/mermaid-diagram-7.svg#only-light){ width=60%}
    ![](data/mermaid-diagram-7d.svg#only-dark){ width=60%}
 

    ??? danger "Solution"
        Au lieu de mettre le nombre 1 quand il y a une arête, on met le "poids" (la _distance_, ou le _coût_) qui figure sur l'arête. On obtient ainsi la matrice symétrique suivante :

        $$\begin{pmatrix}
        0 & 20 & 0 & 35 & 0 & 0\\
        20 & 0 & 100 & 5 & 0 & 0\\
        0 & 100 & 0 & 0 & 30 & 0\\
        35 & 5 & 0 & 0 & 70 & 8\\
        0 & 0 & 30 & 70 & 0 & 0\\ 
        0 & 0 & 0 & 8 & 0 & 0
        \end{pmatrix}$$

#### 3.2.2. Implémentation en Python

Une matrice est codée en Python par un "tableau de tableaux". Le graphe de l'exemple d'introduction 
<div class="centre">
```mermaid
graph LR
A(("A"))
B(("B"))
C(("C"))
D(("D"))
A --> B
B --> C
A --> C
C --> A
B --> D
C --> D
```
</div>
sera donc représenté par 
```python
matrice1 = [ [0, 1, 1, 0],
             [0, 0, 1, 1],
             [1, 0, 0, 1],
             [0, 0, 0, 0]
            ]
```


### 3.3. Interface d'une structure de graphe.

L'interface minimale (les "manipulations élémentaires", _les primitives_) dont on veut disposer sur une structure de graphe est :

| Fonction | Description |
| :------ | :------- | 
| `liste_sommets(G)` | renvoyer la liste des sommets du graphe G |
| `liste_voisins(G,A)` | renvoyer la liste des voisins du sommet A dans le graphe G |
| `sont_voisins(G,A,B)` | renvoyer `True` si les sommets A et B du graphe G sont voisins `False` sinon |
| `ajouter_arete(G, A, B)` | ajouter une arête entre les sommets A et B du graphe G|
| `ajouter_sommet(G,S)` | ajouter le sommet S au graphe G |



## ^^4. Exercices^^

Télécharger le [carnet Jupyter](TP_Graphes.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.