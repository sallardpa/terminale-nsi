# Arbres binaires de recherche (ABR)

Dans un arbre, et en particulier dans un arbre binaire, on peut stocker tout type de données : des nombres (type `int` ou `float`), du texte (type `str`), etc.

Dans un premier temps, nous allons nous concentrer sur des arbres dont les nœuds portent des nombres, comme par exemple ceux-ci :

![](data/mermaid-diagram-1.svg#only-light){.center width=70%}
![](data/mermaid-diagram-1d.svg#only-dark){.center width=70%}


<!--
```mermaid
graph TD
    A(25) --- B(19)
    A --- C(37)
    B --- D(7)
    B --- E(24)
    C --- F(29)
    C --- G(52)

    M(19) --- N(7)
    M --- O(29)
    N --- P(3)
    N --- Q(25)
    O --- R(37)
    O --- S(52)
``` 
-->

## ^^1. Définition d'un arbre binaire de recherche^^



!!! tip "Définition d'un ABR :heart:"
    Un **arbre binaire de recherche** (ABR) est :
    
    * un arbre binaire (tous ses nœuds ont ***au maximum deux fils*** : un fils gauche et un fils droit),
    * dans lequel la valeur de n'importe quel nœud vérifie les deux conditions suivantes :

        - la valeur du nœud est **supérieure** à _toutes_ les valeurs de son **fils gauche**
        - la valeur du nœud est **inférieure** à _toutes_ les valeurs de son **fils droit**

    Parmi les deux arbres représentés dans l'introduction, seul le premier (celui avec 25 à sa racine) est un ABR. Le second (celui avec 19 à sa racine) n'est pas un ABR car 19 n'est pas supérieur à la valeur 25 qui se trouve dans le sous-arbre gauche.

    On verra plus bas le ***lien*** entre les conditions sur les valeurs des nœuds et le qualificatif "de recherche" de ces arbres binaires.

> Remarque : un ABR n'est pas forcément un arbre binaire avec tous ses niveaux complets ("arbre binaire parfait"), il peut très bien avoir des sous-arbres de hauteurs différentes.

<!--     
```mermaid
    graph TD
        NN(50) --- MM(10)
        NN --- PP(20)
        NN --- QQ(70)
        QQ --- RR(65)
        QQ --- TT(75)

        A(3) --- B(2)
        A --- C(10)
        C --- F(8)
        C --- G(33)
        G --- H(25)
        G --- J(40)

        M(10) --- N(3)
        M --- O(25)
        N --- P(2)
        N --- Q(8)
        O --- R(33)
        O --- S(52)

        T(25) --- U(10)
        T --- V(33)
        U --- W(3)
        U --- X(8)
``` 
--> 

!!! question "Exercice : identifier un arbre binaire de recherche"

    Voici quatre arbres binaires :

    ![](data/mermaid-diagram-2.svg#only-light){.center width=90%}
    ![](data/mermaid-diagram-2d.svg#only-dark){.center width=90%}

    {{ multi_qcm(
        [
            "Parmi ces arbres, un seul est un ABR. Donner la valeur de sa racine.",
            ["50", "3", "10", "25"],[2],
        ],
        shuffle = False) }}

    ??? warning "Explications"
        Le premier arbre (celui avec 50 à sa racine) n'est même pas un arbre binaire : la racine a 3 fils.

        L'arbre ayant 10 pour racine a un sous-arbre droit qui ne respecte pas la condition : 25 n'est pas supérieur à 33.

        Dans l'arbre ayant 25 pour racine, c'est le sous-arbre gauche qui ne respecte pas la condition : 10 n'est pas inféieur à 8.

!!! question "Exercice : construire un ABR"

    On veut stocker les valeurs suivantes dans un ABR : 1,2,5,10,20,50,100. 

    Proposer un ABR valable, d'une hauteur inférieure ou égale à 3 (_plusieurs solutions sont possibles_).

    ??? warning "Solution"
        Il n'y a pas une solution unique. Voici deux possibilités :
        
        ![](data/mermaid-diagram-3.svg#only-light){.center width=70%}
        ![](data/mermaid-diagram-3d.svg#only-dark){.center width=70%}

        Tous les ABR valables ont forcément la même taille (taille $t= 7$ car il y a 7 valeurs à stocker) mais on peut viser de construire un ABR de hauteur minimale : ici $h_{min}=2$. Revoir si besoin la formule $t \leqslant 2^{h+1} -1$ du cours sur les arbres binaires. 


<!-- 
```mermaid
        graph TD 
            classDef Nil stroke:none,fill:none,color:none 
            A(10) --- B(2)
            A --- C(50)
            B --- D(1)
            B --- E(5)
            C --- F(20)
            C --- G(100)


            M(50) --- N(5)
            M --- O(100)
            N --- P(2)
            N --- Q(10)
            P --- R(1)
            P --- V2( ):::Nil
            Q --- V3( ):::Nil
            Q --- S(20)
``` 
-->

## ^^2. Insérer une valeur dans un ABR^^

Traitons un exemple : un arbre binaire de recherche (ABR) de départ est donné, comme celui-ci rencontré dans l'exercice précédent :

![](data/mermaid-diagram-4.svg#only-light){.center width=30%}
![](data/mermaid-diagram-4d.svg#only-dark){.center width=30%}

<!-- ```mermaid
graph TD  
    A(10) --- B(2)
    A --- C(50)
    B --- D(1)
    B --- E(5)
    C --- F(20)
    C --- G(100)
``` -->

On demande de **créer une nouvelle feuille et d'y insérer** la valeur 40 dans cet ABR de telle sorte que **l'arbre final reste un ABR**. 

On se rend compte qu'il n'y a qu'une seule possibilité : 

![](data/mermaid-diagram-5.svg#only-light){.center width=30%}
![](data/mermaid-diagram-5d.svg#only-dark){.center width=30%}

<!-- 
```mermaid
graph TD  
    A(10) --- B(2)
    A --- C(50)
    B --- D(1)
    B --- E(5)
    C --- F(20)
    C --- G(100)
    F --- V( )
    F --- H(40)
``` 
-->

En effet, 40>10 donc il faut partir vers le fils droit de la racine. Puis 40<50 donc il faut aller vers le fils gauche du nœud 50. Et ainsi de suite.

<!-- ```mermaid
    graph TD
        classDef Nil stroke:none,fill:none,color:none  
        M(50) --- N(5)
        M --- O(100)
        N --- P(2)
        N --- Q(10)
        P --- R(1)
        P --- V2( ):::Nil
        Q --- V3( ):::Nil
        Q --- S(20)
``` -->

!!! question "Exercice : une autre insertion"
    On reprend l'autre ABR rencontré dans l'exercice précédent :

    ![](data/mermaid-diagram-6.svg#only-light){.center width=30%}
    ![](data/mermaid-diagram-6d.svg#only-dark){.center width=30%}


    Et là-encore, on demande d'insérer la valeur 40 pour que l'arbre reste bien un ABR. Donner la seule solution possible.

    ??? warning "Solution"
        La seule solution possible est

        ![](data/mermaid-diagram-7.svg#only-light){.center width=30%}
        ![](data/mermaid-diagram-7d.svg#only-dark){.center width=30%}
        
        
<!-- ```mermaid
        graph TD  
            classDef Nil stroke:none,fill:none,color:none
            M(50) --- N(5)
            M --- O(100)
            N --- P(2)
            N --- Q(10)
            P --- R(1)
            P --- V2( ):::Nil
            Q --- V3( ):::Nil
            Q --- S(20)
            S --- V4( ):::Nil
            S --- T(40)
``` -->

Pour la mise en œuvre pratique de l'algorithme d'insertion d'une valeur dans un ABR : voir le chapitre dédié.

## ^^3. Rechercher une valeur dans un ABR^^

Traitons encore un exemple. Considérons un arbre binaire de recherche (ABR) comme ci-dessous :

![](data/abr_search.png)

- sa racine porte la valeur 1234
- ses deux sous-arbres gauche et droit ont chacun une taille égale à 500.

La taille de cet arbre est donc 1+500+500 = 1001.


Si on veut **rechercher la valeur** 88 dans cet arbre, de quel côté allons-nous chercher ? Évidemment du côté gauche, puisque 88 < 1234. On se rend ainsi compte que, au lieu de rechercher la valeur 88 parmi les mille nœuds de l'ABR, on peut se contenter de la rechercher parmi les 500 nœuds du sous-arbre gauche : la charge de travail est donc **divisée par deux**.

La recherche d'une valeur dans un ABR va donc se faire selon le principe ***"diviser pour régner"***, qui est un gage d'**efficacité**.

> Remarque : il y a des similitudes avec la **recherche par dichotomie** d'une valeur dans un tableau trié.

Cette efficacité explique pourquoi les ABR sont des outils usuels en informatique. Par ricochet, cela explique aussi le nom de "_arbre binaire de_ ***recherche***" : ce sont des arbres binaires particulièrement bien adaptés à la recherche de valeurs dans leurs nœuds.

Nous étudierons plus précisément dans le chapitre dédié cet algorithme de recherche d'une valeur dans un ABR et nous évaluerons sa complexité algorithmique.


## ^^4. Récupération des valeurs d'un ABR sous forme d'un tableau trié^^

Voici un ABR : 

![](data/mermaid-diagram-8.svg#only-light){.center width=40%}
![](data/mermaid-diagram-8d.svg#only-dark){.center width=40%}

<!--
```mermaid
    graph TD 
        classDef Nil stroke:none,fill:none,color:none 
        M(50) --- N(5)
        M --- O(100)
        N --- P(2)
        N --- Q(10)
        P --- R(1)
        P --- V2( ):::Nil
        Q --- V3( ):::Nil
        Q --- S(20)
        S --- V4( ):::Nil
        S --- T(40)
        O --- A(60)
        O --- B(110)
```
-->

Quel type de parcours va permettre d'afficher les valeurs de cet arbre dans l'ordre croissant : un parcours en largeur, un parcours (en profondeur) préfixe, infixe ou postfixe ?

??? warning "Solution"
    On se rend compte que le parcours infixe permet d'obtenir l'affichage des valeurs dans l'ordre croissant.

## ^^5. Extension du champ d'application des ABR^^

En introduction, il a été mentionné que l'on se concentre au départ sur les arbres remplis de nombres. 

On peut élargir le champ d'application des arbres binaires de recherche (ABR) au d'autres types de données, comme à des mots ou des textes (type `str`) : il suffit de définir un ***ordre de classement*** et, dans le cas de mots, on pense naturellement à l'ordre alphabétique (_ordre lexicographique_).

Voici par exemple les jours de la semaine stockés dans un ABR :

![](data/mermaid-diagram-9.svg#only-light){.center width=50%}
![](data/mermaid-diagram-9d.svg#only-dark){.center width=50%}


<!-- 
```mermaid
    graph TD  
        M(Mardi) --- N(Jeudi)
        M --- O(Samedi)
        N --- P(Dimanche)
        N --- Q(Lundi)
        O --- V(Mercredi )
        O --- S(Vendredi)
``` -->


## ^^6. Exercices et TP sur les ABR^^


* Généralités : 

    - Exercices sur feuille : télécharger le [document](Exos_ABR.pdf).

    - Exercices Python : télécharger le [carnet Jupyter](TP_Exos_ABR.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac n°1 : [énoncé](Structures_ABR_SujetBac_23NSI_0B.pdf) à faire sur feuille.

* Exercice type Bac n°2 : [énoncé](Structures_ABR_SujetBac_23NSIJ2PO1.pdf) à faire sur feuille.