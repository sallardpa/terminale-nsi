# Arbres binaires

## ^^1. Première définition d'un arbre binaire^^


!!! tip "Définition usuelle d'un arbre binaire :heart:"
    Un **arbre binaire** est un arbre dont les nœuds ont ***au maximum deux fils***.


!!! question "Identifier un arbre binaire"

    Voici trois arbres :

    ![](data/mermaid-diagram-1.svg#only-light){.center width=60%}
    ![](data/mermaid-diagram-1d.svg#only-dark){.center width=60%}

    {{ multi_qcm(
        [
            "Parmi ces arbres, un seul n'est pas un arbre binaire. Donner la lettre de sa racine.",
            ["A", "G", "P"],[3],
        ],
        shuffle = False) }}

<!--     ```mermaid
    graph TD
        A(A) --- B(B)
        A --- C(C)
        B --- D(D)
        B --- E(E)
        C --- F(F)


        G(G) --- H(H)
        H --- J(J)
        J --- K(K)
        J --- L(L)

        P(P) --- Q(Q)
        P --- R(R)
        P --- S(S)
        R --- T(T)
        R --- U(U)
    ``` -->



!!! tip "Fils droit, fils gauche dans un arbre binaire"

    Pour simplifier l'implémentation (_le codage informatique_) des arbres binaires, on va considérer dorénavant que chaque nœud d'un arbre binaire a ***exactement deux fils : un fils gauche et un fils droit***, mais que ces fils gauche ou droit peuvent être vides ! 

    On aura alors des représentations graphiques de forme "pyramidale", et on s'obligera (au moins dans un premier temps) à représenter les fils vides comme dans l'exemple ci-dessous :

    ![](data/mermaid-diagram-2.svg#only-light){.center width=30%}
    ![](data/mermaid-diagram-2d.svg#only-dark){.center width=30%}
<!--     ```mermaid
    graph TD
        classDef Nil stroke:none,fill:none,color:none  
        A(A) --- B(B)
        A --- D(D)
        B --- V( ):::Nil
        B --- C(C)
        C --- F( ):::Nil
        C --- G( ):::Nil
        D --- V1( ):::Nil
        D --- V2( ):::Nil
    ``` -->


!!! warning "Bien distinguer fils gauche et fils droit"

    Avec cette notion de "fils gauche / fils droit", la représentation de l'arbre binaire ci-dessous est ambigüe :

    ![](data/mermaid-diagram-3.svg#only-light){.center width=20%}
    ![](data/mermaid-diagram-3d.svg#only-dark){.center width=20%}


    On est obligé de préciser si le nœud F est le fils gauche ou le fils droit du nœud C. Il y a donc deux arbres binaires possibles :
    ![](data/mermaid-diagram-4.svg#only-light){.center width=90%}
    ![](data/mermaid-diagram-4d.svg#only-dark){.center width=90%}
<!-- ```mermaid
    graph TD
        A(A) --- B(B)
        A --- C(C)
        B --- D(D)
        B --- E(E)
        C --- F(F)
    ```
    ```mermaid
    graph TD
        classDef Nil stroke:none,fill:none,color:none  
        A(A) --- B(B)
        A --- C(C)
        B --- D(D)
        B --- E(E)
        C --- F(F)
        C --- G( ):::Nil
        D --- V1( ):::Nil
        D --- V2( ):::Nil
        E --- V3( ):::Nil
        E --- V4( ):::Nil
        F --- V5( ):::Nil
        F --- V6( ):::Nil
        rq[[ou bien]]
        A1(A) --- B1(B)
        A1 --- C1(C)
        B1 --- D1(D)
        B1 --- E1(E)
        C1 --- F1( ):::Nil
        C1 --- G1(F)
        D1 --- w1( ):::Nil
        D1 --- w2( ):::Nil
        E1 --- w3( ):::Nil
        E1 --- w4( ):::Nil
        G1 --- w5( ):::Nil
        G1 --- w6( ):::Nil
    ``` -->
    


!!! question "Exercice : dessiner des arbres binaires"

    Dessiner tous les arbres binaires de hauteur 2, et préciser la taille de chacun.

    _Indication_ : vous devez trouver en tout 21 arbres binaires différents !

    ??? danger "Solution"
        ![](data/arbre_bin_hauteur2.jpg){: .center width=80%}



!!! question "Exercice : taille et hauteur d'un arbre binaire"

    1. Dans un arbre binaire de hauteur $h$ très grande (par exemple $h=10$), combien y a-t-il au maximum de nœuds (non vides)  de profondeur $p=2$ ? 

    2. Même question avec $p=3$, puis $p=4$, puis généraliser pour un entier $p \leqslant h$ quelconque.

    3. En admettant la formule (_au programme de maths de Première_) : $1+2+2^2+2^3 + \cdots + 2^p = 2^{p+1} -1$, justifier qu'un arbre binaire de hauteur $h$ a une taille $t$ qui vérifie : $t \leqslant 2^{h+1} -1$. 

    4. Un arbre binaire a une hauteur $h =10$. Combien comporte-t-il de nœuds (non vides) au maximum ?

    5. Un arbre binaire a une taille $t = 15$. Justifier que sa hauteur $h$ est au minimum égale à 3 et au maximum égale à 14, c'est-à-dire que $3 \leqslant h \leqslant 14$. 

## ^^2. Deuxième définition d'un arbre binaire^^

Grâce à cette distinction entre fils gauche et fils droit, on peut donner une autre définition d'un arbre binaire :

!!! tip "Définition récursive d'un arbre binaire :heart:"
    Un **arbre binaire** est :
    
    - soit un arbre vide, 
    - soit un nœud (racine) ayant lui-même deux sous-arbres binaires (un gauche et un droit).

    De la même façon que, dans un chapitre précédent, une liste chainée était "*tenue* " par son premier maillon, on perçoit ici qu'un arbre binaire est "*tenu*" par sa racine.

!!! question "Exercice : utiliser la définition récursive d'un arbre binaire"

    On donne la construction suivante :

    - A~1~ est l'arbre dont le nœud a pour étiquette "Alice" et dont les deux sous-arbres sont vides ;
    - A~2~ est l'arbre dont le nœud a pour étiquette "Bob" et dont les deux sous-arbres sont vides ;
    - A~3~ est l'arbre dont le nœud a pour étiquette "Gaston", dont le sous-arbre gauche vaut A~1~ et le sous-arbre droit est vide ;
    - A~4~ est l'arbre dont le nœud a pour étiquette "Jeanne", dont le sous-arbre gauche vaut A~2~ et le sous-arbre droit vaut A~3~.

    Dessiner l'arbre binaire A~4~.

    ??? danger "Solution"

        La représentation de l'arbre binaire A~4~ est :
        ![](data/mermaid-diagram-5.svg#only-light){.center width=30%}
        ![](data/mermaid-diagram-5d.svg#only-dark){.center width=30%}
<!--         ```mermaid
        graph TD
            classDef Nil stroke:none,fill:none,color:none  
            A(Jeanne) --- B(Bob)
            A --- C(Gaston)
            B --- D( ):::Nil
            B --- E( ):::Nil
            C --- F(Alice)
            C --- G( ):::Nil
            F --- V5( ):::Nil
            F --- V6( ):::Nil
        ``` -->
   


## ^^3. Interface d'un arbre binaire^^

L'**interface** minimale d'un arbre binaire comporte les opérations (_les primitives_) suivantes :

| Fonction | Description |
| :------ | :------- | 
| `valeur(noeud)` | renvoie l'étiquette du nœud|
| `sous_arbre_gauche(noeud)` | renvoie le sous-arbre gauche du nœud  |
| `sous_arbre_droit(noeud)` | renvoie le sous-arbre droit du nœud  |
| `est_vide(noeud)` | renvoie `True` si le nœud est vide et `False` sinon  |


## ^^4. Implémentation d'un arbre binaire : version procédurale, à l'aide de tuples^^

On peut choisir de représenter un nœud d'un arbre binaire par un **triplet** (type `tuple`) contenant :

- l'étiquette (ou la valeur) du nœud ;
- le sous-arbre gauche (qui est lui-même un triplet, éventuellement vide) ;
- le sous-arbre droit (qui est lui aussi un triplet, éventuellement vide).

Un nœud sera alors défini sous la forme : `monNoeud = (valeur, SAG, SAD)` , où `SAG` est le triplet qui représente le sous-arbre gauche et `SAG` le sous-arbre droit.

L'arbre binaire est alors représenté par son nœud racine : souvenez-vous de l'idée "**on tient l'arbre binaire grâce à sa racine" !**

!!! done "Une implémentation d'un arbre binaire avec des tuples"
    On veut représenter l'arbre binaire de l'exercice précédent :

    ![](data/mermaid-diagram-5.svg#only-light){.center width=30%}
    ![](data/mermaid-diagram-5d.svg#only-dark){.center width=30%}
    
    On écrit alors le code suivant :
    ```python
    noeudA = ("Alice", (), () )
    noeudB = ("Bob", (), () )
    noeudG = ("Gaston", noeudA, () )
    noeudJ = ("Jeanne", noeudB, noeudG)
    monArbreBinaire = noeudJ
    ```

<!--     ```mermaid
    graph TD
        classDef Nil stroke:none,fill:none,color:none  
        A(Jeanne) --- B(Bob)
        A --- C(Gaston)
        B --- D( ):::Nil
        B --- E( ):::Nil
        C --- F(Alice)
        C --- G( ):::Nil
        F --- V5( ):::Nil
        F --- V6( ):::Nil
    ``` -->


??? question "Exercice : utiliser l'implémentation à l'aide de tuples"

    Écrire le code pour représenter à l'aide de tuples l'arbre binaire ci-dessous :

    ![](data/mermaid-diagram-6.svg#only-light){.center width=30%}
    ![](data/mermaid-diagram-6d.svg#only-dark){.center width=30%}

    ??? danger "Solution"

        Il faut écrire le code suivant :
        ```python
        K = ("Karl", (), () )
        A = ("Amina", (), () )
        S = ("Sophie", (), K )
        D = ("Dorothée", S, A)
        monArbreBinaire = D # cette dernière ligne est superflue
        ```

        Si on n'a pas peur de se tromper dans les parenthèses, on peut condenser tout ceci en une seule ligne :
        ```python
        monArbreBinaire = ("David", ("Sophie", (), ("Kevin", (), () ) ), ("Amina", (), () )) 
        ```
<!-- 
        ```mermaid
    graph TD
        classDef Nil stroke:none,fill:none,color:none  
        A(Dorothée) --- B(Sophie)
        A --- C(Amina)
        B --- D( ):::Nil
        B --- E(Karl)
        C --- F( ):::Nil
        C --- G( ):::Nil
        E --- V5( ):::Nil
        E --- V6( ):::Nil
    ``` -->

Les **opérations de l'interface** sont alors codées ainsi :

```python 
def valeur(noeud) :
    return noeud[0]

def sous_arbre_gauche(noeud) :
    return noeud[1]

def sous_arbre_droit(noeud) :
    return noeud[2]

def est_vide(noeud) :
    return len(noeud) == 0
```

!!! question "Exploration d'un arbre binaire (1)"
    En appuyant sur le bouton "Lancer", vous allez exécuter un code masqué qui crée un arbre binaire dénommé `monArbre`. 

    En utilisant uniquement les opérations de l'interface `valeur`, `sous_arbre_gauche` et `sous_arbre_droit`, explorer cet arbre puis le dessiner.

    {{ IDE("data/arbBinaire_tuple_handball") }}


    ??? danger "Solution"

        On voit facilement que la racine est `Danemark`. Puis le plus simple est de commencer par définir `sag = sous_arbre_gauche(monArbre)` et `sad = sous_arbre_droit(monArbre)` et de récupérer leurs étiquettes avec `valeur(sag)` et `valeur(sad)`. Puis on continue en explorant le sous-arbre gauche : `sag_2 = sous_arbre_gauche(sag)` et `sad_2 = sous_arbre_droit(sag)`. Et ainsi de suite.

        Au final, l'arbre mystère représente la [phase finale du Mondial de Handball masculin de 2023](https://fr.wikipedia.org/wiki/Championnat_du_monde_masculin_de_handball_2023#Phase_finale){target="_blank"}

## ^^5. Implémentation d'un arbre binaire : version Orientée Objet^^

Une autre façon d'implémenter un arbre binaire est de créer un objet "Noeud", c'est-à-dire en Python de créer une **classe** `Noeud` qui a trois **attributs** :

* la donnée à stocker (_l'étiquette du noeud_),
* le sous-arbre gauche noté `left`, qui est lui-même un objet de la classe `Noeud` (éventuellement vide) ;
* le sous-arbre droit noté `right`, qui est lui aussi un objet de la classe `Noeud` (éventuellement vide).

Un arbre vide est représenté par la valeur `None`. 

```python
class Noeud:

    def __init__(self,val = None, sag = None, sad = None) :
        self.valeur = val
        self.left = sag
        self.right = sad
```

> On a mis `None` en valeur par défaut à tous les attributs de cet objet pour simplifier la création (l'instanciation) des nœuds.

On peut noter que cette classe `Noeud` n'a pas de _méthodes_ dédiées. Les actions de l'interface (`valeur(noeud)`, `sous_arbre_gauche(noeud)`, `sous_arbre_droit(noeud)` ) n'ont pas besoin d'être codées puisqu'elles correspondent aux attributs de l'objet `Noeud` et qu'on peut y accéder directement.

!!! done "Une implémentation d'un arbre binaire avec un objet `Noeud`"
    On reprend l'arbre binaire de l'exemple précédent : 

    ![](data/mermaid-diagram-5.svg#only-light){.center width=30%}
    ![](data/mermaid-diagram-5d.svg#only-dark){.center width=30%}

    En version Orientée Objet, cet arbre est alors codé ainsi :
    ```python
    A = Noeud("Alice") # par défaut, les sous-arbres gauche et droit valent None
    B = Noeud("Bob")
    G = Noeud("Gaston", A, None )
    J = Noeud("Jeanne", B, G)
    monArbreBinaire = J # on aurait pu écrire directement monArbreBinaire = Noeud("Jeanne", B, G) 
    ```

    Pour créer une variable `var` égale à la valeur stockée à droite de la racine, on écrira :
    ```python
    var = monArbreBinaire.right.valeur
    ```


!!! question "Exploration d'un arbre binaire (2)"
    En appuyant sur le bouton "Lancer", vous allez exécuter un code masqué qui crée (_qui instancie_) un objet `monArbreMystere` de la classe `Noeud`. 

    En vous servant des attributs de cet objet, explorer cet arbre puis le dessiner.

    {{ IDE("data/arbBinaire_POO_atome") }}

    ??? danger "Solution"
        L'arbre mystère est l'arbre représenté par

        ![](data/mermaid-diagram-7.svg#only-light){.center width=30%}
        ![](data/mermaid-diagram-7d.svg#only-dark){.center width=30%} 

        On l'obtient en saisissant `monArbreMystere.valeur`, puis `monArbreMystere.left.valeur`, etc.
<!-- 
        ```mermaid
        graph TD
            A(Atome) --- E(Electron)
            A --- Y(Noyau)
            Y --- P(Proton)
            Y --- N(Neutron)
        ``` -->


## ^^6. Algorithmes de calcul de la taille et de la hauteur d'un arbre binaire :heart:^^

* Supposez que l'on vous demande la taille d'un arbre binaire qui a un sous-arbre gauche de taille 5 et un sous-arbre droit de taille 12.
```mermaid
graph TD
    A(Racine) --- E[[Sous-arbre gauche de taille 5]]
    A --- Y[[Sous-arbre droit de taille 12]]
```
Vous répondrez sans hésitation que la taille totale de l'arbre est **1+5+12** = 18. 

Si on généralise, on comprend qu'on a une relation de la forme : 

!!! check ""
    Taille(arbreBinaire) = 1 + Taille(sous-arbre gauche) + Taille(sous-arbre droit)

Cette relation permet de définir un **algorithme récursif** de calcul de la taille d'un arbre binaire :

    FONCTION Taille(arbreBinaire):

        SI est_vide(arbreBinaire) ALORS
            RENVOYER 0 # cas de base
        SINON
            RENVOYER 1 + Taille(sous_arbre_gauche(arbreBinaire)) + Taille(sous_arbre_droit(arbreBinaire))
        FIN_SI



* Supposez maintenant que l'on vous demande la hauteur d'un arbre binaire qui a un sous-arbre gauche de hauteur 8 et un sous-arbre droit de hauteur 5.

![](data/hauteurArbre.drawio.png){: .center width 60%}

Puisque le sous-arbre gauche est ici plus haut, c'est lui qui l'emporte et la hauteur total de l'arbre vaut alors 1+8 = 9.  

Mais on peut aussi avoir une situation où c'est le sous-arbre droit qui est plus haut. On comprend alors qu'une formule générale pour la hauteur d'un arbre est de la forme :

!!! check ""
    Hauteur(arbre) = 1 + MAX ( Hauteur(sous-arbre gauche), Hauteur(sous-arbre droit) ) 

Là encore, cette relation permet de définir un **algorithme récursif** de calcul de la hauteur d'un arbre binaire :

    FONCTION Hauteur(arbreBinaire):

        SI est_vide(arbreBinaire) ALORS
            RENVOYER -1 # cas de base
        SINON
            RENVOYER 1 + MAX ( Hauteur(sous_arbre_gauche(arbreBinaire)) ; Hauteur(sous_arbre_droit(arbreBinaire)) )
        FIN_SI

_Remarque_ : pour le cas de base (arbre vide), on renvoie la valeur -1 car la formule générale permet alors d'avoir une hauteur égale à 1+ (-1) = 0 pour un arbre réduit à un seul nœud (racine). Cette valeur du cas de base est à adapter selon la définition de la hauteur que l'on vous donne en exercice.



## ^^7. Exercices et TP sur les arbres binaires^^

* Généralités : télécharger le [carnet Jupyter](TP_Exos_ArbresBinaires.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac n°1 :

    - [énoncé](Structures_Arbres_SujetBac_PO1.pdf), à faire sur feuille
    - codage et visualisation de la dernière partie avec un [carnet Jupyter](Structures_Arbres_SujetBac_PO1.ipynb).

* Exercice type Bac n° 2 :

    - [énoncé](Structures_Arbres_SujetBac_22NSIJ2AN1.pdf), à faire d'abord sur feuille
    - codage (_et indication pour la dernière question_) avec un [carnet Jupyter](Structures_Arbres_SujetBac_22NSIJ2AN1.ipynb).
    
* Exercice type Bac n° 3 :
    - [énoncé](ExoArbrePenche.pdf), à faire d'abord sur feuille
    - codage avec un [carnet Jupyter](ExoArbresPenches.ipynb).
