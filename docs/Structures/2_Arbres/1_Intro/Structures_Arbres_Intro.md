# Généralités sur les arbres


En informatique, les **arbres** sont des **structures de données**. Contrairement à la file et à la pile, cette structure n'est pas linéaire mais **hiérarchique**.

Ces structures sont très présentes en informatique car 

- elles permettent de représenter une organisation hiérarchique ;
- elles permettent de stocker des données volumineuses avec un accès rapide.

## ^^1. Quelques exemples de représentation de données à l'aide d'arbres^^

* arborescence de répertoires et fichiers d'un ordinateur :

![](data/arborescence-fichiers.jpg){: .center width=60%}

* arbres généalogiques (_dans l'illustration ci-dessous, il y a plusieurs arbres, on parle alors d'une forêt..._ ) :

![](data/game-of-thrones-family-tree.png){: .center width=60%}

* arbre de classification des espèces :

![](data/arbre_espces.jpg){: .center width=60%}

* arbre des matchs d'un tournoi :

![](data/wimbledon.png){: .center width=60%}

* arbre de décision :

```mermaid
graph TD
    A("Veux-tu manger un donut  ?") --> |Oui| B("As-tu vraiment faim ?")
    A --> |Non| C("Veux-tu une pomme ?")
    B --> |Oui| D("Voilà ton donut")
    B -->|Non| E("Est-ce un donut appétissant ?")
    E -->|Oui| F("Savoure ton donut, mais ne dis rien à ta mère")
    E -->|Non| G("Va faire du sport")
```

* arbre DOM (_Document Object Model_) d'un fichier HTML : quand il reçoit un fichier HTML, votre navigateur crée un arbre DOM de la page Web en question.

Par exemple, avec le fichier ci-dessous
```html
<html>
    <head>
        <title> My Title </title>
    </head>
    <body>
        <a href="My Link"></a>
        <h1> My header </h1>
    </body>
</html>
```
le navigateur va créer l'arbre suivant :

![](data/arbre_dom.png){: .center width=60%}

!!! exercice "Dessiner l'arbre de votre choix"

    [Cliquer ici](https://mermaid.live){target="_blank"} pour dessiner l'arbre de votre choix. C'est assez intuitif et si vous avez besoin d'aide, cliquer sur ***Documentation*** .

##  ^^2. Vocabulaire des arbres^^

Il existe un vocabulaire spécifique quand on parle d'**arbre** en informatique.

* Un arbre est composé de **nœuds** (:flag_gb: ***node***), qui sont soit la **racine** de l'arbre, un nœud interne ou une **feuille**. Les nœuds sont reliés par des **arcs** ou **arêtes**. Un sous-arbre est une partie d'un arbre qui peut être vue comme un arbre.

![](data/vocab1.png){ width="50%" .center}

* La valeur portée par un nœud s'appelle l'**étiquette** du nœud. Ce peut être un nombre, une chaine de caractères, un p-uplet, etc.

* Quand on veut insister sur le lien de parenté entre différents nœuds, on parle de nœud parent (ou père) et de nœud enfant (ou fils).

![](data/vocab2.png){ width="30%" .center}


<!--
```mermaid
graph TD
    A(A) --- B(B)
    A --- C(C)
    B --- D(D)
    B --- E(E)
    C --- F(F)
     C --- G(G)
```
```mermaid
graph TD
    A(A) --- B(B)
    A --- C(C)
    B --- D(D)
    B --- E(E)
    B --- F(F)
    A --- H(H)
    H --- J(J)
    H --- K(K)
``` 
-->


!!! faq "QCM"

    **Question 1** : 
    {{ multi_qcm(
        [
            "Comment appelle-t-on un nœud d'un arbre qui n'a pas de fils ?",
            ["une feuille", "une racine", "un arc", "un nœud interne"],[1],
        ],
        [
            "Comment appelle-t-on un nœud d'un arbre qui n'a pas de père ?",
            ["une feuille", "une racine", "un arc", "un nœud interne"],[2],
        ],
        [
            "Comment appelle-t-on un nœud d'un arbre qui a à la fois un père et au moins un fils ?",
            ["une feuille", "une racine", "un arc", "un nœud interne"],[4],
        ],
        [
            """
            Quels sont les nœuds qui font partie du sous-arbre de gauche de la racine de cet arbre ?

            ![](data/mermaid-diagram-1.svg#only-light){.center width=30%}
            ![](data/mermaid-diagram-1d.svg#only-dark){.center width=30%} 

            """,
            ["B, D, E", "D", "C, F, G", "F"],[1],
        ],
        [
            "Avec le même arbre que ci-dessus, on s'intéresse au sous-arbre de droite de la racine. Quelle est la racine de ce sous-arbre ?",
            ["A", "C", "B", "G"],[2],
        ],
        [
            """
            Dans l'arbre ci-dessous, combien de fils a le nœud B ?

            ![](data/mermaid-diagram-2.svg#only-light){.center width=50%}
            ![](data/mermaid-diagram-2d.svg#only-dark){.center width=50%} 

            """,
            ["0", "1", "2", "3"],[4],
        ],
        shuffle = False
    ) }}


??? info "Quelques précisions"
    
    * Par habitude, on représente toujours l'arbre de haut en bas, avec sa racine tout en haut.

    * De la même façon qu'on a besoin du nombre 0 quand on veut travailler avec les nombres, on a besoin d'imaginer un _arbre vide_ quand on veut travailler avec les arbres : c'est un "arbre" qui n'a rien, ni racine, ni feuille !

    * Tous les arbres que nous rencontrerons en NSI ont une racine (*sauf un arbre vide*) : il s'agit donc d'**arbres enracinés**. 

    Dans vos études post-Bac, vous rencontrerez des arbres qui, en apparence, n'ont pas de racine. Le schéma ci-dessous en est un exemple :

    ![](data/mermaid-diagram-3.svg#only-light){.center width=30%}
    ![](data/mermaid-diagram-3d.svg#only-dark){.center width=30%}
    
<!--     ```mermaid
    graph TD
        A(A) --- B(B)
        C(C) --- D(D)
        A --- D
        C --- E(E)
        D --- F(F)
        D --- G(G)
    ``` -->

??? warning "Arbre ou Pas Arbre ?"

    Dans un arbre, il ne doit pas y avoir deux chemins différents pour aller d'un nœud A à un nœud B.


    Par exemple, le schéma ci-dessous n'est pas un arbre :

    ![](data/mermaid-diagram-4.svg#only-light){.center width=30%}
    ![](data/mermaid-diagram-4d.svg#only-dark){.center width=30%}

<!--     ```mermaid
    graph TD
        A(A) --- G(G)
        A --- D(D)
        G --- C(C)
        C --- E(E)
        D --- F(B)
        D --- K(K)
        C --- F
    ``` -->

## ^^3. Quelques caractéristiques d'un arbre^^

La **taille** d'un arbre est le nombre total de nœuds de l'arbre, y compris sa racine.

Par exemple, la taille de l'arbre ci-dessous vaut 10.

![](data/mermaid-diagram-5.svg#only-light){.center width=40%}
![](data/mermaid-diagram-5d.svg#only-dark){.center width=40%}
<!-- 
```mermaid
graph TD
        A(A) --- B(B)
        A --- C(C)
        B --- D(D)
        B --- E(E)
        B --- F(F)
        A --- H(H)
        H --- J(J)
        H --- K(K)
        E --- M(M)
```
-->

La **profondeur** d'un nœud correspond au nombre d'arcs qu'il faut emprunter pour rejoindre la racine : par exemple, dans l'arbre ci-dessus, la profondeur du nœud E vaut 2. On se rend compte que, si on voit l'arbre comme un _immeuble inversé_, la profondeur d'un nœud correspond à son _étage_ (avec la racine qui est au rez-de chaussée)


La **hauteur** de l'arbre correspond à la plus grande profondeur d'une feuille : par exemple, toujours avec l'abre ci-dessus, sa hauteur vaut 3 (c'est la profondeur de de la feuille M). En gardant la comparaison avec un immeuble inversé, la hauteur de l'arbre correspond au nombre d'étages de l'immeuble.



??? Warning "Hauteur d'un arbre réduit à sa racine, hauteur d'un arbre vide"

    * L'arbre ci-dessous est composé d'un seul nœud (qui est donc sa racine), sa taille vaut 1 et sa hauteur vaut 0.
    
    ```mermaid
    graph TD
        A(Racine)
    ```

    * Si on imagine un arbre vide, sa taille vaut 0 et sa hauteur n'est pas définie, d'après le sens qu'on a donné plus haut à la hauteur d'un arbre. On verra qu'il est parfois pratique de considérer que la hauteur d'un arbre vide est -1. 

    * Certains auteurs n'aiment pas avoir une situation de "non définition" de la hauteur de l'arbre vide et ils prennent une autre définition pour la profondeur d'un nœud : ils démarrent à 1 pour la racine et les profondeurs des autres nœuds sont décalés de +1. Avec cette définition, l'arbre de l'exemple ci-dessus aura une hauteur de 4 et un arbre vide aura une hauteur de 0.

    **Lisez** bien les énoncés des exercices : il y sera précisé quelle définition il faut utiliser !


## ^^4. Exercices^^

!!! question "Exercice 1"

    Parmi les structures de données déjà rencontrées, à savoir :

    - les files
    - les piles
    - les listes chainées
    - les arbres

    laquelle ou lesquelles utiliseriez-vous pour représenter :

    1. l'ensemble des présidents de la V^e^ République française ?
    2. l'organigramme d'une entreprise ?
    3. la liste des passagers d'un avion doté d'une seule porte, où l'on fait rentrer d'abord ceux qui ont un siège au fond de l'avion (loin de la porte) et où on fait sortir d'abord ceux qui sont à l'avant de l'avion ?

!!! question "Exercice 2"

    1. Dessiner tous les arbres de taille 3 et, pour chacun d'eux, déterminer leur hauteur. 
    <!-- Il y en a 2. -->

    2. Même question avec les arbres de taille 4.
    <!-- Il y en a 5 (si on distingue bien la droite et la gauche). -->

    ??? danger "Solution"
        1. Il y a seulement deux arbres de taille 3, l'un de hauteur 1 et l'autre de hauteur 2.

        2. Il y a cinq arbres de taille 4 (si on distingue bien la droite et la gauche) :
        ![](data/arbre_taille4.png){: .center width=80%}


!!! question "Exercice 3" 

    {{ multi_qcm(
        [
            "La taille d'un arbre est-elle strictement supérieure à sa hauteur ?",
            ["Oui", "Non, pas toujours"],[1],
        ],
        shuffle = False) }}


## ^^5. Exercice sur l'introduction aux structures arborescentes^^

Télécharger l'[énoncé](Exos_Intro_Arbres.pdf) : exercice à faire sur feuille, ou bien sur le [carnet Jupyter](TP_Exos_ArbresIntro.ipynb) à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.