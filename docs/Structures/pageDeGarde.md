# Structures de données :japanese_castle:

Cette partie est décomposée en :

* :scroll: Dictionnaires (rappels et compléments)

* 📚 Structures linéaires :

    - Files
    - Piles
    - Listes chainées

* :palm_tree: Arbres :

    - Généralités sur les arbres
    - Arbres binaires
    - Arbres binaires de recherche (ABR)

* 🕸️ Graphes :

    - Introduction aux graphes

